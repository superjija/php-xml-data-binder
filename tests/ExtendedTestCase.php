<?php

namespace PXDB\Tests;

use PHPUnit\Framework\TestCase;


class ExtendedTestCase
    extends TestCase
{
    protected function assertEqualsWhiteSpaceInsensitive(
        $expected,
        $actual,
        $message = '',
        $delta = 0.0,
        $maxDepth = 10,
        $canonicalize = FALSE,
        $ignoreCase = FALSE
    ) {

        $expected = $this->normalize($expected);
        $actual = $this->normalize($actual);
        $this->assertEquals($expected, $actual, $message, $delta, $maxDepth, $canonicalize, $ignoreCase);
    }

    protected function normalize($s)
    {
        $s = str_replace(["\r\n", "\r"], "\n", $s);
        $s = preg_replace('#[\x00-\x08\x0B-\x1F\x7F-\x9F]+#u', '', $s);
        // right trim
        $s = preg_replace('#[\t ]+$#m', '', $s);
        // leading and trailing blank lines
        $s = trim($s, "\n");
        $s = preg_replace("#[ \t]+#", " ", $s);
        return $s;
    }
}