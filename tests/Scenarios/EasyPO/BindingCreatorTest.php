<?php
namespace PXDB\Tests\Scenarios\EasyPO;

use PXDB\Binding\Creator;
use PXDB\CodeGen\ASTCreator;
use PXDB\CodeGen\SchemaParser;
use PXDB\CodeGen\TypeUsage;
use PXDB\Tests\ExtendedTestCase;


class BindingCreatorTest
    extends ExtendedTestCase
{
    public function testEasyPoBinding() {
        $filepath = dirname(__FILE__) . '/../../_files/EasyPO/';
        $schemaFile = $filepath . '/easypo.xsd';
        $schema = simplexml_load_file($schemaFile);
        $bindingFile = file_get_contents($filepath . '/binding.xml');

        $typeUsage = new TypeUsage();

        $parser = new SchemaParser($schemaFile, $typeUsage);
        $parsedTree = $parser->parse();

        $creator = new ASTCreator();
        $parsedTree->accept($creator);

        $typeList = $creator->getTypeList();

        $usages = $typeUsage->getTypeUsages();


        //        $optimizer = new ASTOptimizer($typeList, $typeUsage);
        //        $typeList = $optimizer->optimize();

        $b = new Creator($typeList);

        foreach ($typeList as &$type) {
            $type->accept($b);
        }

        $this->assertEqualsWhiteSpaceInsensitive($bindingFile, $b->getXml());
    }
}