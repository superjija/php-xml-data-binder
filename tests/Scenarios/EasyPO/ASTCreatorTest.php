<?php

namespace PXDB\Tests\Scenarios\EasyPO;

use PXDB\AST\CollectionItem;
use PXDB\AST\Type;
use PXDB\AST\TypeAttribute;
use PXDB\CodeGen\ASTCreator;
use PXDB\CodeGen\SchemaParser;
use PXDB\Tests\ExtendedTestCase;


class ASTCreatorTest
    extends ExtendedTestCase
{
    public function testEasyPoXSD()
    {
        $filepath = dirname(__FILE__) . '/../../_files/EasyPO/';
        $schema = simplexml_load_file($filepath . '/easypo.xsd');

        $parser = new SchemaParser();
        $parser->setSchema($schema);

        $parsedTree = $parser->parse();

        $namespaces = [
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'po' => 'http://openuri.org/easypo',
        ];

        $expectedTypeList = [];

        $expectedType1 = new Type('purchase-order');
        $expectedType1->setAsRoot();
        $expectedType1->setTargetNamespace('http://openuri.org/easypo');
        $expectedType1->setNamespaces($namespaces);
        $expectedType1->add(new TypeAttribute('customer', 'customer'));
        $expectedType1->add(new TypeAttribute('date', 'dateTime'));
        $expectedType1->add(new CollectionItem('line-item', 'line-item'));
        $expectedType1->add(new TypeAttribute('shipper', 'shipper', TRUE));

        $expectedType2 = new Type('customer');
        $expectedType2->setTargetNamespace('http://openuri.org/easypo');
        $expectedType2->setNamespaces($namespaces);
        $expectedType2->add(new TypeAttribute('name', 'string'));
        $expectedType2->add(new TypeAttribute('address', 'string'));
        $ta = new TypeAttribute('age', 'int', FALSE);
        $ta->setStyle('attribute');
        $expectedType2->add($ta);
        $ta = new TypeAttribute('moo', 'int', TRUE);
        $ta->setStyle('attribute');
        $expectedType2->add($ta);
        $ta = new TypeAttribute('poo', 'int', TRUE);
        $ta->setStyle('attribute');
        $expectedType2->add($ta);

        $expectedType3 = new Type('line-item');
        $expectedType3->setTargetNamespace('http://openuri.org/easypo');
        $expectedType3->setNamespaces($namespaces);
        $expectedType3->add(new TypeAttribute('description', 'string'));
        $expectedType3->add(new TypeAttribute('per-unit-ounces', 'decimal'));
        $expectedType3->add(new TypeAttribute('price', 'decimal'));
        $expectedType3->add(new TypeAttribute('quantity', 'integer'));

        $expectedType4 = new Type('shipper');
        $expectedType4->setTargetNamespace('http://openuri.org/easypo');
        $expectedType4->setNamespaces($namespaces);
        $expectedType4->add(new TypeAttribute('name', 'string'));
        $expectedType4->add(new TypeAttribute('per-ounce-rate', 'decimal'));

        $parser = new SchemaParser();
        $parser->setSchema($schema);
        $tree = $parser->parse();

        $creator = new ASTCreator();
        $tree->accept($creator);

        $typeList = $creator->getTypeList();
        $this->assertEquals(4, count($typeList));

        $this->assertEquals($expectedType1, $typeList[0]);
        $this->assertEquals($expectedType2, $typeList[1]);
        $this->assertEquals($expectedType3, $typeList[2]);
        $this->assertEquals($expectedType4, $typeList[3]);
    }
}