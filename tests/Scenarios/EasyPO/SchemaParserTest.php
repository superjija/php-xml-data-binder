<?php
namespace PXDB\Tests\Scenarios\EasyPO;


use PXDB\CodeGen\SchemaParser;
use PXDB\Tests\ExtendedTestCase;


class SchemaParserTest
    extends ExtendedTestCase
{
    public function testScenarioOTARQSchema() {
        $filepath = dirname(__FILE__) . '/../../_files/EasyPO/';
        $xml = simplexml_load_file($filepath . '/easypo.xsd');

        $parser = new SchemaParser();
        $parser->setSchema($xml);

        $parsedTree = $parser->parse();
        $this->assertTrue(TRUE);
    }
}