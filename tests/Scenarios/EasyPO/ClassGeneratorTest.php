<?php
namespace PXDB\Tests\Scenarios\EasyPO;

use PXDB\Binding\Creator;
use PXDB\CodeGen\ASTCreator;
use PXDB\CodeGen\ASTOptimizer;
use PXDB\CodeGen\ClassGenerator;
use PXDB\CodeGen\SchemaParser;
use PXDB\CodeGen\TypeUsage;
use PXDB\Tests\ExtendedTestCase;


class ClassGeneratorTest
    extends ExtendedTestCase
{
    public function testClassGeneration() {
        $filepath = dirname(__FILE__) . '/../../_files/EasyPO';
        $schemaFile = $filepath . '/easypo.xsd';
        $bindingFile = $filepath . '/binding.xml';
        $customerFile = $filepath . '/Customer.php';
        $lineItemFile = $filepath . '/LineItem.php';
        $purchaseOrderFile = $filepath . '/PurchaseOrder.php';
        $shipperFile = $filepath . '/Shipper.php';

        $typeUsage = new TypeUsage();

        // most of this test-case follows the flow of CodeGen
        // phase 1
        $parser = new SchemaParser($schemaFile, $typeUsage);
        $parsedTree = $parser->parse();

        // phase 2
        $creator = new ASTCreator();
        $parsedTree->accept($creator);

        $typeList = $creator->getTypeList();

        // phase 3
        $usages = $typeUsage->getTypeUsages();


        $optimizer = new ASTOptimizer($typeList, $typeUsage);
        //        $typeList = $optimizer->optimize();

        // phase 4
        $b = new Creator($typeList);

        foreach ($typeList as &$type) {
            $type->accept($b);
        }

        $this->assertEqualsWhiteSpaceInsensitive(file_get_contents($bindingFile), $b->getXml());

        // phase 5
        $generator = new ClassGenerator();
        foreach ($typeList as &$type) {
            $type->accept($generator);
        }

        $classes = $generator->getClasses();

        $this->assertEquals(4, count($classes));
        $this->assertEqualsWhiteSpaceInsensitive(file_get_contents($customerFile), "<?php\n" . $classes['Customer']);
        $this->assertEqualsWhiteSpaceInsensitive(file_get_contents($lineItemFile), "<?php\n" . $classes['LineItem']);
        $this->assertEqualsWhiteSpaceInsensitive(file_get_contents($purchaseOrderFile), "<?php\n" . $classes['PurchaseOrder']);
        $this->assertEqualsWhiteSpaceInsensitive(file_get_contents($shipperFile), "<?php\n" . $classes['Shipper']);
    }

    public function _testClassGenerationWithTypeChecks() {
        $filepath = dirname(__FILE__) . '/../../_files/EasyPO';
        $schemaFile = $filepath . '/easypo.xsd';
        $bindingFile = $filepath . '/binding.xml';
        $customerFile = $filepath . '/Customer_TypeChecked.php';
        $lineItemFile = $filepath . '/LineItem_TypeChecked.php';
        $purchaseOrderFile = $filepath . '/PurchaseOrder_TypeChecked.php';
        $shipperFile = $filepath . '/Shipper_TypeChecked.php';

        $typeUsage = new TypeUsage();

        // most of this test-case follows the flow of CodeGen
        // phase 1
        $parser = new SchemaParser($schemaFile, $typeUsage);
        $parsedTree = $parser->parse();

        // phase 2
        $creator = new ASTCreator();
        $parsedTree->accept($creator);

        $typeList = $creator->getTypeList();

        // phase 3
        $usages = $typeUsage->getTypeUsages();


        $optimizer = new ASTOptimizer($typeList, $typeUsage);
        $typeList = $optimizer->optimize();

        $optimizer = new ASTOptimizer($typeList, $typeUsage);
        //        $typeList = $optimizer->optimize();

        // phase 4
        $b = new Creator($typeList);

        foreach ($typeList as &$type) {
            $type->accept($b);
        }

        $this->assertEquals(file_get_contents($bindingFile), $b->getXml());

        // phase 5
        $generator = new ClassGenerator();
        $generator->enableTypeChecks();
        foreach ($typeList as &$type) {
            $type->accept($generator);
        }

        $classes = $generator->getClasses();

        $this->assertEquals(4, count($classes));
        $this->assertEquals(file_get_contents($customerFile), "<?php\n" . $classes['Customer']);
        $this->assertEquals(file_get_contents($lineItemFile), "<?php\n" . $classes['LineItem']);
        $this->assertEquals(file_get_contents($purchaseOrderFile), "<?php\n" . $classes['PurchaseOrder']);
        $this->assertEquals(file_get_contents($shipperFile), "<?php\n" . $classes['Shipper']);
    }
}