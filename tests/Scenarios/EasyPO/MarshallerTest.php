<?php
namespace PXDB\Tests\Scenarios\EasyPO;

use Customer;
use DOMDocument;
use LineItem;
use PurchaseOrder;
use PXDB\Runtime\Binding;
use PXDB\Runtime\Marshaller;
use PXDB\Tests\ExtendedTestCase;
use Shipper;

require_once dirname(__FILE__) . '/../../_files/EasyPO/Customer.php';
require_once dirname(__FILE__) . '/../../_files/EasyPO/LineItem.php';
require_once dirname(__FILE__) . '/../../_files/EasyPO/PurchaseOrder.php';
require_once dirname(__FILE__) . '/../../_files/EasyPO/Shipper.php';

class MarshallerTest
    extends ExtendedTestCase
{
    public function testMarshalling() {
        $expectedXml = <<<XML
<?xml version="1.0"?>
<purchase-order xmlns="http://openuri.org/easypo">
  <customer age="18">
    <name>Gladys Kravitz</name>
    <address>Anytown, PA</address>
  </customer>
  <date>2003-01-07T14:16:00-05:00</date>
  <line-item>
    <description>Burnham's Celestial Handbook, Vol 1</description>
    <per-unit-ounces>5</per-unit-ounces>
    <price>21.79</price>
    <quantity>2</quantity>
  </line-item>
  <line-item>
    <description>Burnham's Celestial Handbook, Vol 2</description>
    <per-unit-ounces>5</per-unit-ounces>
    <price>19.89</price>
    <quantity>2</quantity>
  </line-item>
  <shipper>
    <name>ZipShip</name>
    <per-ounce-rate>0.74</per-ounce-rate>
  </shipper>
</purchase-order>
XML;

        $filepath = dirname(__FILE__) . '/../../_files/EasyPO/';
        $binding = new Binding($filepath . '/binding.xml');
        $marshaller = new Marshaller($binding);

        $po = new PurchaseOrder();
        $po->setDate('2003-01-07T14:16:00-05:00');

        $customer = new Customer();
        $customer->setName('Gladys Kravitz');
        $customer->setAddress('Anytown, PA');
        $customer->setAge(18);

        $lineItem1 = new LineItem();
        $lineItem1->setDescription('Burnham\'s Celestial Handbook, Vol 1');
        $lineItem1->setPerUnitOunces('5');
        $lineItem1->setPrice(21.79);
        $lineItem1->setQuantity(2);

        $lineItem2 = new LineItem();
        $lineItem2->setDescription('Burnham\'s Celestial Handbook, Vol 2');
        $lineItem2->setPerUnitOunces('5');
        $lineItem2->setPrice(19.89);
        $lineItem2->setQuantity(2);

        $shipper = new Shipper();
        $shipper->setName('ZipShip');
        $shipper->setPerOunceRate(0.74);

        $po->setCustomer($customer);
        $po->setLineItems(array($lineItem1, $lineItem2));
        $po->setShipper($shipper);

        $xml = $marshaller->marshal($po);

        $this->assertEqualsWhiteSpaceInsensitive($expectedXml, $xml);

        $dom = new DOMDocument();
        $dom->loadXML($xml);
        $this->assertTrue($dom->schemaValidate($filepath . '/easypo.xsd'));
    }
}