<?php
namespace PXDB\Tests\Scenarios\Reference;

use PXDB\AST\Enumeration;
use PXDB\AST\EnumerationValue;
use PXDB\AST\Structure;
use PXDB\AST\StructureElement;
use PXDB\AST\StructureType;
use PXDB\AST\Type;
use PXDB\AST\TypeAttribute;
use PXDB\ParseTree\AttributeNode;
use PXDB\ParseTree\ChoiceNode;
use PXDB\ParseTree\ComplexContentNode;
use PXDB\ParseTree\ComplexTypeNode;
use PXDB\ParseTree\ElementNode;
use PXDB\ParseTree\EnumerationNode;
use PXDB\ParseTree\ExtensionNode;
use PXDB\ParseTree\RestrictionNode;
use PXDB\ParseTree\RootNode;
use PXDB\ParseTree\SequenceNode;
use PXDB\ParseTree\SimpleTypeNode;
use PXDB\Util\XsdType;


class LocalElementComplexTypeTest
    extends ReferenceTest
{
    public function setUp() {
        $this->pathToTestFiles = dirname(__FILE__) . '/../../../_files/Reference/Basic/LocalElementComplexType';
        $this->schemaFile = 'LocalElementComplexType.xsd';
    }

    public function getASTs() {
        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        $type1 = new Type('localElementComplexType', 'LocalElementComplexType');
        $type1->setAsRoot();
        $type1->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type1->setNamespaces($namespaces);

        $type2 = new Type('LocalElementComplexType');
        $type2->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type2->setNamespaces($namespaces);
        $type2_structure = new Structure('name');
        $type2_structure_element1 = new StructureElement('firstName', 'string');
        $type2_structure_element2 = new StructureElement('lastName', 'string');

        $type2_structure->add($type2_structure_element1);
        $type2_structure->add($type2_structure_element2);
        $type2->add($type2_structure);

        $type3 = new Type('echoLocalElementComplexType');
        $type3->setAsRoot();
        $type3->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type3->setNamespaces($namespaces);
        $type3_attribute = new TypeAttribute('', 'localElementComplexType');

        $type3->add($type3_attribute);

        return array($type1, $type2, $type3);
    }

    public function getParseTree() {
        $tree = new RootNode();
        $tree->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');

        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );
        /*
       <xs:element xmlns:wsdl11="http://schemas.xmlsoap.org/wsdl/"
                   xmlns:soap11enc="http://schemas.xmlsoap.org/soap/encoding/"
                   name="localElementComplexType"
                   type="ex:LocalElementComplexType"/>
         */
        $node1_element = new ElementNode(array('name' => 'localElementComplexType', 'type' => 'LocalElementComplexType'), 0);
        $node1_element->setNamespaces($namespaces);
        /*
       <xs:complexType xmlns:wsdl11="http://schemas.xmlsoap.org/wsdl/"
                       xmlns:soap11enc="http://schemas.xmlsoap.org/soap/encoding/"
                       name="LocalElementComplexType">
            <xs:sequence>
              <xs:element name="name">
                <xs:complexType>
                  <xs:sequence>
                      <xs:element name="firstName" type="xs:string"/>

                      <xs:element name="lastName" type="xs:string"/>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
         */
        $node2_complexType = new ComplexTypeNode(array('name' => 'LocalElementComplexType'), 0);
        $node2_complexType->setNamespaces($namespaces);
        $node2_sequence = new SequenceNode(array(), 1);
        $node2_sequence->setNamespaces($namespaces);
        $node2_element = new ElementNode(array('name' => 'name'), 2);
        $node2_element->setNamespaces($namespaces);
        $node2_complexType1 = new ComplexTypeNode(array(), 3);
        $node2_complexType1->setNamespaces($namespaces);
        $node2_sequence1 = new SequenceNode(array(), 4);
        $node2_sequence1->setNamespaces($namespaces);
        $node2_element1 = new ElementNode(array('name' => 'firstName', 'type' => 'string'), 5);
        $node2_element1->setNamespaces($namespaces);
        $node2_element2 = new ElementNode(array('name' => 'lastName', 'type' => 'string'), 5);
        $node2_element2->setNamespaces($namespaces);

        $node2_sequence1->add($node2_element1);
        $node2_sequence1->add($node2_element2);
        $node2_complexType1->add($node2_sequence1);
        $node2_element->add($node2_complexType1);
        $node2_sequence->add($node2_element);
        $node2_complexType->add($node2_sequence);

        /*
       <xs:element name="echoLocalElementComplexType">
          <xs:complexType>
             <xs:sequence>

                <xs:element ref="ex:localElementComplexType"/>
             </xs:sequence>
          </xs:complexType>
       </xs:element>
         */
        $node3_element = new ElementNode(array('name' => 'echoLocalElementComplexType'), 0);
        $node3_element->setNamespaces($namespaces);
        $node3_complexType = new ComplexTypeNode(array(), 1);
        $node3_complexType->setNamespaces($namespaces);
        $node3_sequence = new SequenceNode(array(), 2);
        $node3_sequence->setNamespaces($namespaces);
        $node3_element1 = new ElementNode(array('type' => 'localElementComplexType'), 3);
        $node3_element1->setNamespaces($namespaces);

        $node3_sequence->add($node3_element1);
        $node3_complexType->add($node3_sequence);
        $node3_element->add($node3_complexType);

        $tree->add($node1_element);
        $tree->add($node2_complexType);
        $tree->add($node3_element);

        return $tree;
    }
}