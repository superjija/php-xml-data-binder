<?php
namespace PXDB\Tests\Scenarios\Reference;

use PXDB\AST\Type;
use PXDB\AST\TypeAttribute;
use PXDB\ParseTree\AttributeNode;
use PXDB\ParseTree\ComplexTypeNode;
use PXDB\ParseTree\ElementNode;
use PXDB\ParseTree\RootNode;
use PXDB\ParseTree\SequenceNode;


class GlobalAttributeTest
    extends ReferenceTest
{
    public function setUp() {
        $this->pathToTestFiles = dirname(__FILE__) . '/../../../_files/Reference/Basic/GlobalAttribute';
        $this->schemaFile = 'GlobalAttribute.xsd';
    }

    public function getASTs() {
        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        $type1 = new Type('globalAttributeAttr', 'string');
        $type1->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type1->setNamespaces($namespaces);
        $type1->setValueStyle('attribute');


        $type2 = new Type('GlobalAttribute');
        $type2->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type2->setNamespaces($namespaces);
        $type2_attribute1 = new TypeAttribute('globalAttributeElement', 'string');
        $type2_attribute2 = new TypeAttribute('', 'globalAttributeAttr', true);
        $type2_attribute2->setStyle('attribute');

        $type2->add($type2_attribute1);
        $type2->add($type2_attribute2);


        $type3 = new Type('globalAttribute', 'GlobalAttribute');
        $type3->setAsRoot();
        $type3->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type3->setNamespaces($namespaces);


        $type4 = new Type('echoGlobalAttribute');
        $type4->setAsRoot();
        $type4->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type4->setNamespaces($namespaces);
        $type4_attribute = new TypeAttribute('', 'globalAttribute');

        $type4->add($type4_attribute);


        return array($type1, $type2, $type3, $type4);
    }

    public function getParseTree() {
        $tree = new RootNode();
        $tree->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');

        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        /*
       <xs:attribute xmlns:wsdl11="http://schemas.xmlsoap.org/wsdl/"
                     xmlns:soap11enc="http://schemas.xmlsoap.org/soap/encoding/"
                     name="globalAttributeAttr"
                     type="xs:string"/>
        */
        $node1_attribute = new AttributeNode(array('name' => 'globalAttributeAttr', 'type' => 'string'), 0);
        $node1_attribute->setNamespaces($namespaces);

        /*
       <xs:complexType xmlns:wsdl11="http://schemas.xmlsoap.org/wsdl/"
                       xmlns:soap11enc="http://schemas.xmlsoap.org/soap/encoding/"
                       name="GlobalAttribute">
            <xs:sequence>
              <xs:element name="globalAttributeElement" type="xs:string"/>
            </xs:sequence>
            <xs:attribute ref="ex:globalAttributeAttr"/>
          </xs:complexType>
        */
        $node2_complexType = new ComplexTypeNode(array('name' => 'GlobalAttribute'),0);
        $node2_complexType->setNamespaces($namespaces);
        $node2_sequence = new SequenceNode(array(), 1);
        $node2_sequence->setNamespaces($namespaces);
        $node2_element = new ElementNode(array('name' => 'globalAttributeElement', 'type' => 'string'), 2);
        $node2_element->setNamespaces($namespaces);
        $node2_attribute = new AttributeNode(array('type' => 'globalAttributeAttr'), 1);
        $node2_attribute->setNamespaces($namespaces);

        $node2_sequence->add($node2_element);
        $node2_complexType->add($node2_sequence);
        $node2_complexType->add($node2_attribute);

        /*
       <xs:element xmlns:wsdl11="http://schemas.xmlsoap.org/wsdl/"
                   xmlns:soap11enc="http://schemas.xmlsoap.org/soap/encoding/"
                   name="globalAttribute"
                   type="ex:GlobalAttribute"/>
        */
        $node3_element = new ElementNode(array('name' => 'globalAttribute', 'type' => 'GlobalAttribute'), 0);
        $node3_element->setNamespaces($namespaces);

        /*
       <xs:element name="echoGlobalAttribute">
          <xs:complexType>
             <xs:sequence>
                <xs:element ref="ex:globalAttribute"/>
             </xs:sequence>
          </xs:complexType>
       </xs:element>
        */
        $node4_element = new ElementNode(array('name' => 'echoGlobalAttribute'), 0);
        $node4_element->setNamespaces($namespaces);
        $node4_complexType = new ComplexTypeNode(array(), 1);
        $node4_complexType->setNamespaces($namespaces);
        $node4_sequence = new SequenceNode(array(), 2);
        $node4_sequence->setNamespaces($namespaces);
        $node4_element1 = new ElementNode(array('type' => 'globalAttribute'), 3);
        $node4_element1->setNamespaces($namespaces);

        $node4_sequence->add($node4_element1);
        $node4_complexType->add($node4_sequence);
        $node4_element->add($node4_complexType);


        $tree->add($node1_attribute);
        $tree->add($node2_complexType);
        $tree->add($node3_element);
        $tree->add($node4_element);

        return $tree;
    }
}