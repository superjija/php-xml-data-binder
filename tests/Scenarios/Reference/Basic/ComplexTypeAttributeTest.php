<?php
namespace PXDB\Tests\Scenarios\Reference;

use PXDB\AST\Type;
use PXDB\AST\TypeAttribute;
use PXDB\ParseTree\AttributeNode;
use PXDB\ParseTree\ComplexTypeNode;
use PXDB\ParseTree\ElementNode;
use PXDB\ParseTree\RootNode;
use PXDB\ParseTree\SequenceNode;


class ComplexTypeAttributeTest
    extends ReferenceTest
{
    public function setUp() {
        $this->pathToTestFiles = dirname(__FILE__) . '/../../../_files/Reference/Basic/ComplexTypeAttribute';
        $this->schemaFile = 'ComplexTypeAttribute.xsd';
    }

    public function getASTs() {
        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        $typeReferencedElement = new Type('complexTypeAttribute', 'ComplexTypeAttribute');
        $typeReferencedElement->setAsRoot();
        $typeReferencedElement->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $typeReferencedElement->setNamespaces($namespaces);

        $typeElement = new Type('ComplexTypeAttribute');
        $typeElement->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $typeElement->setNamespaces($namespaces);
        $attribute = new TypeAttribute('premium', 'string');
        $typeElement->add($attribute);

        $attribute = new TypeAttribute('id', 'string');
        $attribute->setStyle('attribute');
        $typeElement->add($attribute);

        $attribute = new TypeAttribute('currency', 'string', true);
        $attribute->setStyle('attribute');
        $typeElement->add($attribute);

        $typeEchoElement = new Type('echoComplexTypeAttribute');
        $typeEchoElement->setAsRoot();
        $typeEchoElement->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $typeEchoElement->setNamespaces($namespaces);
        $typeEchoElementAttribute1 = new TypeAttribute('', 'complexTypeAttribute');


        $typeEchoElement->add($typeEchoElementAttribute1);

        return array($typeReferencedElement, $typeElement, $typeEchoElement);
    }

    public function getParseTree() {
        $tree = new RootNode();
        $tree->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');

        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );
        /*
           <xs:element xmlns:wsdl11="http://schemas.xmlsoap.org/wsdl/"
                       xmlns:soap11enc="http://schemas.xmlsoap.org/soap/encoding/"
                       name="complexTypeAttribute"
                       type="ex:ComplexTypeAttribute"/>
         */
        $element1 = new ElementNode(array('name' => 'complexTypeAttribute', 'type' => 'ComplexTypeAttribute'), 0);
        $element1->setNamespaces($namespaces);
        /*
           <xs:complexType xmlns:wsdl11="http://schemas.xmlsoap.org/wsdl/"
                           xmlns:soap11enc="http://schemas.xmlsoap.org/soap/encoding/"
                           name="ComplexTypeAttribute">
                <xs:sequence>
                  <xs:element name="premium" type="xs:string"/>
                </xs:sequence>
                <xs:attribute name="id" type="xs:string"/>
                <xs:attribute name="currency" type="xs:string"/>

              </xs:complexType>
         */
        $complexType = new ComplexTypeNode(array('name' => 'ComplexTypeAttribute'), 0);
        $complexType->setNamespaces($namespaces);
        $sequence = new SequenceNode(array(), 1);
        $sequence->setNamespaces($namespaces);
        $elementNode = new ElementNode(array('name' => 'premium', 'type' => 'string'), 2);
        $elementNode->setNamespaces($namespaces);
        $sequence->add($elementNode);

        $complexType->add($sequence);
        $attribute = new AttributeNode(array('name' => 'id', 'type' => 'string', 'use' => 'required'), 1);
        $attribute->setNamespaces($namespaces);
        $complexType->add($attribute);

        $attribute = new AttributeNode(array('name' => 'currency', 'type' => 'string'), 1);
        $attribute->setNamespaces($namespaces);
        $complexType->add($attribute);
        /*
           <xs:element name="echoComplexTypeAttribute">
              <xs:complexType>
                 <xs:sequence>
                    <xs:element ref="ex:complexTypeAttribute"/>
                 </xs:sequence>
              </xs:complexType>
           </xs:element>
         */
        $element2 = new ElementNode(array('name' => 'echoComplexTypeAttribute'), 0);
        $element2->setNamespaces($namespaces);
        $complexType1 = new ComplexTypeNode(array(), 1);
        $complexType1->setNamespaces($namespaces);
        $sequence1 = new SequenceNode(array(), 2);
        $sequence1->setNamespaces($namespaces);
        $element3 = new ElementNode(array('type' => 'complexTypeAttribute'), 3);
        $element3->setNamespaces($namespaces);

        $sequence1->add($element3);
        $complexType1->add($sequence1);
        $element2->add($complexType1);

        $tree->add($element1);
        $tree->add($complexType);
        $tree->add($element2);

        return $tree;
    }
}