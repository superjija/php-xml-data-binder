<?php
namespace PXDB\Tests\Scenarios\Reference;

use PXDB\AST\Type;
use PXDB\AST\TypeAttribute;
use PXDB\ParseTree\ComplexTypeNode;
use PXDB\ParseTree\ElementNode;
use PXDB\ParseTree\RootNode;
use PXDB\ParseTree\SequenceNode;


class ElementReferenceTest
    extends ReferenceTest
{
    public function setUp() {
        $this->pathToTestFiles = dirname(__FILE__) . '/../../../_files/Reference/Basic/ElementReference';
        $this->schemaFile = 'ElementReference.xsd';
    }

    public function getASTs() {
        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        $type1 = new Type('customerName', 'CustomerName');
        $type1->setAsRoot();
        $type1->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type1->setNamespaces($namespaces);


        $type2 = new Type('firstName', 'string');
        $type2->setAsRoot();
        $type2->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type2->setNamespaces($namespaces);


        $type3 = new Type('lastName', 'string');
        $type3->setAsRoot();
        $type3->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type3->setNamespaces($namespaces);


        $type4 = new Type('CustomerName');
        $type4->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type4->setNamespaces($namespaces);
        $type4_attribute1 = new TypeAttribute('', 'firstName');
        $type4_attribute2 = new TypeAttribute('', 'lastName');

        $type4->add($type4_attribute1);
        $type4->add($type4_attribute2);


        $type5 = new Type('echoElementReference');
        $type5->setAsRoot();
        $type5->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type5->setNamespaces($namespaces);
        $type5_attribute = new TypeAttribute('', 'customerName');

        $type5->add($type5_attribute);


        return array($type1, $type2, $type3, $type4, $type5);
    }

    public function getParseTree() {
        $tree = new RootNode();
        $tree->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');

        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );
        /*
           <xs:element xmlns:wsdl11="http://schemas.xmlsoap.org/wsdl/"
                       xmlns:soap11enc="http://schemas.xmlsoap.org/soap/encoding/"
                       name="customerName"
                       type="ex:CustomerName"/>
         */
        $node1_element = new ElementNode(array('name' => 'customerName', 'type' => 'CustomerName'), 0);
        $node1_element->setNamespaces($namespaces);

        /*
           <xs:element xmlns:wsdl11="http://schemas.xmlsoap.org/wsdl/"
                       xmlns:soap11enc="http://schemas.xmlsoap.org/soap/encoding/"
                       name="firstName"
                       type="xs:string"/>
         */
        $node2_element = new ElementNode(array('name' => 'firstName', 'type' => 'string'), 0);
        $node2_element->setNamespaces($namespaces);

        /*
           <xs:element xmlns:wsdl11="http://schemas.xmlsoap.org/wsdl/"
                       xmlns:soap11enc="http://schemas.xmlsoap.org/soap/encoding/"
                       name="lastName"
                       type="xs:string"/>
         */
        $node3_element = new ElementNode(array('name' => 'lastName', 'type' => 'string'), 0);
        $node3_element->setNamespaces($namespaces);

        /*
           <xs:complexType xmlns:wsdl11="http://schemas.xmlsoap.org/wsdl/"
                           xmlns:soap11enc="http://schemas.xmlsoap.org/soap/encoding/"
                           name="CustomerName">
                <xs:sequence>
                  <xs:element ref="ex:firstName"/>
                  <xs:element ref="ex:lastName"/>

                </xs:sequence>
              </xs:complexType>
         */
        $node4_complexType = new ComplexTypeNode(array('name' => 'CustomerName'),0);
        $node4_complexType->setNamespaces($namespaces);
        $node4_sequence = new SequenceNode(array(), 1);
        $node4_sequence->setNamespaces($namespaces);
        $node4_element1 = new ElementNode(array('type' => 'firstName'), 2);
        $node4_element1->setNamespaces($namespaces);
        $node4_element2 = new ElementNode(array('type' => 'lastName'), 2);
        $node4_element2->setNamespaces($namespaces);

        $node4_sequence->add($node4_element1);
        $node4_sequence->add($node4_element2);
        $node4_complexType->add($node4_sequence);
        /*
           <xs:element name="echoElementReference">
              <xs:complexType>
                 <xs:sequence>
                    <xs:element ref="ex:customerName"/>
                 </xs:sequence>
              </xs:complexType>
           </xs:element>
         */
        $node5_element = new ElementNode(array('name' => 'echoElementReference'), 0);
        $node5_element->setNamespaces($namespaces);
        $node5_complexType = new ComplexTypeNode(array(), 1);
        $node5_complexType->setNamespaces($namespaces);
        $node5_sequence = new SequenceNode(array(), 2);
        $node5_sequence->setNamespaces($namespaces);
        $node5_element1 = new ElementNode(array('type' => 'customerName'), 3);
        $node5_element1->setNamespaces($namespaces);

        $node5_sequence->add($node5_element1);
        $node5_complexType->add($node5_sequence);
        $node5_element->add($node5_complexType);


        $tree->add($node1_element);
        $tree->add($node2_element);
        $tree->add($node3_element);
        $tree->add($node4_complexType);
        $tree->add($node5_element);

        return $tree;
    }
}