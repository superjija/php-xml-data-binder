<?php
namespace PXDB\Tests\Scenarios\Reference;

use PXDB\AST\Type;
use PXDB\AST\TypeAttribute;
use PXDB\ParseTree\ComplexTypeNode;
use PXDB\ParseTree\ElementNode;
use PXDB\ParseTree\RootNode;
use PXDB\ParseTree\SequenceNode;


class DoubleElementTest
    extends ReferenceTest
{
    public function setUp() {
        $this->pathToTestFiles = dirname(__FILE__) . '/../../../_files/Reference/Basic/DoubleElement';
        $this->schemaFile = 'DoubleElement.xsd';
    }

    public function getASTs() {
        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        $typeReferencedElement = new Type('doubleElement', 'double');
        $typeReferencedElement->setAsRoot();
        $typeReferencedElement->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $typeReferencedElement->setNamespaces($namespaces);

        $typeElement = new Type('echoDoubleElement');
        $typeElement->setAsRoot();
        $typeElement->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $typeElement->setNamespaces($namespaces);

        $attribute = new TypeAttribute('', 'doubleElement');

        $typeElement->add($attribute);

        return array($typeReferencedElement, $typeElement);
    }

    public function getParseTree() {
        $tree = new RootNode();
        $tree->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');

        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        $element1 = new ElementNode(array('name' => 'doubleElement', 'type' => 'double'), 0);
        $element1->setNamespaces($namespaces);
        $element2 = new ElementNode(array('name' => 'echoDoubleElement', 'type' => ''), 0);
        $element2->setNamespaces($namespaces);
        $complexType = new ComplexTypeNode(array(), 1);
        $complexType->setNamespaces($namespaces);
        $sequence = new SequenceNode(array(), 2);
        $sequence->setNamespaces($namespaces);
        $element3 = new ElementNode(array('type' => 'doubleElement'), 3);
        $element3->setNamespaces($namespaces);

        $sequence->add($element3);
        $complexType->add($sequence);
        $element2->add($complexType);

        $tree->add($element1);
        $tree->add($element2);

        return $tree;
    }
}