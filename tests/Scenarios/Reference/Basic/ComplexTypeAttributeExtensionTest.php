<?php
namespace PXDB\Tests\Scenarios\Reference;

use PXDB\AST\Type;
use PXDB\AST\TypeAttribute;
use PXDB\ParseTree\AttributeNode;
use PXDB\ParseTree\ComplexContentNode;
use PXDB\ParseTree\ComplexTypeNode;
use PXDB\ParseTree\ElementNode;
use PXDB\ParseTree\ExtensionNode;
use PXDB\ParseTree\RootNode;
use PXDB\ParseTree\SequenceNode;


class ComplexTypeAttributeExtensionTest
    extends ReferenceTest
{
    public function setUp() {
        $this->pathToTestFiles = dirname(__FILE__) . '/../../../_files/Reference/Basic/ComplexTypeAttributeExtension';
        $this->schemaFile = 'ComplexTypeAttributeExtension.xsd';
    }

    public function getASTs() {
        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        $type1 = new Type('complexTypeAttributeExtension', 'ComplexTypeAttributeExtension');
        $type1->setAsRoot();
        $type1->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type1->setNamespaces($namespaces);


        $type2 = new Type('ComplexTypeAttributeBase');
        $type2->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type2->setNamespaces($namespaces);
        $type2_attribute = new TypeAttribute('name', 'string');
        $type2->add($type2_attribute);

        //            $attribute = new TypeAttribute('id', 'string');
        //            $attribute->setStyle('attribute');
        //            $type2->add($attribute);
        //
        //            $attribute = new TypeAttribute('currency', 'string');
        //            $attribute->setStyle('attribute');
        //            $type2->add($attribute);

        $type3 = new Type('ComplexTypeAttributeExtension', '', 'ComplexTypeAttributeBase');
        $type3->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type3->setNamespaces($namespaces);
        $type3_attribute = new TypeAttribute('gender', 'string', true);
        $type3_attribute->setStyle('attribute');

        $type3->add($type3_attribute);


        $type4 = new Type('echoComplexTypeAttributeExtension');
        $type4->setAsRoot();
        $type4->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type4->setNamespaces($namespaces);
        $type4_attribute = new TypeAttribute('', 'complexTypeAttributeExtension');


        $type4->add($type4_attribute);

        return array($type1, $type2, $type3, $type4);
    }

    public function getParseTree() {
        $tree = new RootNode();
        $tree->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');

        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );
        /*
           <xs:element xmlns:wsdl11="http://schemas.xmlsoap.org/wsdl/"
                       xmlns:soap11enc="http://schemas.xmlsoap.org/soap/encoding/"
                       name="complexTypeAttributeExtension"
                       type="ex:ComplexTypeAttributeExtension"/>
         */
        $node1_element = new ElementNode(array('name' => 'complexTypeAttributeExtension', 'type' => 'ComplexTypeAttributeExtension'), 0);
        $node1_element->setNamespaces($namespaces);

        /*
           <xs:complexType xmlns:wsdl11="http://schemas.xmlsoap.org/wsdl/"
                           xmlns:soap11enc="http://schemas.xmlsoap.org/soap/encoding/"
                           name="ComplexTypeAttributeBase">
                <xs:sequence>
                  <xs:element name="name" type="xs:string"/>
                </xs:sequence>
              </xs:complexType>
         */
        $node2_complexType = new ComplexTypeNode(array('name' => 'ComplexTypeAttributeBase'), 0);
        $node2_complexType->setNamespaces($namespaces);
        $node2_sequence = new SequenceNode(array(), 1);
        $node2_sequence->setNamespaces($namespaces);
        $node2_element = new ElementNode(array('name' => 'name', 'type' => 'string'), 2);
        $node2_element->setNamespaces($namespaces);

        $node2_sequence->add($node2_element);
        $node2_complexType->add($node2_sequence);

        /*
           <xs:complexType xmlns:wsdl11="http://schemas.xmlsoap.org/wsdl/"
                           xmlns:soap11enc="http://schemas.xmlsoap.org/soap/encoding/"
                           name="ComplexTypeAttributeExtension">

                <xs:complexContent>
                  <xs:extension base="ex:ComplexTypeAttributeBase">
                    <xs:attribute name="gender" type="xs:string"/>
                  </xs:extension>
                </xs:complexContent>
              </xs:complexType>
         */

        $node3_complexType = new ComplexTypeNode(array('name' => 'ComplexTypeAttributeExtension'), 0);
        $node3_complexType->setNamespaces($namespaces);
        $node3_complexContent = new ComplexContentNode(array(), 1);
        $node3_complexContent->setNamespaces($namespaces);
        $node3_extension = new ExtensionNode(array('base' => 'ComplexTypeAttributeBase'), 2);
        $node3_extension->setNamespaces($namespaces);
        $node3_attribute = new AttributeNode(array('name' => 'gender', 'type' => 'string'), 3);
        $node3_attribute->setNamespaces($namespaces);

        $node3_extension->add($node3_attribute);
        $node3_complexContent->add($node3_extension);
        $node3_complexType->add($node3_complexContent);

        /*
           <xs:element name="echoComplexTypeAttributeExtension">
              <xs:complexType>
                 <xs:sequence>

                    <xs:element ref="ex:complexTypeAttributeExtension"/>
                 </xs:sequence>
              </xs:complexType>
           </xs:element>
         */
        $node4_element = new ElementNode(array('name' => 'echoComplexTypeAttributeExtension'), 0);
        $node4_element->setNamespaces($namespaces);
        $node4_complexType = new ComplexTypeNode(array(), 1);
        $node4_complexType->setNamespaces($namespaces);
        $node4_sequence = new SequenceNode(array(), 2);
        $node4_sequence->setNamespaces($namespaces);
        $node4_element1 = new ElementNode(array('type' => 'complexTypeAttributeExtension'), 3);
        $node4_element1->setNamespaces($namespaces);

        $node4_sequence->add($node4_element1);
        $node4_complexType->add($node4_sequence);
        $node4_element->add($node4_complexType);

        $tree->add($node1_element);
        $tree->add($node2_complexType);
        $tree->add($node3_complexType);
        $tree->add($node4_element);

        return $tree;
    }
}