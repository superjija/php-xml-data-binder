<?php
namespace PXDB\Tests\Scenarios\Reference;

use PXDB\AST\Structure;
use PXDB\AST\StructureElement;
use PXDB\AST\StructureType;
use PXDB\AST\Type;
use PXDB\AST\TypeAttribute;
use PXDB\ParseTree\AttributeNode;
use PXDB\ParseTree\ChoiceNode;
use PXDB\ParseTree\ComplexContentNode;
use PXDB\ParseTree\ComplexTypeNode;
use PXDB\ParseTree\ElementNode;
use PXDB\ParseTree\ExtensionNode;
use PXDB\ParseTree\RootNode;
use PXDB\ParseTree\SequenceNode;


class AttributeFormUnqualifiedTest
    extends ReferenceTest
{
    public function setUp() {
        $this->pathToTestFiles = dirname(__FILE__) . '/../../../_files/Reference/Basic/AttributeFormUnqualified';
        $this->schemaFile = 'AttributeFormUnqualified.xsd';
    }

    public function getASTs() {
        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        $type1 = new Type('attributeFormUnqualified', 'AttributeFormUnqualified');
        $type1->setAsRoot();
        $type1->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type1->setNamespaces($namespaces);


        $type2 = new Type('AttributeFormUnqualified');
        $type2->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type2->setNamespaces($namespaces);
        $type2_attribute1 = new TypeAttribute('premium', 'string');
        $type2_attribute2 = new TypeAttribute('id', 'string');
        $type2_attribute2->setStyle('attribute');

        $type2->add($type2_attribute1);
        $type2->add($type2_attribute2);


        $type3 = new Type('echoAttributeFormUnqualified');
        $type3->setAsRoot();
        $type3->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type3->setNamespaces($namespaces);
        $type3_attribute = new TypeAttribute('', 'attributeFormUnqualified');

        $type3->add($type3_attribute);

        return array($type1, $type2, $type3);
    }

    public function getParseTree() {
        $tree = new RootNode();
        $tree->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');

        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );
        /*
           <xs:element xmlns:wsdl11="http://schemas.xmlsoap.org/wsdl/"
                       xmlns:soap11enc="http://schemas.xmlsoap.org/soap/encoding/"
                       name="attributeFormUnqualified"
                       type="ex:AttributeFormUnqualified"/>
         */
        $node1_element = new ElementNode(array('name' => 'attributeFormUnqualified', 'type' => 'AttributeFormUnqualified'), 0);
        $node1_element->setNamespaces($namespaces);
        /*
           <xs:complexType xmlns:wsdl11="http://schemas.xmlsoap.org/wsdl/"
                           xmlns:soap11enc="http://schemas.xmlsoap.org/soap/encoding/"
                           name="AttributeFormUnqualified">
                <xs:sequence>
                  <xs:element name="premium" type="xs:string"/>
                </xs:sequence>
                <xs:attribute name="id" type="xs:string" form="unqualified"/>
              </xs:complexType>
         */
        $node2_complexType = new ComplexTypeNode(array('name' => 'AttributeFormUnqualified'), 0);
        $node2_complexType->setNamespaces($namespaces);
        $node2_sequence = new SequenceNode(array(), 1);
        $node2_sequence->setNamespaces($namespaces);
        $node2_element = new ElementNode(array('name' => 'premium', 'type' => 'string'), 2);
        $node2_element->setNamespaces($namespaces);
        $node2_attribute = new AttributeNode(array('name' => 'id', 'type' => 'string', 'form' => 'unqualified', 'use'=>'required'), 1);
        $node2_attribute->setNamespaces($namespaces);

        $node2_sequence->add($node2_element);
        $node2_complexType->add($node2_sequence);
        $node2_complexType->add($node2_attribute);

        /*
           <xs:element name="echoAttributeFormUnqualified">
              <xs:complexType>
                 <xs:sequence>
                    <xs:element ref="ex:attributeFormUnqualified"/>
                 </xs:sequence>
              </xs:complexType>
           </xs:element>
         */
        $node3_element = new ElementNode(array('name' => 'echoAttributeFormUnqualified'), 0);
        $node3_element->setNamespaces($namespaces);
        $node3_complexType = new ComplexTypeNode(array(), 1);
        $node3_complexType->setNamespaces($namespaces);
        $node3_sequence = new SequenceNode(array(), 2);
        $node3_sequence->setNamespaces($namespaces);
        $node3_element1 = new ElementNode(array('type' => 'attributeFormUnqualified'), 3);
        $node3_element1->setNamespaces($namespaces);

        $node3_sequence->add($node3_element1);
        $node3_complexType->add($node3_sequence);
        $node3_element->add($node3_complexType);

        $tree->add($node1_element);
        $tree->add($node2_complexType);
        $tree->add($node3_element);

        return $tree;
    }
}