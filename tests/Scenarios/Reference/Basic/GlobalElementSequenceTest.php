<?php
namespace PXDB\Tests\Scenarios\Reference;

use PXDB\AST\Type;
use PXDB\AST\TypeAttribute;
use PXDB\ParseTree\ComplexTypeNode;
use PXDB\ParseTree\ElementNode;
use PXDB\ParseTree\RootNode;
use PXDB\ParseTree\SequenceNode;


class GlobalElementSequenceTest
    extends ReferenceTest
{
    public function setUp() {
        $this->pathToTestFiles = dirname(__FILE__) . '/../../../_files/Reference/Basic/GlobalElementSequence';
        $this->schemaFile = 'GlobalElementSequence.xsd';
    }

    public function getASTs() {
        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        $type1 = new Type('globalElementSequence');
        $type1->setAsRoot();
        $type1->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type1->setNamespaces($namespaces);
        $attribute1_1 = new TypeAttribute('foo', 'string');
        $attribute1_2 = new TypeAttribute('bar', 'string');

        $type2 = new Type('echoGlobalElementSequence');
        $type2->setAsRoot();
        $type2->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type2->setNamespaces($namespaces);

        $attribute = new TypeAttribute('', 'globalElementSequence');

        $type1->add($attribute1_1);
        $type1->add($attribute1_2);
        $type2->add($attribute);

        return array($type1, $type2);
    }

    public function getParseTree() {
        $tree = new RootNode();
        $tree->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');

        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        $element1 = new ElementNode(array('name' => 'globalElementSequence'), 0);
        $element1->setNamespaces($namespaces);
        $complexType1 = new ComplexTypeNode(array(), 1);
        $complexType1->setNamespaces($namespaces);
        $sequence1 = new SequenceNode(array(), 2);
        $sequence1->setNamespaces($namespaces);
        $element1_1 = new ElementNode(array('name' => 'foo', 'type' => 'string'), 3);
        $element1_1->setNamespaces($namespaces);

        $element1_2 = new ElementNode(array('name' => 'bar', 'type' => 'string'), 3);
        $element1_2->setNamespaces($namespaces);

        $element2 = new ElementNode(array('name' => 'echoGlobalElementSequence'), 0);
        $element2->setNamespaces($namespaces);
        $complexType2 = new ComplexTypeNode(array(), 1);
        $complexType2->setNamespaces($namespaces);
        $sequence2 = new SequenceNode(array(), 2);
        $sequence2->setNamespaces($namespaces);
        $element3 = new ElementNode(array('type' => 'globalElementSequence'), 3);
        $element3->setNamespaces($namespaces);

        $sequence1->add($element1_1);
        $sequence1->add($element1_2);
        $complexType1->add($sequence1);
        $element1->add($complexType1);

        $sequence2->add($element3);
        $complexType2->add($sequence2);
        $element2->add($complexType2);

        $tree->add($element1);
        $tree->add($element2);

        return $tree;
    }
}