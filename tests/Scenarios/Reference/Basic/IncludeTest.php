<?php
namespace PXDB\Tests\Scenarios\Reference;

use PXDB\AST\Type;
use PXDB\AST\TypeAttribute;
use PXDB\ParseTree\ComplexTypeNode;
use PXDB\ParseTree\ElementNode;
use PXDB\ParseTree\RootNode;
use PXDB\ParseTree\SequenceNode;


class IncludeTest
    extends ReferenceTest
{
    public function setUp() {
        $this->pathToTestFiles = dirname(__FILE__) . '/../../../_files/Reference/Basic/Include';
        $this->schemaFile = 'Include.xsd';
    }

    public function getASTs() {
        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );
        $includedNamespaces = array(
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
        );

        $type1 = new Type('echoInclude');
        $type1->setAsRoot();
        $type1->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type1->setNamespaces($namespaces);
        $type1_attribute = new TypeAttribute('', 'include');

        $type1->add($type1_attribute);


        $type2 = new Type('include', 'string');
        $type2->setAsRoot();
        $type2->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type2->setNamespaces($includedNamespaces);


        return array($type1, $type2);
    }

    public function getParseTree() {
        $tree = new RootNode();
        $tree->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');

        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );
        $includedNamespaces = array(
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
        );
        /*
       <xs:element name="echoInclude">
          <xs:complexType>
             <xs:sequence>
                <xs:element ref="ex:include"/>
             </xs:sequence>
          </xs:complexType>

       </xs:element>
        */
        $node1_element = new ElementNode(array('name' => 'echoInclude'), 0);
        $node1_element->setNamespaces($namespaces);
        $node1_complexType = new ComplexTypeNode(array(), 1);
        $node1_complexType->setNamespaces($namespaces);
        $node1_sequence = new SequenceNode(array(), 2);
        $node1_sequence->setNamespaces($namespaces);
        $node1_element1 = new ElementNode(array('type' => 'include'), 3);
        $node1_element1->setNamespaces($namespaces);

        $node1_sequence->add($node1_element1);
        $node1_complexType->add($node1_sequence);
        $node1_element->add($node1_complexType);

        /*
           <xs:element name="include" type="xs:string"/>
        */
        $node2_element = new ElementNode(array('name' => 'include', 'type' => 'string'), 0);
        $node2_element->setNamespaces($includedNamespaces);

        $tree->add($node1_element);
        $tree->add($node2_element);

        return $tree;
    }
}