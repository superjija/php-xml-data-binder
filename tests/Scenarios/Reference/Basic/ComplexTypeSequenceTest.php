<?php
namespace PXDB\Tests\Scenarios\Reference;

use PXDB\AST\Enumeration;
use PXDB\AST\EnumerationValue;
use PXDB\AST\Structure;
use PXDB\AST\StructureElement;
use PXDB\AST\StructureType;
use PXDB\AST\Type;
use PXDB\AST\TypeAttribute;
use PXDB\ParseTree\AttributeNode;
use PXDB\ParseTree\ChoiceNode;
use PXDB\ParseTree\ComplexContentNode;
use PXDB\ParseTree\ComplexTypeNode;
use PXDB\ParseTree\ElementNode;
use PXDB\ParseTree\EnumerationNode;
use PXDB\ParseTree\ExtensionNode;
use PXDB\ParseTree\RestrictionNode;
use PXDB\ParseTree\RootNode;
use PXDB\ParseTree\SequenceNode;
use PXDB\ParseTree\SimpleTypeNode;


class ComplexTypeSequenceTest
    extends ReferenceTest
{
    public function setUp() {
        $this->pathToTestFiles = dirname(__FILE__) . '/../../../_files/Reference/Basic/ComplexTypeSequence';
        $this->schemaFile = 'ComplexTypeSequence.xsd';
    }

    public function getASTs() {
        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        $typeReferencedElement = new Type('complexTypeSequence', 'ComplexTypeSequence');
        $typeReferencedElement->setAsRoot();
        $typeReferencedElement->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $typeReferencedElement->setNamespaces($namespaces);

        $typeElement = new Type('ComplexTypeSequence');
        $typeElement->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $typeElement->setNamespaces($namespaces);
        $attribute = new TypeAttribute('name', 'string');
        $typeElement->add($attribute);

        $attribute = new TypeAttribute('shade', 'string');
        $typeElement->add($attribute);

        $attribute = new TypeAttribute('length', 'int');
        $typeElement->add($attribute);

        $attribute = new TypeAttribute('id', 'string');
        $attribute->setStyle('attribute');
        $typeElement->add($attribute);

        $attribute = new TypeAttribute('inStock', 'int', true);
        $attribute->setStyle('attribute');
        $typeElement->add($attribute);

        $typeEchoElement = new Type('echoComplexTypeSequence');
        $typeEchoElement->setAsRoot();
        $typeEchoElement->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $typeEchoElement->setNamespaces($namespaces);
        $typeEchoElementAttribute1 = new TypeAttribute('', 'complexTypeSequence');


        $typeEchoElement->add($typeEchoElementAttribute1);

        return array($typeReferencedElement, $typeElement, $typeEchoElement);
    }

    public function getParseTree() {
        $tree = new RootNode();
        $tree->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');

        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );
        /*
           <xs:element xmlns:wsdl11="http://schemas.xmlsoap.org/wsdl/"
                       xmlns:soap11enc="http://schemas.xmlsoap.org/soap/encoding/"
                       name="complexTypeSequence"
                       type="ex:ComplexTypeSequence"/>
         */
        $element1 = new ElementNode(array('name' => 'complexTypeSequence', 'type' => 'ComplexTypeSequence'), 0);
        $element1->setNamespaces($namespaces);
        /*
           <xs:complexType xmlns:wsdl11="http://schemas.xmlsoap.org/wsdl/"
                           xmlns:soap11enc="http://schemas.xmlsoap.org/soap/encoding/"
                           name="ComplexTypeSequence">
                <xs:sequence>
                  <xs:element name="name" type="xs:string"/>
                  <xs:element name="shade" type="xs:string"/>
                  <xs:element name="length" type="xs:int"/>
                </xs:sequence>

                <xs:attribute name="id" type="xs:string"/>
                <xs:attribute name="inStock" type="xs:int"/>
              </xs:complexType>

         */
        $complexType = new ComplexTypeNode(array('name' => 'ComplexTypeSequence'), 0);
        $complexType->setNamespaces($namespaces);
        $sequence = new SequenceNode(array(), 1);
        $sequence->setNamespaces($namespaces);
        $elementNode = new ElementNode(array('name' => 'name', 'type' => 'string'), 2);
        $elementNode->setNamespaces($namespaces);
        $sequence->add($elementNode);

        $elementNode = new ElementNode(array('name' => 'shade', 'type' => 'string'), 2);
        $elementNode->setNamespaces($namespaces);
        $sequence->add($elementNode);

        $elementNode = new ElementNode(array('name' => 'length', 'type' => 'int'), 2);
        $elementNode->setNamespaces($namespaces);
        $sequence->add($elementNode);
        $complexType->add($sequence);
        $attribute = new AttributeNode(array('name' => 'id', 'type' => 'string', 'use' => 'required'), 1);
        $attribute->setNamespaces($namespaces);
        $complexType->add($attribute);

        $attribute = new AttributeNode(array('name' => 'inStock', 'type' => 'int'), 1);
        $attribute->setNamespaces($namespaces);
        $complexType->add($attribute);
        /*
           <xs:element name="echoComplexTypeSequence">
              <xs:complexType>
                 <xs:sequence>
                    <xs:element ref="ex:complexTypeSequence"/>
                 </xs:sequence>
              </xs:complexType>

           </xs:element>
         */
        $element2 = new ElementNode(array('name' => 'echoComplexTypeSequence'), 0);
        $element2->setNamespaces($namespaces);
        $complexType1 = new ComplexTypeNode(array(), 1);
        $complexType1->setNamespaces($namespaces);
        $sequence1 = new SequenceNode(array(), 2);
        $sequence1->setNamespaces($namespaces);
        $element3 = new ElementNode(array('type' => 'complexTypeSequence'), 3);
        $element3->setNamespaces($namespaces);

        $sequence1->add($element3);
        $complexType1->add($sequence1);
        $element2->add($complexType1);

        $tree->add($element1);
        $tree->add($complexType);
        $tree->add($element2);

        return $tree;
    }
}