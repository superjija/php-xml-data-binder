<?php
namespace PXDB\Tests\Scenarios\Reference;

use PXDB\AST\Type;
use PXDB\AST\TypeAttribute;
use PXDB\ParseTree\ComplexTypeNode;
use PXDB\ParseTree\ElementNode;
use PXDB\ParseTree\RootNode;
use PXDB\ParseTree\SequenceNode;


class ElementTypeReferenceTest
    extends ReferenceTest
{
    public function setUp() {
        $this->pathToTestFiles = dirname(__FILE__) . '/../../../_files/Reference/Basic/ElementTypeReference';
        $this->schemaFile = 'ElementTypeReference.xsd';
    }

    public function getASTs() {
        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        $type1 = new Type('elementTypeReference', 'ElementTypeReference');
        $type1->setAsRoot();
        $type1->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type1->setNamespaces($namespaces);


        $type2 = new Type('ElementTypeReferenced', '');
        $type2->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type2->setNamespaces($namespaces);
        $type2_attribute = new TypeAttribute('referenced', 'string');

        $type2->add($type2_attribute);


        $type3 = new Type('ElementTypeReference');
        $type3->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type3->setNamespaces($namespaces);
        $type3_attribute1 = new TypeAttribute('text', 'ElementTypeReferenced');

        $type3->add($type3_attribute1);


        $type4 = new Type('echoElementTypeReference');
        $type4->setAsRoot();
        $type4->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type4->setNamespaces($namespaces);
        $type4_attribute = new TypeAttribute('', 'elementTypeReference');

        $type4->add($type4_attribute);

        return array($type1, $type2, $type3, $type4);
    }

    public function getParseTree() {
        $tree = new RootNode();
        $tree->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');

        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        /*
       <xs:element xmlns:wsdl11="http://schemas.xmlsoap.org/wsdl/"
                   xmlns:soap11enc="http://schemas.xmlsoap.org/soap/encoding/"
                   name="elementTypeReference"
                   type="ex:ElementTypeReference"/>
        */
        $node1_element = new ElementNode(array('name' => 'elementTypeReference', 'type' => 'ElementTypeReference'), 0);
        $node1_element->setNamespaces($namespaces);

        /*
       <xs:complexType xmlns:wsdl11="http://schemas.xmlsoap.org/wsdl/"
                       xmlns:soap11enc="http://schemas.xmlsoap.org/soap/encoding/"
                       name="ElementTypeReferenced">
            <xs:sequence>
              <xs:element name="referenced" type="xs:string"/>
            </xs:sequence>
          </xs:complexType>
        */
        $node2_complexType = new ComplexTypeNode(array('name' => 'ElementTypeReferenced'), 0);
        $node2_complexType->setNamespaces($namespaces);
        $node2_sequence = new SequenceNode(array(), 1);
        $node2_sequence->setNamespaces($namespaces);
        $node2_element = new ElementNode(array('name' => 'referenced', 'type' => 'string'), 2);
        $node2_element->setNamespaces($namespaces);

        $node2_sequence->add($node2_element);
        $node2_complexType->add($node2_sequence);

        /*
       <xs:complexType xmlns:wsdl11="http://schemas.xmlsoap.org/wsdl/"
                       xmlns:soap11enc="http://schemas.xmlsoap.org/soap/encoding/"
                       name="ElementTypeReference">

            <xs:sequence>
              <xs:element name="text" type="ex:ElementTypeReferenced"/>
            </xs:sequence>
          </xs:complexType>
        */
        $node3_complexType = new ComplexTypeNode(array('name' => 'ElementTypeReference'),0);
        $node3_complexType->setNamespaces($namespaces);
        $node3_sequence = new SequenceNode(array(), 1);
        $node3_sequence->setNamespaces($namespaces);
        $node3_element1 = new ElementNode(array('name' => 'text', 'type' => 'ElementTypeReferenced'), 2);
        $node3_element1->setNamespaces($namespaces);

        $node3_sequence->add($node3_element1);
        $node3_complexType->add($node3_sequence);

        /*
       <xs:element name="echoElementTypeReference">
          <xs:complexType>
             <xs:sequence>
                <xs:element ref="ex:elementTypeReference"/>
             </xs:sequence>

          </xs:complexType>
       </xs:element>
        */
        $node4_element = new ElementNode(array('name' => 'echoElementTypeReference'), 0);
        $node4_element->setNamespaces($namespaces);
        $node4_complexType = new ComplexTypeNode(array(), 1);
        $node4_complexType->setNamespaces($namespaces);
        $node4_sequence = new SequenceNode(array(), 2);
        $node4_sequence->setNamespaces($namespaces);
        $node4_element1 = new ElementNode(array('type' => 'elementTypeReference'), 3);
        $node4_element1->setNamespaces($namespaces);

        $node4_sequence->add($node4_element1);
        $node4_complexType->add($node4_sequence);
        $node4_element->add($node4_complexType);


        $tree->add($node1_element);
        $tree->add($node2_complexType);
        $tree->add($node3_complexType);
        $tree->add($node4_element);

        return $tree;
    }
}