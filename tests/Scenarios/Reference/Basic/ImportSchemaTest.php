<?php
namespace PXDB\Tests\Scenarios\Reference;

use PXDB\CodeGen\SchemaParser;
use PXDB\Tests\ExtendedTestCase;
use RuntimeException;


class ImportSchemaTest
    extends ExtendedTestCase
{
    /**
     * @expectedException RuntimeException
     */
    public function testSchemaShouldThrowException() {
        $pathToTestFiles = dirname(__FILE__) . '/../../../_files/Reference/Basic/ImportSchema';
        $schemaFile = 'ImportSchema.xsd';

        $schemaFile = $pathToTestFiles . DIRECTORY_SEPARATOR . $schemaFile;

        $parser = new SchemaParser($schemaFile);
        $parser->parse();
    }
}