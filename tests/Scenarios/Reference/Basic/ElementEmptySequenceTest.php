<?php
namespace PXDB\Tests\Scenarios\Reference;

use PXDB\AST\Enumeration;
use PXDB\AST\EnumerationValue;
use PXDB\AST\Structure;
use PXDB\AST\StructureElement;
use PXDB\AST\StructureType;
use PXDB\AST\Type;
use PXDB\AST\TypeAttribute;
use PXDB\ParseTree\AttributeNode;
use PXDB\ParseTree\ChoiceNode;
use PXDB\ParseTree\ComplexContentNode;
use PXDB\ParseTree\ComplexTypeNode;
use PXDB\ParseTree\ElementNode;
use PXDB\ParseTree\EnumerationNode;
use PXDB\ParseTree\ExtensionNode;
use PXDB\ParseTree\RestrictionNode;
use PXDB\ParseTree\RootNode;
use PXDB\ParseTree\SequenceNode;
use PXDB\ParseTree\SimpleTypeNode;


class ElementEmptySequenceTest
    extends ReferenceTest
{
    public function setUp() {
        $this->pathToTestFiles = dirname(__FILE__) . '/../../../_files/Reference/Basic/ElementEmptySequence';
        $this->schemaFile = 'ElementEmptySequence.xsd';
    }

    public function getASTs() {
        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        $typeReferencedElement = new Type('elementEmptySequence');
        $typeReferencedElement->setAsRoot();
        $typeReferencedElement->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $typeReferencedElement->setNamespaces($namespaces);

        $typeElement = new Type('echoElementEmptySequence');
        $typeElement->setAsRoot();
        $typeElement->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $typeElement->setNamespaces($namespaces);

        $attribute = new TypeAttribute('', 'elementEmptySequence');

        $typeElement->add($attribute);

        return array($typeReferencedElement, $typeElement);
    }

    public function getParseTree() {
        $tree = new RootNode();
        $tree->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');

        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        $element1 = new ElementNode(array('name' => 'elementEmptySequence'), 0);
        $element1->setNamespaces($namespaces);
        $complexType1 = new ComplexTypeNode(array(), 1);
        $complexType1->setNamespaces($namespaces);
        $sequence1 = new SequenceNode(array(), 2);
        $sequence1->setNamespaces($namespaces);

        $element2 = new ElementNode(array('name' => 'echoElementEmptySequence'), 0);
        $element2->setNamespaces($namespaces);
        $complexType2 = new ComplexTypeNode(array(), 1);
        $complexType2->setNamespaces($namespaces);
        $sequence2 = new SequenceNode(array(), 2);
        $sequence2->setNamespaces($namespaces);
        $element3 = new ElementNode(array('type' => 'elementEmptySequence'), 3);
        $element3->setNamespaces($namespaces);

        $complexType1->add($sequence1);
        $element1->add($complexType1);

        $sequence2->add($element3);
        $complexType2->add($sequence2);
        $element2->add($complexType2);

        $tree->add($element1);
        $tree->add($element2);

        return $tree;
    }
}