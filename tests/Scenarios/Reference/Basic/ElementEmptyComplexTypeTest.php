<?php
namespace PXDB\Tests\Scenarios\Reference;

use PXDB\AST\Type;
use PXDB\AST\TypeAttribute;
use PXDB\ParseTree\ComplexTypeNode;
use PXDB\ParseTree\ElementNode;
use PXDB\ParseTree\RootNode;
use PXDB\ParseTree\SequenceNode;


class ElementEmptyComplexTypeTest
    extends ReferenceTest
{
    public function setUp() {
        $this->pathToTestFiles = dirname(__FILE__) . '/../../../_files/Reference/Basic/ElementEmptyComplexType';
        $this->schemaFile = 'ElementEmptyComplexType.xsd';
    }

    public function getASTs() {
        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        $typeReferencedElement = new Type('elementEmptyComplexType');
        $typeReferencedElement->setAsRoot();
        $typeReferencedElement->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $typeReferencedElement->setNamespaces($namespaces);

        $typeElement = new Type('echoElementEmptyComplexType');
        $typeElement->setAsRoot();
        $typeElement->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $typeElement->setNamespaces($namespaces);

        $attribute = new TypeAttribute('', 'elementEmptyComplexType');

        $typeElement->add($attribute);

        return array($typeReferencedElement, $typeElement);
    }

    public function getParseTree() {
        $tree = new RootNode();
        $tree->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');

        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        $element1 = new ElementNode(array('name' => 'elementEmptyComplexType'), 0);
        $element1->setNamespaces($namespaces);
        $complexType1 = new ComplexTypeNode(array(), 1);
        $complexType1->setNamespaces($namespaces);

        $element2 = new ElementNode(array('name' => 'echoElementEmptyComplexType'), 0);
        $element2->setNamespaces($namespaces);
        $complexType2 = new ComplexTypeNode(array(), 1);
        $complexType2->setNamespaces($namespaces);
        $sequence = new SequenceNode(array(), 2);
        $sequence->setNamespaces($namespaces);
        $element3 = new ElementNode(array('type' => 'elementEmptyComplexType'), 3);
        $element3->setNamespaces($namespaces);

        $element1->add($complexType1);

        $sequence->add($element3);
        $complexType2->add($sequence);
        $element2->add($complexType2);

        $tree->add($element1);
        $tree->add($element2);

        return $tree;
    }
}