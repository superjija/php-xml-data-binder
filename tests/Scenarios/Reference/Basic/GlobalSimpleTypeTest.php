<?php
namespace PXDB\Tests\Scenarios\Reference;

use PXDB\AST\Enumeration;
use PXDB\AST\EnumerationValue;
use PXDB\AST\Type;
use PXDB\AST\TypeAttribute;
use PXDB\ParseTree\ComplexTypeNode;
use PXDB\ParseTree\ElementNode;
use PXDB\ParseTree\EnumerationNode;
use PXDB\ParseTree\RestrictionNode;
use PXDB\ParseTree\RootNode;
use PXDB\ParseTree\SequenceNode;
use PXDB\ParseTree\SimpleTypeNode;


class GlobalSimpleTypeTest
    extends ReferenceTest
{
    public function setUp() {
        $this->pathToTestFiles = dirname(__FILE__) . '/../../../_files/Reference/Basic/GlobalSimpleType';
        $this->schemaFile = 'GlobalSimpleType.xsd';
    }

    public function getASTs() {
        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        $typeReferencedElement = new Type('globalSimpleType', 'GlobalSimpleType');
        $typeReferencedElement->setAsRoot();
        $typeReferencedElement->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $typeReferencedElement->setNamespaces($namespaces);

        $typeElement = new Type('GlobalSimpleType','','string');
        $typeElement->setAsRoot();
        $typeElement->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $typeElement->setNamespaces($namespaces);
        $enumeration = new Enumeration();
        $enum = new EnumerationValue('foo', 'string');

        $typeEchoElement = new Type('echoGlobalSimpleType');
        $typeEchoElement->setAsRoot();
        $typeEchoElement->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $typeEchoElement->setNamespaces($namespaces);
        $typeEchoElementAttribute1 = new TypeAttribute('', 'globalSimpleType');
        $typeEchoElementAttribute1->setStyle('element');

        $enumeration->add($enum);
        $typeElement->add($enumeration);

        $typeEchoElement->add($typeEchoElementAttribute1);

        return array($typeReferencedElement, $typeElement, $typeEchoElement);
    }

    public function getParseTree() {
        $tree = new RootNode();
        $tree->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');

        $options = array(
            'name' => 'stringElement',
            'type' => ''
        );
        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        $element1 = new ElementNode(array('name' => 'globalSimpleType', 'type' => 'GlobalSimpleType'), 0);
        $element1->setNamespaces($namespaces);

        $simpleType1 = new SimpleTypeNode(array('name' => 'GlobalSimpleType'), 0);
        $simpleType1->setNamespaces($namespaces);
        $restriction = new RestrictionNode(array('base' => 'string'), 1);
        $restriction->setNamespaces($namespaces);
        $enumeration = new EnumerationNode(array('value' => 'foo'), 2);
        $enumeration->setNamespaces($namespaces);

        $element2 = new ElementNode(array('name' => 'echoGlobalSimpleType'), 0);
        $element2->setNamespaces($namespaces);
        $complexType1 = new ComplexTypeNode(array(), 1);
        $complexType1->setNamespaces($namespaces);
        $sequence1 = new SequenceNode(array(), 2);
        $sequence1->setNamespaces($namespaces);
        $element3 = new ElementNode(array('type' => 'globalSimpleType'), 3);
        $element3->setNamespaces($namespaces);

        $restriction->add($enumeration);
        $simpleType1->add($restriction);

        $sequence1->add($element3);
        $complexType1->add($sequence1);
        $element2->add($complexType1);

        $tree->add($element1);
        $tree->add($simpleType1);
        $tree->add($element2);

        return $tree;
    }
}