<?php
namespace PXDB\Tests\Scenarios\Reference;

use PXDB\AST\Structure;
use PXDB\AST\StructureElement;
use PXDB\AST\Type;
use PXDB\AST\TypeAttribute;
use PXDB\ParseTree\ComplexTypeNode;
use PXDB\ParseTree\ElementNode;
use PXDB\ParseTree\RootNode;
use PXDB\ParseTree\SequenceNode;


class GlobalElementComplexTypeTest
    extends ReferenceTest
{
    public function setUp() {
        $this->pathToTestFiles = dirname(__FILE__) . '/../../../_files/Reference/Basic/GlobalElementComplexType';
        $this->schemaFile = 'GlobalElementComplexType.xsd';
    }

    public function getASTs() {
        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        $type1 = new Type('globalElementComplexType');
        $type1->setAsRoot();
        $type1->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type1->setNamespaces($namespaces);
        $type1_structure = new Structure('name');
        $type1_structure_element1 = new StructureElement('firstName', 'string');
        $type1_structure_element2 = new StructureElement('lastName', 'string');

        $type1_structure->add($type1_structure_element1);
        $type1_structure->add($type1_structure_element2);
        $type1->add($type1_structure);

        $type2 = new Type('echoGlobalElementComplexType');
        $type2->setAsRoot();
        $type2->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type2->setNamespaces($namespaces);
        $type2_attribute = new TypeAttribute('', 'globalElementComplexType');

        $type2->add($type2_attribute);

        return array($type1, $type2);
    }

    public function getParseTree() {
        $tree = new RootNode();
        $tree->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');

        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        /*
       <xs:element xmlns:wsdl11="http://schemas.xmlsoap.org/wsdl/"
                   xmlns:soap11enc="http://schemas.xmlsoap.org/soap/encoding/"
                   name="globalElementComplexType">
          <xs:complexType>
            <xs:sequence>
                <xs:element name="name">
                   <xs:complexType>
                      <xs:sequence>
                         <xs:element name="firstName" type="xs:string"/>

                         <xs:element name="lastName" type="xs:string"/>
                      </xs:sequence>
                   </xs:complexType>
                </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
         */
        $node1_element = new ElementNode(array('name' => 'globalElementComplexType'), 0);
        $node1_element->setNamespaces($namespaces);
        $node1_complexType = new ComplexTypeNode(array(), 1);
        $node1_complexType->setNamespaces($namespaces);
        $node1_sequence = new SequenceNode(array(), 2);
        $node1_sequence->setNamespaces($namespaces);
        $node1_element1 = new ElementNode(array('name' => 'name'), 3);
        $node1_element1->setNamespaces($namespaces);
        $node1_complexType1 = new ComplexTypeNode(array(), 4);
        $node1_complexType1->setNamespaces($namespaces);
        $node1_sequence1 = new SequenceNode(array(), 5);
        $node1_sequence1->setNamespaces($namespaces);
        $node1_element2 = new ElementNode(array('name' => 'firstName', 'type' => 'string'), 6);
        $node1_element2->setNamespaces($namespaces);
        $node1_element3 = new ElementNode(array('name' => 'lastName', 'type' => 'string'), 6);
        $node1_element3->setNamespaces($namespaces);

        $node1_sequence1->add($node1_element2);
        $node1_sequence1->add($node1_element3);
        $node1_complexType1->add($node1_sequence1);
        $node1_element1->add($node1_complexType1);
        $node1_sequence->add($node1_element1);
        $node1_complexType->add($node1_sequence);
        $node1_element->add($node1_complexType);

        /*
       <xs:element name="echoGlobalElementComplexType">
          <xs:complexType>

             <xs:sequence>
                <xs:element ref="ex:globalElementComplexType"/>
             </xs:sequence>
          </xs:complexType>
       </xs:element>
         */
        $node2_element = new ElementNode(array('name' => 'echoGlobalElementComplexType'), 0);
        $node2_element->setNamespaces($namespaces);
        $node2_complexType = new ComplexTypeNode(array(), 1);
        $node2_complexType->setNamespaces($namespaces);
        $node2_sequence = new SequenceNode(array(), 2);
        $node2_sequence->setNamespaces($namespaces);
        $node2_element1 = new ElementNode(array('type' => 'globalElementComplexType'), 3);
        $node2_element1->setNamespaces($namespaces);

        $node2_sequence->add($node2_element1);
        $node2_complexType->add($node2_sequence);
        $node2_element->add($node2_complexType);

        $tree->add($node1_element);
        $tree->add($node2_element);

        return $tree;
    }
}