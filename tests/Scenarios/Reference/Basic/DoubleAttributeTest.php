<?php
namespace PXDB\Tests\Scenarios\Reference;

use PXDB\AST\Type;
use PXDB\AST\TypeAttribute;
use PXDB\ParseTree\AttributeNode;
use PXDB\ParseTree\ComplexTypeNode;
use PXDB\ParseTree\ElementNode;
use PXDB\ParseTree\RootNode;
use PXDB\ParseTree\SequenceNode;


class DoubleAttributeTest
    extends ReferenceTest
{
    public function setUp() {
        $this->pathToTestFiles = dirname(__FILE__) . '/../../../_files/Reference/Basic/DoubleAttribute';
        $this->schemaFile = 'DoubleAttribute.xsd';
    }

    public function getASTs() {
        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        $typeReferencedElement = new Type('doubleAttribute', 'DoubleAttribute');
        $typeReferencedElement->setAsRoot();
        $typeReferencedElement->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $typeReferencedElement->setNamespaces($namespaces);

        $typeElement = new Type('DoubleAttribute');
        $typeElement->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $typeElement->setNamespaces($namespaces);
        $typeElementAttribute1 = new TypeAttribute('text', 'string', true);
        $typeElementAttribute1->setStyle('element');

        $typeElementAttribute2 = new TypeAttribute('double', 'double', true);
        $typeElementAttribute2->setStyle('attribute');

        $typeEchoElement = new Type('echoDoubleAttribute');
        $typeEchoElement->setAsRoot();
        $typeEchoElement->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $typeEchoElement->setNamespaces($namespaces);
        $typeEchoElementAttribute1 = new TypeAttribute('', 'doubleAttribute');
        $typeEchoElementAttribute1->setStyle('element');


        $typeElement->add($typeElementAttribute1);
        $typeElement->add($typeElementAttribute2);

        $typeEchoElement->add($typeEchoElementAttribute1);

        return array($typeReferencedElement, $typeElement, $typeEchoElement);
    }

    public function getParseTree() {
        $tree = new RootNode();
        $tree->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');

        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        $element1 = new ElementNode(array('name' => 'doubleAttribute', 'type' => 'DoubleAttribute'), 0);
        $element1->setNamespaces($namespaces);
        $complexType1 = new ComplexTypeNode(array('name' => 'DoubleAttribute'), 0);
        $complexType1->setNamespaces($namespaces);
        $sequence1 = new SequenceNode(array(), 1);
        $sequence1->setNamespaces($namespaces);
        $element2 = new ElementNode(array('name' => 'text', 'type' => 'string', 'minOccurs' => '0'), 2);
        $element2->setNamespaces($namespaces);
        $attribute = new AttributeNode(array('name' => 'double', 'type' => 'double'), 1);
        $attribute->setNamespaces($namespaces);
        $element3 = new ElementNode(array('name' => 'echoDoubleAttribute'), 0);
        $element3->setNamespaces($namespaces);
        $complexType2 = new ComplexTypeNode(array(), 1);
        $complexType2->setNamespaces($namespaces);
        $sequence2 = new SequenceNode(array(), 2);
        $sequence2->setNamespaces($namespaces);
        $element4 = new ElementNode(array('type' => 'doubleAttribute'), 3);
        $element4->setNamespaces($namespaces);

        $sequence1->add($element2);
        $complexType1->add($sequence1);
        $complexType1->add($attribute);

        $sequence2->add($element4);
        $complexType2->add($sequence2);
        $element3->add($complexType2);

        $tree->add($element1);
        $tree->add($complexType1);
        $tree->add($element3);

        return $tree;
    }
}