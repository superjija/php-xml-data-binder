<?php
namespace PXDB\Tests\Scenarios\Reference;

use PXDB\AST\Enumeration;
use PXDB\AST\EnumerationValue;
use PXDB\AST\Type;
use PXDB\AST\TypeAttribute;
use PXDB\ParseTree\AttributeNode;
use PXDB\ParseTree\ComplexTypeNode;
use PXDB\ParseTree\ElementNode;
use PXDB\ParseTree\EnumerationNode;
use PXDB\ParseTree\RestrictionNode;
use PXDB\ParseTree\RootNode;
use PXDB\ParseTree\SequenceNode;
use PXDB\ParseTree\SimpleTypeNode;


class AttributeTypeReferenceTest
    extends ReferenceTest
{
    public function setUp() {
        $this->pathToTestFiles = dirname(__FILE__) . '/../../../_files/Reference/Basic/AttributeTypeReference';
        $this->schemaFile = 'AttributeTypeReference.xsd';
    }

    public function getASTs() {
        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        $type1 = new Type('attributeTypeReference', 'AttributeTypeReference');
        $type1->setAsRoot();
        $type1->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type1->setNamespaces($namespaces);


        $type2 = new Type('SimpleType', '', 'string');
        $type2->setAsRoot();
        $type2->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type2->setNamespaces($namespaces);
        $type2_enumeration = new Enumeration();
        $type2_enumeration_value1 = new EnumerationValue('value1', 'string');
        $type2_enumeration_value2 = new EnumerationValue('value2', 'string');

        $type2_enumeration->add($type2_enumeration_value1);
        $type2_enumeration->add($type2_enumeration_value2);
        $type2->add($type2_enumeration);


        $type3 = new Type('AttributeTypeReference');
        $type3->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type3->setNamespaces($namespaces);
        $type3_attribute1 = new TypeAttribute('element', 'string');
        $type3_attribute2 = new TypeAttribute('attr', 'SimpleType');
        $type3_attribute2->setStyle('attribute');

        $type3->add($type3_attribute1);
        $type3->add($type3_attribute2);


        $type4 = new Type('echoAttributeTypeReference');
        $type4->setAsRoot();
        $type4->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type4->setNamespaces($namespaces);
        $type4_attribute = new TypeAttribute('', 'attributeTypeReference');

        $type4->add($type4_attribute);

        return array($type1, $type2, $type3, $type4);
    }

    public function getParseTree() {
        $tree = new RootNode();
        $tree->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');

        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );
        /*
       <xs:element xmlns:wsdl11="http://schemas.xmlsoap.org/wsdl/"
                   xmlns:soap11enc="http://schemas.xmlsoap.org/soap/encoding/"
                   name="attributeTypeReference"
                   type="ex:AttributeTypeReference"/>

        */
        $node1_element = new ElementNode(array('name' => 'attributeTypeReference', 'type' => 'AttributeTypeReference'), 0);
        $node1_element->setNamespaces($namespaces);

        /*
       <xs:simpleType xmlns:wsdl11="http://schemas.xmlsoap.org/wsdl/"
                      xmlns:soap11enc="http://schemas.xmlsoap.org/soap/encoding/"
                      name="SimpleType">
            <xs:restriction base="xs:string">
              <xs:enumeration value="value1"/>
              <xs:enumeration value="value2"/>
            </xs:restriction>
          </xs:simpleType>
        */
        $node2_simpleType = new SimpleTypeNode(array('name' => 'SimpleType', 'use' => 'required'), 0);
        $node2_simpleType->setNamespaces($namespaces);
        $node2_restriction = new RestrictionNode(array('base' => 'string'), 1);
        $node2_restriction->setNamespaces($namespaces);
        $node2_enumeraton1 = new EnumerationNode(array('value' => 'value1'), 2);
        $node2_enumeraton1->setNamespaces($namespaces);
        $node2_enumeraton2 = new EnumerationNode(array('value' => 'value2'), 2);
        $node2_enumeraton2->setNamespaces($namespaces);

        $node2_restriction->add($node2_enumeraton1);
        $node2_restriction->add($node2_enumeraton2);
        $node2_simpleType->add($node2_restriction);

        /*
       <xs:complexType xmlns:wsdl11="http://schemas.xmlsoap.org/wsdl/"
                       xmlns:soap11enc="http://schemas.xmlsoap.org/soap/encoding/"
                       name="AttributeTypeReference">
            <xs:sequence>
              <xs:element name="element" type="xs:string"/>
            </xs:sequence>
            <xs:attribute name="attr" type="ex:SimpleType"/>
          </xs:complexType>
        */
        $node3_complexType = new ComplexTypeNode(array('name' => 'AttributeTypeReference'),0);
        $node3_complexType->setNamespaces($namespaces);
        $node3_sequence = new SequenceNode(array(), 1);
        $node3_sequence->setNamespaces($namespaces);
        $node3_element1 = new ElementNode(array('name' => 'element', 'type' => 'string'), 2);
        $node3_element1->setNamespaces($namespaces);
        $node3_attribute = new AttributeNode(array('name' => 'attr', 'type' => 'SimpleType', 'use' => 'required'), 1);
        $node3_attribute->setNamespaces($namespaces);

        $node3_sequence->add($node3_element1);
        $node3_complexType->add($node3_sequence);
        $node3_complexType->add($node3_attribute);
        /*
       <xs:element name="echoAttributeTypeReference">
          <xs:complexType>
             <xs:sequence>

                <xs:element ref="ex:attributeTypeReference"/>
             </xs:sequence>
          </xs:complexType>
       </xs:element>
        */
        $node4_element = new ElementNode(array('name' => 'echoAttributeTypeReference'), 0);
        $node4_element->setNamespaces($namespaces);
        $node4_complexType = new ComplexTypeNode(array(), 1);
        $node4_complexType->setNamespaces($namespaces);
        $node4_sequence = new SequenceNode(array(), 2);
        $node4_sequence->setNamespaces($namespaces);
        $node4_element1 = new ElementNode(array('type' => 'attributeTypeReference'), 3);
        $node4_element1->setNamespaces($namespaces);

        $node4_sequence->add($node4_element1);
        $node4_complexType->add($node4_sequence);
        $node4_element->add($node4_complexType);


        $tree->add($node1_element);
        $tree->add($node2_simpleType);
        $tree->add($node3_complexType);
        $tree->add($node4_element);

        return $tree;
    }
}