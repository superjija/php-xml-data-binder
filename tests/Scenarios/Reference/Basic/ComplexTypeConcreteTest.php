<?php
namespace PXDB\Tests\Scenarios\Reference;

use PXDB\AST\Type;
use PXDB\AST\TypeAttribute;
use PXDB\ParseTree\ComplexTypeNode;
use PXDB\ParseTree\ElementNode;
use PXDB\ParseTree\RootNode;
use PXDB\ParseTree\SequenceNode;


class ComplexTypeConcreteTest
    extends ReferenceTest
{
    public function setUp() {
        $this->pathToTestFiles = dirname(__FILE__) . '/../../../_files/Reference/Basic/ComplexTypeConcrete';
        $this->schemaFile = 'ComplexTypeConcrete.xsd';
    }

    public function getASTs() {
        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        $type1 = new Type('complexTypeConcrete', 'ComplexTypeConcrete');
        $type1->setAsRoot();
        $type1->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type1->setNamespaces($namespaces);


        $type2 = new Type('ComplexTypeConcrete');
        $type2->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type2->setNamespaces($namespaces);
        $type2_attribute = new TypeAttribute('premium', 'string');

        $type2->add($type2_attribute);


        $type3 = new Type('echoComplexTypeConcrete');
        $type3->setAsRoot();
        $type3->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type3->setNamespaces($namespaces);
        $type3_attribute = new TypeAttribute('', 'complexTypeConcrete');

        $type3->add($type3_attribute);

        return array($type1, $type2, $type3);
    }

    public function getParseTree() {
        $tree = new RootNode();
        $tree->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');

        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );
        /*
           <xs:element xmlns:wsdl11="http://schemas.xmlsoap.org/wsdl/"
                       xmlns:soap11enc="http://schemas.xmlsoap.org/soap/encoding/"
                       name="complexTypeConcrete"
                       type="ex:ComplexTypeConcrete"/>
         */
        $node1_element = new ElementNode(array('name' => 'complexTypeConcrete', 'type' => 'ComplexTypeConcrete'), 0);
        $node1_element->setNamespaces($namespaces);
        /*
           <xs:complexType xmlns:wsdl11="http://schemas.xmlsoap.org/wsdl/"
                           xmlns:soap11enc="http://schemas.xmlsoap.org/soap/encoding/"
                           name="ComplexTypeConcrete"
                           abstract="false">
                <xs:sequence>
                  <xs:element name="premium" type="xs:string"/>
                </xs:sequence>
              </xs:complexType>
         */
        $node2_complexType = new ComplexTypeNode(array('name' => 'ComplexTypeConcrete', 'abstract' => 'false'), 0);
        $node2_complexType->setNamespaces($namespaces);
        $node2_sequence = new SequenceNode(array(), 1);
        $node2_sequence->setNamespaces($namespaces);
        $node2_element = new ElementNode(array('name' => 'premium', 'type' => 'string'), 2);
        $node2_element->setNamespaces($namespaces);

        $node2_sequence->add($node2_element);
        $node2_complexType->add($node2_sequence);

        /*
           <xs:element name="echoComplexTypeConcrete">

              <xs:complexType>
                 <xs:sequence>
                    <xs:element ref="ex:complexTypeConcrete"/>
                 </xs:sequence>
              </xs:complexType>
           </xs:element>
         */
        $node3_element = new ElementNode(array('name' => 'echoComplexTypeConcrete'), 0);
        $node3_element->setNamespaces($namespaces);
        $node3_complexType = new ComplexTypeNode(array(), 1);
        $node3_complexType->setNamespaces($namespaces);
        $node3_sequence = new SequenceNode(array(), 2);
        $node3_sequence->setNamespaces($namespaces);
        $node3_element1 = new ElementNode(array('type' => 'complexTypeConcrete'), 3);
        $node3_element1->setNamespaces($namespaces);

        $node3_sequence->add($node3_element1);
        $node3_complexType->add($node3_sequence);
        $node3_element->add($node3_complexType);

        $tree->add($node1_element);
        $tree->add($node2_complexType);
        $tree->add($node3_element);

        return $tree;
    }
}