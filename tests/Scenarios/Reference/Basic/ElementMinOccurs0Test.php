<?php
namespace PXDB\Tests\Scenarios\Reference;

use PXDB\AST\Type;
use PXDB\AST\TypeAttribute;
use PXDB\ParseTree\ComplexTypeNode;
use PXDB\ParseTree\ElementNode;
use PXDB\ParseTree\RootNode;
use PXDB\ParseTree\SequenceNode;


class ElementMinOccurs0Test
    extends ReferenceTest
{
    public function setUp() {
        $this->pathToTestFiles = dirname(__FILE__) . '/../../../_files/Reference/Basic/ElementMinOccurs0';
        $this->schemaFile = 'ElementMinOccurs0.xsd';
    }

    public function getASTs() {
        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        $typeReferencedElement = new Type('elementMinOccurs0', 'ElementMinOccurs0');
        $typeReferencedElement->setAsRoot();
        $typeReferencedElement->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $typeReferencedElement->setNamespaces($namespaces);

        $typeElement = new Type('ElementMinOccurs0');
        $typeElement->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $typeElement->setNamespaces($namespaces);
        $typeElementAttribute1 = new TypeAttribute('firstName', 'string');
        $typeElementAttribute2 = new TypeAttribute('middleName', 'string', true);
        $typeElementAttribute3 = new TypeAttribute('lastName', 'string');

        $typeEchoElement = new Type('echoElementMinOccurs0');
        $typeEchoElement->setAsRoot();
        $typeEchoElement->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $typeEchoElement->setNamespaces($namespaces);
        $typeEchoElementAttribute1 = new TypeAttribute('', 'elementMinOccurs0');
        $typeEchoElementAttribute1->setStyle('element');


        $typeElement->add($typeElementAttribute1);
        $typeElement->add($typeElementAttribute2);
        $typeElement->add($typeElementAttribute3);

        $typeEchoElement->add($typeEchoElementAttribute1);

        return array($typeReferencedElement, $typeElement, $typeEchoElement);
    }

    public function getParseTree() {
        $tree = new RootNode();
        $tree->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');

        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        $element1 = new ElementNode(array('name' => 'elementMinOccurs0', 'type' => 'ElementMinOccurs0'), 0);
        $element1->setNamespaces($namespaces);
        $complexType1 = new ComplexTypeNode(array('name' => 'ElementMinOccurs0'), 0);
        $complexType1->setNamespaces($namespaces);
        $sequence1 = new SequenceNode(array(), 1);
        $sequence1->setNamespaces($namespaces);
        $elementNode = new ElementNode(array('name' => 'firstName', 'type' => 'string'), 2);
        $elementNode->setNamespaces($namespaces);
        $sequence1->add($elementNode);

        $elementNode = new ElementNode(array('name' => 'middleName', 'type' => 'string', 'minOccurs' => '0'), 2);
        $elementNode->setNamespaces($namespaces);
        $sequence1->add($elementNode);

        $elementNode = new ElementNode(array('name' => 'lastName', 'type' => 'string'), 2);
        $elementNode->setNamespaces($namespaces);
        $sequence1->add($elementNode);
        $element3 = new ElementNode(array('name' => 'echoElementMinOccurs0'), 0);
        $element3->setNamespaces($namespaces);
        $complexType2 = new ComplexTypeNode(array(), 1);
        $complexType2->setNamespaces($namespaces);
        $sequence2 = new SequenceNode(array(), 2);
        $sequence2->setNamespaces($namespaces);
        $element4 = new ElementNode(array('type' => 'elementMinOccurs0'), 3);
        $element4->setNamespaces($namespaces);

        $complexType1->add($sequence1);

        $sequence2->add($element4);
        $complexType2->add($sequence2);
        $element3->add($complexType2);

        $tree->add($element1);
        $tree->add($complexType1);
        $tree->add($element3);

        return $tree;
    }
}