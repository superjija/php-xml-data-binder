<?php
namespace PXDB\Tests\Scenarios\Reference;

use PXDB\AST\Type;
use PXDB\AST\TypeAttribute;
use PXDB\ParseTree\AttributeNode;
use PXDB\ParseTree\ComplexTypeNode;
use PXDB\ParseTree\ElementNode;
use PXDB\ParseTree\RootNode;
use PXDB\ParseTree\SequenceNode;


class AttributeReferenceTest
    extends ReferenceTest
{
    public function setUp() {
        $this->pathToTestFiles = dirname(__FILE__) . '/../../../_files/Reference/Basic/AttributeReference';
        $this->schemaFile = 'AttributeReference.xsd';
    }

    public function getASTs() {
        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        $type1 = new Type('clientName', 'ClientName');
        $type1->setAsRoot();
        $type1->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type1->setNamespaces($namespaces);


        $type2 = new Type('phoneNumber', 'string');
        $type2->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type2->setNamespaces($namespaces);
        $type2->setValueStyle('attribute');

        $type3 = new Type('ClientName');
        $type3->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type3->setNamespaces($namespaces);
        $type3_attribute1 = new TypeAttribute('firstName', 'string');
        $type3_attribute2 = new TypeAttribute('lastName', 'string');
        $type3_attribute3 = new TypeAttribute('', 'phoneNumber', TRUE);
        $type3_attribute3->setStyle('attribute');

        $type3->add($type3_attribute1);
        $type3->add($type3_attribute2);
        $type3->add($type3_attribute3);


        $type4 = new Type('echoAttributeReference');
        $type4->setAsRoot();
        $type4->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type4->setNamespaces($namespaces);
        $type4_attribute = new TypeAttribute('', 'clientName');

        $type4->add($type4_attribute);

        return array($type1, $type2, $type3, $type4);
    }

    public function getParseTree() {
        $tree = new RootNode();
        $tree->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');

        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );
        /*
        <xs:element xmlns:wsdl11="http://schemas.xmlsoap.org/wsdl/"
                   xmlns:soap11enc="http://schemas.xmlsoap.org/soap/encoding/"
                   name="clientName"
                   type="ex:ClientName"/>
        */
        $node1_element = new ElementNode(array('name' => 'clientName', 'type' => 'ClientName'), 0);
        $node1_element->setNamespaces($namespaces);

        /*
        <xs:attribute xmlns:wsdl11="http://schemas.xmlsoap.org/wsdl/"
                     xmlns:soap11enc="http://schemas.xmlsoap.org/soap/encoding/"
                     name="phoneNumber"
                     type="xs:string"/>
        */
        $node2_attribute = new AttributeNode(array('name' => 'phoneNumber', 'type' => 'string'), 0);
        $node2_attribute->setNamespaces($namespaces);

        /*
        <xs:complexType xmlns:wsdl11="http://schemas.xmlsoap.org/wsdl/"
                       xmlns:soap11enc="http://schemas.xmlsoap.org/soap/encoding/"
                       name="ClientName">
            <xs:sequence>
              <xs:element name="firstName" type="xs:string"/>
              <xs:element name="lastName" type="xs:string"/>
            </xs:sequence>

            <xs:attribute ref="ex:phoneNumber"/>
          </xs:complexType>
        */
        $node3_complexType = new ComplexTypeNode(array('name' => 'ClientName'),0);
        $node3_complexType->setNamespaces($namespaces);
        $node3_sequence = new SequenceNode(array(), 1);
        $node3_sequence->setNamespaces($namespaces);
        $node3_element1 = new ElementNode(array('name' => 'firstName', 'type' => 'string'), 2);
        $node3_element1->setNamespaces($namespaces);
        $node3_element2 = new ElementNode(array('name' => 'lastName', 'type' => 'string'), 2);
        $node3_element2->setNamespaces($namespaces);
        $node3_attribute = new AttributeNode(array('type' => 'phoneNumber'), 1);
        $node3_attribute->setNamespaces($namespaces);

        $node3_sequence->add($node3_element1);
        $node3_sequence->add($node3_element2);
        $node3_complexType->add($node3_sequence);
        $node3_complexType->add($node3_attribute);
        /*
        <xs:element name="echoAttributeReference">
          <xs:complexType>
             <xs:sequence>
                <xs:element ref="ex:clientName"/>
             </xs:sequence>
          </xs:complexType>
       </xs:element>
        */
        $node4_element = new ElementNode(array('name' => 'echoAttributeReference'), 0);
        $node4_element->setNamespaces($namespaces);
        $node4_complexType = new ComplexTypeNode(array(), 1);
        $node4_complexType->setNamespaces($namespaces);
        $node4_sequence = new SequenceNode(array(), 2);
        $node4_sequence->setNamespaces($namespaces);
        $node4_element1 = new ElementNode(array('type' => 'clientName'), 3);
        $node4_element1->setNamespaces($namespaces);

        $node4_sequence->add($node4_element1);
        $node4_complexType->add($node4_sequence);
        $node4_element->add($node4_complexType);


        $tree->add($node1_element);
        $tree->add($node2_attribute);
        $tree->add($node3_complexType);
        $tree->add($node4_element);

        return $tree;
    }
}