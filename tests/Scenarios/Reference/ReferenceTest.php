<?php
namespace PXDB\Tests\Scenarios\Reference;

use PXDB\AST\Structure;
use PXDB\AST\StructureElement;
use PXDB\AST\StructureType;
use PXDB\AST\Type;
use PXDB\AST\TypeAttribute;
use PXDB\Binding\Creator;
use PXDB\CodeGen\ASTCreator;
use PXDB\CodeGen\SchemaParser;
use PXDB\CodeGen\TypeUsage;
use PXDB\ParseTree\ChoiceNode;
use PXDB\ParseTree\ComplexTypeNode;
use PXDB\ParseTree\ElementNode;
use PXDB\ParseTree\RootNode;
use PXDB\ParseTree\SequenceNode;
use PXDB\ParseTree\Tree;
use PXDB\Tests\ExtendedTestCase;


abstract class ReferenceTest
    extends ExtendedTestCase
{
    protected $pathToTestFiles;
    protected $schemaFile;
    protected $classFiles;

    public function setUp() {
        $this->classFiles = array();
    }

    abstract function getASTs();
    abstract function getParseTree();

    public function testAST() {
        $expectedTypes = $this->getASTs();
        $schemaFile = $this->pathToTestFiles . '/' . $this->schemaFile;

        $parser = new SchemaParser($schemaFile);
        $tree = $parser->parse();

        $creator = new ASTCreator();
        $tree->accept($creator);

        $typeList = $creator->getTypeList();

        $this->assertEquals($expectedTypes, $typeList);
    }

    public function testParseTree() {
        $expectedTree = $this->getParseTree();
        $schemaFile = $this->pathToTestFiles . '/' . $this->schemaFile;

        $parser = new SchemaParser($schemaFile);

        $parsedTree = $parser->parse();

        $this->assertTrue($parsedTree instanceof Tree);
        $this->assertTrue($expectedTree instanceof Tree);

        $this->assertEquals($expectedTree, $parsedTree);
    }

    public function testBindingFile() {
        $schemaFile = $this->pathToTestFiles . '/' . $this->schemaFile;
        $bindingFile = file_get_contents($this->pathToTestFiles . '/binding.xml');

        $typeUsage = new TypeUsage();

        $parser = new SchemaParser($schemaFile, $typeUsage);
        $parsedTree = $parser->parse();

        $creator = new ASTCreator();
        $parsedTree->accept($creator);

        $typeList = $creator->getTypeList();

        $usages = $typeUsage->getTypeUsages();

        //$optimizer = new ASTOptimizer($typeList, $typeUsage);
        //$typeList = $optimizer->optimize();

        $b = new Creator($typeList);

        foreach ($typeList as &$type) {
            $type->accept($b);
        }

        $this->assertEqualsWhiteSpaceInsensitive($bindingFile, $b->getXml());
    }
}