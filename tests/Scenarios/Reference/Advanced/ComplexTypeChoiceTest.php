<?php
namespace PXDB\Tests\Scenarios\Reference;

use PXDB\AST\Structure;
use PXDB\AST\StructureElement;
use PXDB\AST\StructureType;
use PXDB\AST\Type;
use PXDB\AST\TypeAttribute;
use PXDB\ParseTree\ChoiceNode;
use PXDB\ParseTree\ComplexTypeNode;
use PXDB\ParseTree\ElementNode;
use PXDB\ParseTree\RootNode;
use PXDB\ParseTree\SequenceNode;


class ComplexTypeChoiceTest
    extends ReferenceTest
{
    public function setUp() {
        $this->pathToTestFiles = dirname(__FILE__) . '/../../../_files/Reference/Advanced/ComplexTypeChoice';
        $this->schemaFile = 'ComplexTypeChoice.xsd';
    }

    public function getASTs() {
        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        $type1 = new Type('fruit', 'Fruit');
        $type1->setAsRoot();
        $type1->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type1->setNamespaces($namespaces);


        $type2 = new Type('Fruit');
        $type2->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type2->setNamespaces($namespaces);
        $type2_structure = new Structure();
        $type2_structure->setStructureType(StructureType::CHOICE());
        $type2_structure->add(new StructureElement('apple', 'int'));
        $type2_structure->add(new StructureElement('orange', 'string'));
        $type2_structure->add(new StructureElement('banana', 'int'));

        $type2->add($type2_structure);


        $type3 = new Type('echoComplexTypeChoice');
        $type3->setAsRoot();
        $type3->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type3->setNamespaces($namespaces);
        $type3_attribute = new TypeAttribute('', 'fruit');

        $type3->add($type3_attribute);

        return array($type1, $type2, $type3);
    }

    public function getParseTree() {
        $tree = new RootNode();
        $tree->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');

        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        $node1_element = new ElementNode(array('name' => 'fruit', 'type' => 'Fruit'), 0);
        $node1_element->setNamespaces($namespaces);


        $node2_complexType = new ComplexTypeNode(array('name' => 'Fruit'), 0);
        $node2_complexType->setNamespaces($namespaces);
        $node2_choice = new ChoiceNode(array(), 1);
        $node2_choice->setNamespaces($namespaces);
        $node2_element_1 = new ElementNode(array('name' => 'apple', 'type' => 'int'), 2);
        $node2_element_1->setNamespaces($namespaces);
        $node2_element_2 = new ElementNode(array('name' => 'orange', 'type' => 'string'), 2);
        $node2_element_2->setNamespaces($namespaces);
        $node2_element_3 = new ElementNode(array('name' => 'banana', 'type' => 'int'), 2);
        $node2_element_3->setNamespaces($namespaces);

        $node2_choice->add($node2_element_1);
        $node2_choice->add($node2_element_2);
        $node2_choice->add($node2_element_3);
        $node2_complexType->add($node2_choice);


        $node3_element = new ElementNode(array('name' => 'echoComplexTypeChoice'), 0);
        $node3_element->setNamespaces($namespaces);
        $node3_complexType = new ComplexTypeNode(array(), 1);
        $node3_complexType->setNamespaces($namespaces);
        $node3_sequence = new SequenceNode(array(), 2);
        $node3_sequence->setNamespaces($namespaces);
        $node3_element1 = new ElementNode(array('type' => 'fruit'), 3);
        $node3_element1->setNamespaces($namespaces);

        $node3_sequence->add($node3_element1);
        $node3_complexType->add($node3_sequence);
        $node3_element->add($node3_complexType);


        $tree->add($node1_element);
        $tree->add($node2_complexType);
        $tree->add($node3_element);

        return $tree;
    }
}