<?php
namespace PXDB\Tests\Scenarios\Reference;

use PXDB\AST\Structure;
use PXDB\AST\Type;
use PXDB\AST\TypeAttribute;
use PXDB\ParseTree\ComplexContentNode;
use PXDB\ParseTree\ComplexTypeNode;
use PXDB\ParseTree\ElementNode;
use PXDB\ParseTree\ExtensionNode;
use PXDB\ParseTree\RootNode;
use PXDB\ParseTree\SequenceNode;


class LocalElementComplexTypeEmptyExtensionTest
    extends ReferenceTest
{
    public function setUp() {
        $this->pathToTestFiles = dirname(__FILE__) . '/../../../_files/Reference/Advanced/LocalElementComplexTypeEmptyExtension';
        $this->schemaFile = 'LocalElementComplexTypeEmptyExtension.xsd';
    }

    public function getASTs() {
        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        $type1 = new Type('localElementComplexTypeEmptyExtension');
        $type1->setAsRoot();
        $type1->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type1->setNamespaces($namespaces);
        $type1_attribute = new Structure('extensionElement', 'ExtensionType');

        $type1->add($type1_attribute);


        $type2 = new Type('ExtensionType');
        $type2->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type2->setNamespaces($namespaces);
        $type2_attribute_1 = new TypeAttribute('element1', 'string');
        $type2_attribute_2 = new TypeAttribute('element2', 'string');

        $type2->add($type2_attribute_1);
        $type2->add($type2_attribute_2);


        $type3 = new Type('echoLocalElementComplexTypeEmptyExtension');
        $type3->setAsRoot();
        $type3->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');
        $type3->setNamespaces($namespaces);
        $type3_attribute = new TypeAttribute('', 'localElementComplexTypeEmptyExtension');

        $type3->add($type3_attribute);

        return array($type1, $type2, $type3);
    }

    public function getParseTree() {
        $tree = new RootNode();
        $tree->setTargetNamespace('http://www.w3.org/2002/ws/databinding/examples/6/09/');

        $namespaces = array(
            '' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'p' => 'http://www.w3.org/2002/ws/databinding/patterns/6/09/',
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        $node1_element = new ElementNode(array('name' => 'localElementComplexTypeEmptyExtension'), 0);
        $node1_element->setNamespaces($namespaces);
        $node1_complexType_1 = new ComplexTypeNode(array(), 1);
        $node1_complexType_1->setNamespaces($namespaces);
        $node1_sequence = new SequenceNode(array(), 2);
        $node1_sequence->setNamespaces($namespaces);
        $node1_element_1 = new ElementNode(array('name' => 'extensionElement'), 3);
        $node1_element_1->setNamespaces($namespaces);
        $node1_complexType_2 = new ComplexTypeNode(array(), 4);
        $node1_complexType_2->setNamespaces($namespaces);
        $node1_complexContent = new ComplexContentNode(array(), 5);
        $node1_complexContent->setNamespaces($namespaces);
        $node1_extension = new ExtensionNode(array('base' => 'ExtensionType'), 6);
        $node1_extension->setNamespaces($namespaces);

        $node1_complexContent->add($node1_extension);
        $node1_complexType_2->add($node1_complexContent);
        $node1_element_1->add($node1_complexType_2);
        $node1_sequence->add($node1_element_1);
        $node1_complexType_1->add($node1_sequence);
        $node1_element->add($node1_complexType_1);


        $node2_complexType = new ComplexTypeNode(array('name' => 'ExtensionType'), 0);
        $node2_complexType->setNamespaces($namespaces);
        $node2_sequence = new SequenceNode(array(), 1);
        $node2_sequence->setNamespaces($namespaces);
        $node2_element_1 = new ElementNode(array('name' => 'element1', 'type' => 'string'), 2);
        $node2_element_1->setNamespaces($namespaces);
        $node2_element_2 = new ElementNode(array('name' => 'element2', 'type' => 'string'), 2);
        $node2_element_2->setNamespaces($namespaces);

        $node2_sequence->add($node2_element_1);
        $node2_sequence->add($node2_element_2);
        $node2_complexType->add($node2_sequence);


        $node3_element = new ElementNode(array('name' => 'echoLocalElementComplexTypeEmptyExtension'), 0);
        $node3_element->setNamespaces($namespaces);
        $node3_complexType = new ComplexTypeNode(array(), 1);
        $node3_complexType->setNamespaces($namespaces);
        $node3_sequence = new SequenceNode(array(), 2);
        $node3_sequence->setNamespaces($namespaces);
        $node3_element1 = new ElementNode(array('type' => 'localElementComplexTypeEmptyExtension'), 3);
        $node3_element1->setNamespaces($namespaces);

        $node3_sequence->add($node3_element1);
        $node3_complexType->add($node3_sequence);
        $node3_element->add($node3_complexType);


        $tree->add($node1_element);
        $tree->add($node2_complexType);
        $tree->add($node3_element);

        return $tree;
    }
}