<?php
namespace PXDB\Tests\Scenarios\Reference;

use PXDB\AST\Type;
use PXDB\ParseTree\ElementNode;
use PXDB\ParseTree\RootNode;


class NoTargetNamespaceTest
    extends ReferenceTest
{
    public function setUp() {
        $this->pathToTestFiles = dirname(__FILE__) . '/../../../_files/Reference/Advanced/NoTargetNamespace';
        $this->schemaFile = 'NoTargetNamespace.xsd';
    }

    public function getASTs() {
        $namespaces = array(
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        $type1 = new Type('noTargetNamespace', 'string');
        $type1->setAsRoot();
        $type1->setNamespaces($namespaces);

        return array($type1);
    }

    public function getParseTree() {
        $tree = new RootNode();

        $namespaces = array(
            'ex' => 'http://www.w3.org/2002/ws/databinding/examples/6/09/',
            'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'xs' => 'http://www.w3.org/2001/XMLSchema',
            'wsdl11' => 'http://schemas.xmlsoap.org/wsdl/',
            'soap11enc' => 'http://schemas.xmlsoap.org/soap/encoding/'
        );

        $node1_element = new ElementNode(array('name' => 'noTargetNamespace', 'type' => 'string'), 0);
        $node1_element->setNamespaces($namespaces);

        $tree->add($node1_element);

        return $tree;
    }
}