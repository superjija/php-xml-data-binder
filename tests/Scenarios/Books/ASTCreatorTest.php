<?php
namespace PXDB\Tests\Scenarios\Books;

use PHPUnit\Framework\TestCase;
use PXDB\AST\Collection;
use PXDB\AST\CollectionItem;
use PXDB\AST\Enumeration;
use PXDB\AST\EnumerationValue;
use PXDB\AST\Structure;
use PXDB\AST\StructureElement;
use PXDB\AST\StructureType;
use PXDB\AST\Type;
use PXDB\AST\TypeAttribute;
use PXDB\CodeGen\ASTCreator;
use PXDB\CodeGen\SchemaParser;
use PXDB\Tests\ExtendedTestCase;


class ASTCreatorTest
    extends ExtendedTestCase
{
    public function testBooksXSD() {
        $filepath = dirname(__FILE__) . '/../../_files/Books';
        $schemaFile = 'books.xsd';

        $parser = new SchemaParser();
        $parser->setSchemaFile($filepath . DIRECTORY_SEPARATOR . $schemaFile);

        $parsedTree = $parser->parse();

        $namespaces = array(
            'xs' => 'http://www.w3.org/2001/XMLSchema',
        );

        $expectedTypes = array();

        $type1 = new Type('Collection');
        $type1->setAsRoot();
        $type1->setNamespaces($namespaces);
        $type1_collection = new Collection('books');
        $type1_collectionItem = new CollectionItem('book', 'bookType');
        $type1_collection->add($type1_collectionItem);
        $type1->add($type1_collection);

        $expectedTypes[] = $type1;


        $type2 = new Type('bookType');
        $type2->setNamespaces($namespaces);
        $type2->add(new TypeAttribute('name', 'string'));
        $type2->add(new TypeAttribute('ISBN', 'long'));
        $type2->add(new TypeAttribute('price', 'string'));

        $type2_collection = new Collection('authors');
        $type2_collectionItem = new CollectionItem('authorName', 'string');
        $type2_collection->add($type2_collectionItem);
        $type2->add($type2_collection);

        $type2->add(new TypeAttribute('description', 'string', true));

        $type2_structure = new Structure('promotion');
        $type2_structure->setStructureType(StructureType::CHOICE());
        $type2_structure->add(new StructureElement('Discount', 'string'));
        $type2_structure->add(new StructureElement('None', 'string'));
        $type2->add($type2_structure);

        $type2->add(new TypeAttribute('publicationDate', 'date'));

        $type2_enumeration = new Enumeration('bookCategory');
        $type2_enumeration->add(new EnumerationValue('magazine', 'NCName'));
        $type2_enumeration->add(new EnumerationValue('novel', 'NCName'));
        $type2_enumeration->add(new EnumerationValue('fiction', 'NCName'));
        $type2_enumeration->add(new EnumerationValue('other', 'NCName'));
        $type2->add($type2_enumeration);

        $type2_attribute = new TypeAttribute('itemId', 'string',FALSE);
        $type2_attribute->setStyle('attribute');
        $type2->add($type2_attribute);

        $type2_attribute2 = new TypeAttribute('itemCode', 'string', TRUE);
        $type2_attribute2->setStyle('attribute');
        $type2->add($type2_attribute2);

        $expectedTypes[] = $type2;


        $type3 = new Type('bookCategoryType');
        $type3->setAsRoot();
        $type3->setNamespaces($namespaces);
        $type3->setBaseType('string');
        $type3_enumeration = new Enumeration();
        $type3_enumeration->add(new EnumerationValue('magazine', 'string'));
        $type3_enumeration->add(new EnumerationValue('novel', 'string'));
        $type3_enumeration->add(new EnumerationValue('fiction', 'string'));
        $type3_enumeration->add(new EnumerationValue('other', 'string'));
        $type3->add($type3_enumeration);

        $expectedTypes[] = $type3;


        $parser = new SchemaParser($filepath . DIRECTORY_SEPARATOR . $schemaFile);
        $tree = $parser->parse();

        $creator = new ASTCreator();
        $tree->accept($creator);

        $typeList = $creator->getTypeList();

        $this->assertEquals($expectedTypes, $typeList);
    }
}