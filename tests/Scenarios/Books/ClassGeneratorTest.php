<?php
namespace PXDB\Tests\Scenarios\Books;

use PHPUnit\Framework\TestCase;
use PXDB\Binding\Creator;
use PXDB\CodeGen\ASTCreator;
use PXDB\CodeGen\ASTOptimizer;
use PXDB\CodeGen\ClassGenerator;
use PXDB\CodeGen\SchemaParser;
use PXDB\CodeGen\TypeUsage;
use PXDB\Tests\ExtendedTestCase;


class ClassGeneratorTest
    extends ExtendedTestCase
{
    public function testScenarioBooksClasses() {
        $filepath = dirname(__FILE__) . '/../../_files/Books';
        $schemaFile = $filepath . '/books.xsd';
        $bindingFile = $filepath . '/binding.xml';
        $collectionFile = $filepath . '/Collection.php';
        $bookTypeFile = $filepath . '/BookType.php';

        $typeUsage = new TypeUsage();

        // most of this test-case follows the flow of PiBX_CodeGen
        // phase 1
        $parser = new SchemaParser($schemaFile, $typeUsage);
        $parsedTree = $parser->parse();

        // phase 2
        $creator = new ASTCreator();
        $parsedTree->accept($creator);

        $typeList = $creator->getTypeList();

        // phase 3
        $usages = $typeUsage->getTypeUsages();

        //        $optimizer = new ASTOptimizer($typeList, $typeUsage);
        //        $typeList = $optimizer->optimize();

        // phase 4
        $b = new Creator($typeList);

        foreach ($typeList as &$type) {
            $type->accept($b);
        }

        //        print_r($typeList);

        //        $this->assertEquals(file_get_contents($bindingFile), $b->getXml());

        // phase 5
        $generator = new ClassGenerator();
        foreach ($typeList as &$type) {
            $type->accept($generator);
        }

        $classes = $generator->getClasses();

        //        $this->assertEquals(2, count($classes));
        $this->assertEqualsWhiteSpaceInsensitive(file_get_contents($collectionFile), "<?php\n" . $classes['Collection']);
        $this->assertEqualsWhiteSpaceInsensitive(file_get_contents($bookTypeFile), "<?php\n" . $classes['BookType']);
    }

    public function testScenarioBooksClassesWithTypeChecks() {
        $filepath = dirname(__FILE__) . '/../../_files/Books';
        $schemaFile = $filepath . '/books.xsd';
        $bindingFile = $filepath . '/binding.xml';
        $collectionFile = $filepath . '/Collection_TypeChecked.php';
        $bookTypeFile = $filepath . '/BookType_TypeChecked.php';

        $typeUsage = new TypeUsage();

        // most of this test-case follows the flow of PiBX_CodeGen
        // phase 1
        $parser = new SchemaParser($schemaFile, $typeUsage);
        $parsedTree = $parser->parse();

        // phase 2
        $creator = new ASTCreator();
        $parsedTree->accept($creator);

        $typeList = $creator->getTypeList();

        // phase 3
        $usages = $typeUsage->getTypeUsages();

        //        $optimizer = new ASTOptimizer($typeList, $typeUsage);
        //        $typeList = $optimizer->optimize();

        // phase 4
        $b = new Creator($typeList);

        foreach ($typeList as &$type) {
            $type->accept($b);
        }

        //        $this->assertEquals(file_get_contents($bindingFile), $b->getXml());

        // phase 5
        $generator = new ClassGenerator();
        $generator->enableTypeChecks();

        foreach ($typeList as &$type) {
            $type->accept($generator);
        }

        $classes = $generator->getClasses();

        //        $this->assertEquals(2, count($classes));
        $this->assertEqualsWhiteSpaceInsensitive(file_get_contents($collectionFile), "<?php\n" . $classes['Collection']);
        $this->assertEqualsWhiteSpaceInsensitive(file_get_contents($bookTypeFile), "<?php\n" . $classes['BookType']);
    }
}