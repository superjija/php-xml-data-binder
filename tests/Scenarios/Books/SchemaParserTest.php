<?php
namespace PXDB\Tests\Scenarios\Books;


use PXDB\CodeGen\SchemaParser;
use PXDB\ParseTree\AttributeNode;
use PXDB\ParseTree\ChoiceNode;
use PXDB\ParseTree\ComplexTypeNode;
use PXDB\ParseTree\ElementNode;
use PXDB\ParseTree\EnumerationNode;
use PXDB\ParseTree\RestrictionNode;
use PXDB\ParseTree\RootNode;
use PXDB\ParseTree\SequenceNode;
use PXDB\ParseTree\SimpleTypeNode;
use PXDB\Tests\ExtendedTestCase;


class SchemaParserTest
    extends ExtendedTestCase
{
    public function testScenarioBooksSchema() {
        $filepath = dirname(__FILE__) . '/../../_files/Books/';
        $xml = simplexml_load_file($filepath . '/books.xsd');

        $namespaces = array('xs' => 'http://www.w3.org/2001/XMLSchema');

        $parser = new SchemaParser();
        $parser->setSchema($xml);

        $parsedTree = $parser->parse();

        $expectedTree = new RootNode();

        $node1_element = new ElementNode(array('name' => 'Collection'), 0);
        $node1_element->setNamespaces($namespaces);
        $node1_complexType_1 = new ComplexTypeNode(array(), 1);
        $node1_complexType_1->setNamespaces($namespaces);
        $node1_sequence_1 = new SequenceNode(array(), 2);
        $node1_sequence_1->setNamespaces($namespaces);
        $node1_element_1 = new ElementNode(array('name' => 'books'), 3);
        $node1_element_1->setNamespaces($namespaces);
        $node1_complexType_2 = new ComplexTypeNode(array(), 4);
        $node1_complexType_2->setNamespaces($namespaces);
        $node1_sequence_2 = new SequenceNode(array(), 5);
        $node1_sequence_2->setNamespaces($namespaces);
        $node1_element_2 = new ElementNode(array('name' => 'book', 'type' => 'bookType', 'maxOccurs' => 'unbounded'), 6);
        $node1_element_2->setNamespaces($namespaces);

        $node1_sequence_2->add($node1_element_2);
        $node1_complexType_2->add($node1_sequence_2);
        $node1_element_1->add($node1_complexType_2);
        $node1_sequence_1->add($node1_element_1);
        $node1_complexType_1->add($node1_sequence_1);
        $node1_element->add($node1_complexType_1);


        $node2_complexType = new ComplexTypeNode(array('name' => 'bookType'), 0);
        $node2_complexType->setNamespaces($namespaces);
        $node2_sequence = new SequenceNode(array(), 1);
        $node2_sequence->setNamespaces($namespaces);
        $node2_element_1 = new ElementNode(array('name' => 'name', 'type' => 'string'), 2);
        $node2_element_1->setNamespaces($namespaces);
        $node2_element_2 = new ElementNode(array('name' => 'ISBN', 'type' => 'long'), 2);
        $node2_element_2->setNamespaces($namespaces);
        $node2_element_3 = new ElementNode(array('name' => 'price', 'type' => 'string'), 2);
        $node2_element_3->setNamespaces($namespaces);
        $node2_element_4 = new ElementNode(array('name' => 'authors'), 2);
        $node2_element_4->setNamespaces($namespaces);
        $node2_complexType_1 = new ComplexTypeNode(array(), 3);
        $node2_complexType_1->setNamespaces($namespaces);
        $node2_sequence_1 = new SequenceNode(array(), 4);
        $node2_sequence_1->setNamespaces($namespaces);
        $node2_element_5 = new ElementNode(array('name' => 'authorName', 'type' => 'string', 'maxOccurs' => 'unbounded'), 5);
        $node2_element_5->setNamespaces($namespaces);
        $node2_element_6 = new ElementNode(array('name' => 'description', 'type' => 'string', 'minOccurs' => '0'), 2);
        $node2_element_6->setNamespaces($namespaces);
        $node2_element_7 = new ElementNode(array('name' => 'promotion'), 2);
        $node2_element_7->setNamespaces($namespaces);
        $node2_complexType_2 = new ComplexTypeNode(array(), 3);
        $node2_complexType_2->setNamespaces($namespaces);
        $node2_choice = new ChoiceNode(array(), 4);
        $node2_choice->setNamespaces($namespaces);
        $node2_element_8 = new ElementNode(array('name' => 'Discount', 'type' => 'string'), 5);
        $node2_element_8->setNamespaces($namespaces);
        $node2_element_9 = new ElementNode(array('name' => 'None', 'type' => 'string'), 5);
        $node2_element_9->setNamespaces($namespaces);
        $node2_element_10 = new ElementNode(array('name' => 'publicationDate', 'type' => 'date'), 2);
        $node2_element_10->setNamespaces($namespaces);
        $node2_element_11 = new ElementNode(array('name' => 'bookCategory'), 2);
        $node2_element_11->setNamespaces($namespaces);
        $node2_simpleType = new SimpleTypeNode(array(), 3);
        $node2_simpleType->setNamespaces($namespaces);
        $node2_restriction = new RestrictionNode(array('base' => 'NCName'), 4);
        $node2_restriction->setNamespaces($namespaces);
        $node2_enumeration_1 = new EnumerationNode(array('value' => 'magazine'), 5);
        $node2_enumeration_1->setNamespaces($namespaces);
        $node2_enumeration_2 = new EnumerationNode(array('value' => 'novel'), 5);
        $node2_enumeration_2->setNamespaces($namespaces);
        $node2_enumeration_3 = new EnumerationNode(array('value' => 'fiction'), 5);
        $node2_enumeration_3->setNamespaces($namespaces);
        $node2_enumeration_4 = new EnumerationNode(array('value' => 'other'), 5);
        $node2_enumeration_4->setNamespaces($namespaces);
        $node2_attribute = new AttributeNode(array('name' => 'itemId', 'type' => 'string', 'use' => 'required'), 1);
        $node2_attribute->setNamespaces($namespaces);
        $node2_attribute2 = new AttributeNode(array('name' => 'itemCode', 'type' => 'string'), 1);
        $node2_attribute2->setNamespaces($namespaces);

        $node2_complexType->add($node2_sequence);
        $node2_sequence->add($node2_element_1);
        $node2_sequence->add($node2_element_2);
        $node2_sequence->add($node2_element_3);
        $node2_sequence->add($node2_element_4);
        $node2_element_4->add($node2_complexType_1);
        $node2_complexType_1->add($node2_sequence_1);
        $node2_sequence_1->add($node2_element_5);
        $node2_sequence->add($node2_element_6);
        $node2_sequence->add($node2_element_7);
        $node2_element_7->add($node2_complexType_2);
        $node2_complexType_2->add($node2_choice);
        $node2_choice->add($node2_element_8);
        $node2_choice->add($node2_element_9);
        $node2_sequence->add($node2_element_10);
        $node2_sequence->add($node2_element_11);
        $node2_element_11->add($node2_simpleType);
        $node2_simpleType->add($node2_restriction);
        $node2_restriction->add($node2_enumeration_1);
        $node2_restriction->add($node2_enumeration_2);
        $node2_restriction->add($node2_enumeration_3);
        $node2_restriction->add($node2_enumeration_4);
        $node2_complexType->add($node2_attribute);
        $node2_complexType->add($node2_attribute2);


        $node3_simpleType = new SimpleTypeNode(array('name' => 'bookCategoryType'), 0);
        $node3_simpleType->setNamespaces($namespaces);
        $node3_restriction = new RestrictionNode(array('base' => 'string'), 1);
        $node3_restriction->setNamespaces($namespaces);
        $node3_enumeration_1 = new EnumerationNode(array('value' => 'magazine'), 2);
        $node3_enumeration_1->setNamespaces($namespaces);
        $node3_enumeration_2 = new EnumerationNode(array('value' => 'novel'), 2);
        $node3_enumeration_2->setNamespaces($namespaces);
        $node3_enumeration_3 = new EnumerationNode(array('value' => 'fiction'), 2);
        $node3_enumeration_3->setNamespaces($namespaces);
        $node3_enumeration_4 = new EnumerationNode(array('value' => 'other'), 2);
        $node3_enumeration_4->setNamespaces($namespaces);

        $node3_restriction->add($node3_enumeration_1);
        $node3_restriction->add($node3_enumeration_2);
        $node3_restriction->add($node3_enumeration_3);
        $node3_restriction->add($node3_enumeration_4);
        $node3_simpleType->add($node3_restriction);


        $expectedTree->add($node1_element);
        $expectedTree->add($node2_complexType);
        $expectedTree->add($node3_simpleType);


        $this->assertEquals($expectedTree, $parsedTree);
    }
}