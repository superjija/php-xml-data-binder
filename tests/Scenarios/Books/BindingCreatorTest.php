<?php
namespace PXDB\Tests\Scenarios\Books;

use PHPUnit\Framework\TestCase;
use PXDB\Binding\Creator;
use PXDB\CodeGen\ASTCreator;
use PXDB\CodeGen\ASTOptimizer;
use PXDB\CodeGen\SchemaParser;
use PXDB\CodeGen\TypeUsage;
use PXDB\Tests\ExtendedTestCase;


class BindingCreatorTest
    extends ExtendedTestCase
{
    public function testBindingCreator() {
        $filepath = dirname(__FILE__) . '/../../_files/Books';
        $schemaFile = $filepath . '/books.xsd';
        $bindingFile = $filepath . '/binding.xml';
        $collectionFile = $filepath . '/Collection.php';
        $bookTypeFile = $filepath . '/BookType.php';

        $typeUsage = new TypeUsage();

        // most of this test-case follows the flow of PiBX_CodeGen
        // phase 1
        $parser = new SchemaParser($schemaFile, $typeUsage);
        $parsedTree = $parser->parse();

        // phase 2
        $creator = new ASTCreator($typeUsage);
        $parsedTree->accept($creator);

        $typeList = $creator->getTypeList();

        // phase 3
        $usages = $typeUsage->getTypeUsages();

        $optimizer = new ASTOptimizer($typeList, $typeUsage);
        //        $typeList = $optimizer->optimize();
        //        print_r($typeList);
        array_pop($typeList);// TODO!

        // phase 4
        $b = new Creator($typeList);

        foreach ($typeList as &$type) {
            $type->accept($b);
        }
        $expectedXml = str_replace("\r\n","\n", file_get_contents($bindingFile));
        $xml = str_replace("\r\n","\n", $b->getXml());
        $this->assertEqualsWhiteSpaceInsensitive($expectedXml, $xml);
    }
}