<?php
namespace PXDB\Tests\ParseTree;

use PHPUnit\Framework\TestCase;
use PXDB\ParseTree\AttributeHelper;


class AttributeHelperTest
    extends TestCase
{
    public function testTypeAttributeWithNamespace() {
        $simpleXML = simplexml_load_string('<element name="elementName" abstract="false" minOccurs="1" type="ns1:anotherType"/>');

        $options = AttributeHelper::getElementOptions($simpleXML);

        $this->assertEquals('anotherType', $options['type']);
    }

    public function testBaseAttributeWithNamespace() {
        $simpleXML = simplexml_load_string('<restriction id="res1" base="ns1:otherType"/>');

        $options = AttributeHelper::getRestrictionOptions($simpleXML);

        $this->assertEquals('otherType', $options['base']);
    }

    public function testRefAttributeShouldBeTypeOption() {
        $simpleXML = simplexml_load_string('<element name="elementName" abstract="false" minOccurs="1" ref="ns1:anotherType"/>');

        $options = AttributeHelper::getElementOptions($simpleXML);

        $this->assertEquals('anotherType', $options['type']);
    }

    public function testTypeConversions() {
        $simpleXML = simplexml_load_string('<element name="elementName" abstract="false" nillable="true" minOccurs="1" maxOccurs="unbounded"/>');

        $options = AttributeHelper::getElementOptions($simpleXML);

        $this->assertEquals('elementName', $options['name']);
        $this->assertFalse($options['abstract']);
        $this->assertTrue($options['nillable']);
        $this->assertEquals(1, $options['minOccurs']);
        $this->assertEquals("unbounded", $options['maxOccurs']);
    }

    public function testElementOptionsWithSimpleXML() {
        $simpleXML = simplexml_load_string('<element name="elementName" abstract="false" minOccurs="1"/>');

        $options = AttributeHelper::getElementOptions($simpleXML);

        $this->assertEquals('elementName', $options['name']);
        $this->assertFalse($options['abstract']);
        $this->assertFalse($options['nillable']);
        $this->assertEquals(1, $options['minOccurs']);
        $this->assertTrue(is_int($options['minOccurs']));
    }

    public function testElementOptionsWithArray() {
        $elementOptions = array('name' => 'elementName', 'abstract' => false, 'minOccurs' => 1);
        $options = AttributeHelper::getElementOptions($elementOptions);

        $this->assertEquals('elementName', $options['name']);
        $this->assertFalse($options['abstract']);
        $this->assertFalse($options['nillable']);
        $this->assertEquals(1, $options['minOccurs']);
        $this->assertTrue(is_int($options['minOccurs']));
    }

    public function testSimpleTypeWithSimpleXML() {
        $simpleXML = simplexml_load_string('<simpleType name="simpleType" id="ST0001"/>');

        $options = AttributeHelper::getSimpleTypeOptions($simpleXML);

        $this->assertEquals('simpleType', $options['name']);
        $this->assertEquals('ST0001', $options['id']);
        $this->assertTrue(!array_key_exists('invalidAttribute', $options));
    }

    public function testSimpleTypeWithArray() {
        $simpleTypeOptions = array('name' => 'simpleType');
        $options = AttributeHelper::getSimpleTypeOptions($simpleTypeOptions);

        $this->assertEquals('simpleType', $options['name']);
        $this->assertEmpty($options['id']);
    }

    public function testComplexTypeWithSimpleXML() {
        $simpleXML = simplexml_load_string('<complexType name="complexType" mixed="false"/>');

        $options = AttributeHelper::getComplexTypeOptions($simpleXML);

        $this->assertEquals('complexType', $options['name']);
        $this->assertFalse($options['mixed']);
        $this->assertFalse($options['abstract']);
    }

    public function testComplexTypeWithArray() {
        $complexTypeOptions = array('name' => 'complexType', 'mixed' => false);

        $options = AttributeHelper::getComplexTypeOptions($complexTypeOptions);

        $this->assertEquals('complexType', $options['name']);
        $this->assertFalse($options['mixed']);
        $this->assertFalse($options['abstract']);
    }

    public function testSequenceWithSimpleXML() {
        $simpleXML = simplexml_load_string('<sequence id="SEQ001"/>');

        $options = AttributeHelper::getSequenceOptions($simpleXML);

        $this->assertEquals('SEQ001', $options['id']);
        $this->assertEquals(1, $options['maxOccurs']);
        $this->assertEquals(1, $options['minOccurs']);
    }

    public function testSequenceWithArray() {
        $complexTypeOptions = array('id' => 'SEQ001', 'minOccurs' => '1');

        $options = AttributeHelper::getSequenceOptions($complexTypeOptions);

        $this->assertEquals('SEQ001', $options['id']);
        $this->assertEquals(1, $options['maxOccurs']);
        $this->assertEquals(1, $options['minOccurs']);
    }

    public function testAttributeWithSimpleXML() {
        $simpleXML = simplexml_load_string('<attribute name="attribute"/>');

        $options = AttributeHelper::getAttributeOptions($simpleXML);

        $this->assertEquals('attribute', $options['name']);
    }

    public function testAttributeWithArray() {
        $attributeOptions = array('name' => 'attribute');

        $options = AttributeHelper::getAttributeOptions($attributeOptions);

        $this->assertEquals('attribute', $options['name']);
    }

    public function testRestrictionWithSimpleXML() {
        $simpleXML = simplexml_load_string('<restriction id="res1" base="otherType"/>');

        $options = AttributeHelper::getRestrictionOptions($simpleXML);

        $this->assertEquals('res1', $options['id']);
        $this->assertEquals('otherType', $options['base']);
    }

    public function testRestrictionWithArray() {
        $restrictionOptions = array('base' => 'otherType');

        $options = AttributeHelper::getRestrictionOptions($restrictionOptions);

        $this->assertEquals('otherType', $options['base']);
    }

    public function testEnumerationWithSimpleXML() {
        $simpleXML = simplexml_load_string('<enumeration value="value"/>');

        $options = AttributeHelper::getEnumerationOptions($simpleXML);

        $this->assertEquals('value', $options['value']);
    }

    public function testEnumerationWithArray() {
        $enumerationOptions = array('value' => 'value');

        $options = AttributeHelper::getEnumerationOptions($enumerationOptions);

        $this->assertEquals('value', $options['value']);
    }

    public function testComplexContentWithSimpleXML() {
        $simpleXML = simplexml_load_string('<complexContent id="cc1" mixed="true"/>');

        $options = AttributeHelper::getComplexContentOptions($simpleXML);

        $this->assertEquals('cc1', $options['id']);
        $this->assertTrue($options['mixed']);
    }

    public function testComplexContentWithArray() {
        $complexContentOptions = array('id' => 'cc1');

        $options = AttributeHelper::getComplexContentOptions($complexContentOptions);

        $this->assertEquals('cc1', $options['id']);
        $this->assertFalse($options['mixed']);
    }

    public function testExtensionWithSimpleXML() {
        $simpleXML = simplexml_load_string('<extension id="cc1" base="baseType"/>');

        $options = AttributeHelper::getExtensionOptions($simpleXML);

        $this->assertEquals('cc1', $options['id']);
        $this->assertEquals('baseType', $options['base']);
    }

    public function testExtensionWithArray() {
        $extensionOptions = array('base' => 'baseType');

        $options = AttributeHelper::getExtensionOptions($extensionOptions);

        $this->assertEquals('baseType', $options['base']);
    }
}