<?php
namespace PXDB\Tests\Util;
use PHPUnit\Framework\TestCase;
use PXDB\Util\XsdType;


class XsdTypeTest
    extends TestCase
{
    public function testAllEnumsShouldHaveTheirProperValues() {
        $this->assertEquals(XsdType::STRING(), 'string');
        $this->assertEquals(XsdType::BOOLEAN(), 'boolean');
        $this->assertEquals(XsdType::DECIMAL(), 'decimal');
        $this->assertEquals(XsdType::FLOAT(), 'float');
        $this->assertEquals(XsdType::DOUBLE(), 'double');
        $this->assertEquals(XsdType::DURATION(), 'duration');
        $this->assertEquals(XsdType::DATETIME(), 'dateTime');
        $this->assertEquals(XsdType::TIME(), 'time');
        $this->assertEquals(XsdType::DATE(), 'date');
        $this->assertEquals(XsdType::GYEARMONTH(), 'gYearMonth');
        $this->assertEquals(XsdType::GYEAR(), 'gYear');
        $this->assertEquals(XsdType::GMONTHDAY(), 'gMonthDay');
        $this->assertEquals(XsdType::GDAY(), 'gDay');
        $this->assertEquals(XsdType::GMONTH(), 'gMonth');
        $this->assertEquals(XsdType::HEXBINARY(), 'hexBinary');
        $this->assertEquals(XsdType::BASE64BINARY(), 'base64Binary');
        $this->assertEquals(XsdType::ANYURI(), 'anyURI');
        $this->assertEquals(XsdType::QNAME(), 'QName');
        $this->assertEquals(XsdType::NOTATION(), 'NOTATION');
        $this->assertEquals(XsdType::NORMALIZEDSTRING(), 'normalizedString');
        $this->assertEquals(XsdType::TOKEN(), 'token');
        $this->assertEquals(XsdType::LANGUAGE(), 'language');
        $this->assertEquals(XsdType::NMTOKEN(), 'NMTOKEN');
        $this->assertEquals(XsdType::NMTOKENS(), 'NMTOKENS');
        $this->assertEquals(XsdType::NAME(), 'Name');
        $this->assertEquals(XsdType::NCNAME(), 'NCName');
        $this->assertEquals(XsdType::ID(), 'ID');
        $this->assertEquals(XsdType::IDREF(), 'IDREF');
        $this->assertEquals(XsdType::IDREFS(), 'IDREFS');
        $this->assertEquals(XsdType::ENTITY(), 'ENTITY');
        $this->assertEquals(XsdType::ENTITIES(), 'ENTITIES');
        $this->assertEquals(XsdType::INTEGER(), 'integer');
        $this->assertEquals(XsdType::NONPOSITIVEINTEGER(), 'nonPositiveInteger');
        $this->assertEquals(XsdType::NEGATIVEINTEGER(), 'negativeInteger');
        $this->assertEquals(XsdType::LONG(), 'long');
        $this->assertEquals(XsdType::INT(), 'int');
        $this->assertEquals(XsdType::SHORT(), 'short');
        $this->assertEquals(XsdType::BYTE(), 'byte');
        $this->assertEquals(XsdType::NONNEGATIVEINTEGER(), 'nonNegativeInteger');
        $this->assertEquals(XsdType::UNSIGNEDLONG(), 'unsignedLong');
        $this->assertEquals(XsdType::UNSIGNEDINT(), 'unsignedInt');
        $this->assertEquals(XsdType::UNSIGNEDSHORT(), 'unsignedShort');
        $this->assertEquals(XsdType::UNSIGNEDBYTE(), 'unsignedByte');
        $this->assertEquals(XsdType::POSITIVEINTEGER(), 'positiveInteger');
    }
}