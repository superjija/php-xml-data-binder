<?php

require_once __DIR__ . '/../autoload.php';
require_once __DIR__ . '/ExtendedTestCase.php';
require_once __DIR__ . '/Scenarios/Reference/ReferenceTest.php';

function addXMLElement(\SimpleXMLElement $dest, \SimpleXMLElement $source) {
    $newDest = $dest->addChild($source->getName(), $source[0]);
    foreach ($source->children() as $child) {
        addXMLElement($newDest, $child);
    }
}