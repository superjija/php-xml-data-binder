<?php
namespace PXDB\Tests\CodeGen;

use PHPUnit\Framework\TestCase;
use PXDB\AST\Collection;
use PXDB\AST\CollectionItem;
use PXDB\AST\Enumeration;
use PXDB\AST\EnumerationValue;
use PXDB\AST\Type;
use PXDB\AST\TypeAttribute;
use PXDB\CodeGen\ASTCreator;
use PXDB\CodeGen\SchemaParser;


class ASTCreatorTest
    extends TestCase
{
    public function testSimpleTypeWithEnumeration() {
        $data = <<<XML
<?xml version="1.0"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:simpleType name="bookCategoryType">
        <xs:restriction base="string">
            <xs:enumeration value="magazine"/>
            <xs:enumeration value="novel"/>
            <xs:enumeration value="fiction"/>
            <xs:enumeration value="other"/>
        </xs:restriction>
    </xs:simpleType>
</xs:schema>
XML;

        $expectedType = new Type('bookCategoryType');
        $expectedType->setAsRoot();
        $expectedType->setBaseType('string');
        $expectedType->setNamespaces(array('xs' => 'http://www.w3.org/2001/XMLSchema'));
        $enumeration = new Enumeration();
        $enum = new EnumerationValue('magazine', 'string');
        $enumeration->add($enum);
        $enum = new EnumerationValue('novel', 'string');
        $enumeration->add($enum);
        $enum = new EnumerationValue('fiction', 'string');
        $enumeration->add($enum);
        $enum = new EnumerationValue('other', 'string');
        $enumeration->add($enum);
        $expectedType->add($enumeration);

        $schema = simplexml_load_string($data);

        $parser = new SchemaParser();
        $parser->setSchema($schema);
        $tree = $parser->parse();

        $creator = new ASTCreator();
        $tree->accept($creator);

        $typeList = $creator->getTypeList();
        list($type) = $typeList;

        $this->assertEquals($expectedType, $type);
    }

    public function testComplexTypeWithUnboundedSequence() {
        $data = <<<XML
<?xml version="1.0"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:element name="Collection">
       <xs:complexType>
          <xs:sequence>
            <xs:element name ="books">
               <xs:complexType>
                  <xs:sequence>
                    <xs:element name="book" type="bookType" minOccurs="1" maxOccurs="unbounded"/>
                  </xs:sequence>
               </xs:complexType>
            </xs:element>
          </xs:sequence>
       </xs:complexType>
    </xs:element>
</xs:schema>
XML;

        $expectedType = new Type('Collection');
        $expectedType->setAsRoot();
        $expectedType->setNamespaces(array('xs' => 'http://www.w3.org/2001/XMLSchema'));
        $c = new Collection('books');
        $ci = new CollectionItem('book', 'bookType');
        $c->add($ci);
        $expectedType->add($c);

        $schema = simplexml_load_string($data);

        $parser = new SchemaParser();
        $parser->setSchema($schema);
        $tree = $parser->parse();

        $creator = new ASTCreator();
        $tree->accept($creator);

        $typeList = $creator->getTypeList();
        list($type) = $typeList;

        $this->assertEquals($expectedType, $type);
    }

    public function testComplexTypeWithElements() {
        $data = <<<XML
<?xml version="1.0"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:complexType name="complexTypeWithElements">
        <xs:sequence>
            <xs:element name="element1" type="xs:string"/>
            <xs:element name="element2" type="xs:long"/>
            <xs:element name="element3" type="xs:string"/>
            <xs:element name="element4" type="xs:string"  minOccurs="0"/>
            <xs:element name="element5" type="xs:date"/>
        </xs:sequence>
    </xs:complexType>
</xs:schema>
XML;

        $expectedType = new Type('complexTypeWithElements');
        $expectedType->setNamespaces(array('xs' => 'http://www.w3.org/2001/XMLSchema'));
        $attr = new TypeAttribute('element1');
        $attr->setType('string');
        $expectedType->add($attr);
        $attr = new TypeAttribute('element2');
        $attr->setType('long');
        $expectedType->add($attr);
        $attr = new TypeAttribute('element3');
        $attr->setType('string');
        $expectedType->add($attr);
        $attr = new TypeAttribute('element4', '', true);
        $attr->setType('string');
        $expectedType->add($attr);
        $attr = new TypeAttribute('element5');
        $attr->setType('date');
        $expectedType->add($attr);

        $schema = simplexml_load_string($data);

        $parser = new SchemaParser();
        $parser->setSchema($schema);
        $tree = $parser->parse();

        $creator = new ASTCreator();
        $tree->accept($creator);

        $typeList = $creator->getTypeList();
        list($type) = $typeList;

        $this->assertEquals($expectedType, $type);
    }

    public function testComplexTypeWithElementsAndSequence() {
        $data = <<<XML
<?xml version="1.0"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:complexType name="complexTypeWithElements">
        <xs:sequence>
            <xs:element name="element1" type="xs:string"/>
            <xs:element name="element2" type="xs:long"/>
            <xs:element name="element3" type="xs:string"/>
            <xs:element name="element4" type="xs:string"  minOccurs="0"/>
            <xs:element name="elements" >
                <xs:complexType>
                    <xs:sequence>
                        <xs:element name="element" type="xs:string" minOccurs="1" maxOccurs="unbounded"/>
                    </xs:sequence>
                </xs:complexType>
            </xs:element>
            <xs:element name="element6" type="xs:date"/>
        </xs:sequence>
    </xs:complexType>
</xs:schema>
XML;

        $expectedType = new Type('complexTypeWithElements');
        $expectedType->setNamespaces(array('xs' => 'http://www.w3.org/2001/XMLSchema'));
        $attr = new TypeAttribute('element1');
        $attr->setType('string');
        $expectedType->add($attr);
        $attr = new TypeAttribute('element2');
        $attr->setType('long');
        $expectedType->add($attr);
        $attr = new TypeAttribute('element3');
        $attr->setType('string');
        $expectedType->add($attr);
        $attr = new TypeAttribute('element4', '', true);
        $attr->setType('string');
        $expectedType->add($attr);

        $collection = new Collection('elements');
        $collectionItem = new CollectionItem('element');
        $collectionItem->setType('string');
        $collection->add($collectionItem);
        $expectedType->add($collection);

        $attr = new TypeAttribute('element6');
        $attr->setType('date');
        $expectedType->add($attr);

        $schema = simplexml_load_string($data);

        $parser = new SchemaParser();
        $parser->setSchema($schema);
        $tree = $parser->parse();

        $creator = new ASTCreator();
        $tree->accept($creator);

        $typeList = $creator->getTypeList();
        list($type) = $typeList;

        $this->assertEquals($expectedType, $type);
    }
}