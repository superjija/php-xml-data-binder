<?php
namespace PXDB\Tests\CodeGen;

use PHPUnit\Framework\TestCase;
use PXDB\AST\CollectionItem;
use PXDB\AST\Enumeration;
use PXDB\AST\EnumerationValue;
use PXDB\AST\Structure;
use PXDB\AST\StructureElement;
use PXDB\AST\StructureType;
use PXDB\AST\Type;
use PXDB\CodeGen\ClassGenerator;


class ClassGeneratorTest
    extends TestCase
{
    public function testIndentationWithSpaces() {
        $dummyAST = new Type('generatedWithSpaces', 'string');
        $dummyAST->setAsRoot();

        $generator = new ClassGenerator('    ');
        $dummyAST->accept($generator);


        $classes = $generator->getClasses();

        $expectedClassCode = "class GeneratedWithSpaces {\n"
            . "    private \$generatedWithSpaces;\n"
            . "\n"
            . "    public function setGeneratedWithSpaces(\$generatedWithSpaces) {\n"
            . "        \$this->generatedWithSpaces = \$generatedWithSpaces;\n"
            . "        return \$this;\n"
            . "    }\n"
            . "    public function getGeneratedWithSpaces() {\n"
            . "        return \$this->generatedWithSpaces;\n"
            . "    }\n"
            . "}";
        $this->assertEquals($expectedClassCode, $classes['GeneratedWithSpaces']);
    }

    public function testIndentationWithTabs() {
        $dummyAST = new Type('generatedWithTabs', 'string');
        $dummyAST->setAsRoot();

        $generator = new ClassGenerator("\t");
        $dummyAST->accept($generator);

        $classes = $generator->getClasses();

        $expectedClassCode = "class GeneratedWithTabs {\n"
            . "\tprivate \$generatedWithTabs;\n"
            . "\n"
            . "\tpublic function setGeneratedWithTabs(\$generatedWithTabs) {\n"
            . "\t\t\$this->generatedWithTabs = \$generatedWithTabs;\n"
            . "\t\treturn \$this;\n"
            . "\t}\n"
            . "\tpublic function getGeneratedWithTabs() {\n"
            . "\t\treturn \$this->generatedWithTabs;\n"
            . "\t}\n"
            . "}";
        $this->assertEquals($expectedClassCode, $classes['GeneratedWithTabs']);
    }

    public function testNoIndentation() {
        $dummyAST = new Type('generatedWithoutIndentation', 'string');
        $dummyAST->setAsRoot();
        $generator = new ClassGenerator('');
        $generator->enableTypeChecks();
        $dummyAST->accept($generator);
        $classes = $generator->getClasses();
        $expectedClassCode = "class GeneratedWithoutIndentation {\n"
            . "private \$generatedWithoutIndentation;\n"
            . "\n"
            . "public function setGeneratedWithoutIndentation(string \$generatedWithoutIndentation) {\n"
            . "\$this->generatedWithoutIndentation = \$generatedWithoutIndentation;\n"
            . "return \$this;\n"
            . "}\n"
            . "public function getGeneratedWithoutIndentation() {\n"
            . "return \$this->generatedWithoutIndentation;\n"
            . "}\n"
            . "}";
        $this->assertEquals($expectedClassCode, $classes['GeneratedWithoutIndentation']);
    }
    public function testGlobalComplexTypeChoice() {
        $type = new Type('Fruit');
        $type_structure = new Structure();
        $type_structure->setStructureType(StructureType::CHOICE());
        $type_structure->add(new StructureElement('apple', 'string'));
        $type_structure->add(new StructureElement('orange', 'string'));
        $type->add($type_structure);
        $generator = new ClassGenerator('  ');
        $type->accept($generator);
        $classes = $generator->getClasses();
        $expectedClassCode  = "class Fruit {\n"
            . "  private \$choiceSelect = -1;\n"
            . "  private \$APPLE_CHOICE = 0;\n"
            . "  private \$ORANGE_CHOICE = 1;\n"
            . "  private \$apple;\n"
            . "  private \$orange;\n"
            . "\n"
            . "  public function clearChoiceSelect() {\n"
            . "    \$this->choiceSelect = -1;\n"
            . "    return \$this;\n"
            . "  }\n"
            . "  public function isApple() {\n"
            . "    return \$this->choiceSelect == \$this->APPLE_CHOICE;\n"
            . "  }\n"
            . "  public function setApple(\$apple) {\n"
            . "    \$this->setChoiceSelect(\$this->APPLE_CHOICE);\n"
            . "    \$this->apple = \$apple;\n"
            . "    return \$this;\n"
            . "  }\n"
            . "  public function getApple() {\n"
            . "    return \$this->apple;\n"
            . "  }\n"
            . "  public function isOrange() {\n"
            . "    return \$this->choiceSelect == \$this->ORANGE_CHOICE;\n"
            . "  }\n"
            . "  public function setOrange(\$orange) {\n"
            . "    \$this->setChoiceSelect(\$this->ORANGE_CHOICE);\n"
            . "    \$this->orange = \$orange;\n"
            . "    return \$this;\n"
            . "  }\n"
            . "  public function getOrange() {\n"
            . "    return \$this->orange;\n"
            . "  }\n"
            . "  private function setChoiceSelect(\$choice) {\n"
            . "    if (\$this->choiceSelect == -1) {\n"
            . "      \$this->choiceSelect = \$choice;\n"
            . "    } elseif (\$this->choiceSelect != \$choice) {\n"
            . "      throw new RuntimeException('Need to call clearChoiceSelect() before changing existing choice');\n"
            . "    }\n"
            . "    return \$this;\n"
            . "  }\n"
            ."}";
        $this->assertEquals($expectedClassCode, $classes['Fruit']);
    }

    public function testGlobalEnumerationType() {
        $ast = new Type('EnumerationType');
        $ast->setAsRoot();
        $enumeration = new Enumeration();
        $enumeration->add(new EnumerationValue('One', 'string'));
        $enumeration->add(new EnumerationValue('Two', 'string'));
        $enumeration->add(new EnumerationValue('Three', 'string'));
        $ast->add($enumeration);

        $generator = new ClassGenerator('  ');
        $ast->accept($generator);

        $classes = $generator->getClasses();

        $expectedClassCode  = "class EnumerationType {\n"
            . "  private \$enumerationType;\n"
            . "\n"
            . "  public function setEnumerationType(\$enumerationType) {\n"
            . "    \$this->enumerationType = \$enumerationType;\n"
            . "    return \$this;\n"
            . "  }\n"
            . "  public function getEnumerationType() {\n"
            . "    return \$this->enumerationType;\n"
            . "  }\n"
            . "}";

        $this->assertEquals($expectedClassCode, $classes['EnumerationType']);
    }

    public function testIndirectCollection() {
        $ast = new Type('Type');
        $ast->setAsRoot();
        $ast->add(new CollectionItem('memberList', 'string'));

        $generator = new ClassGenerator('  ');
        $generator->enableTypeChecks();
        $ast->accept($generator);

        $classes = $generator->getClasses();

        $expectedClassCode  = "class Type {\n"
            . "  private \$memberList;\n"
            . "\n"
            . "  public function setMemberLists(array \$memberList) {\n"
            . "    foreach (\$memberList as &\$m) {\n"
            . "      if (!is_string(\$m)) {\n"
            . "        throw new InvalidArgumentException('Invalid list. All containing elements have to be of type \"string\".');\n"
            . "      }\n"
            . "    }\n"
            . "    \$this->memberList = \$memberList;\n"
            . "    return \$this;\n"
            . "  }\n"
            . "  public function getMemberLists() {\n"
            . "    return \$this->memberList;\n"
            . "  }\n"
            . "}";

        $this->assertEquals($expectedClassCode, $classes['Type']);
    }
    public function testValidCollectionNames() {
        $ast = new Type('Type');
        $ast->setAsRoot();
        $ast->add(new CollectionItem('oneList', 'string'));
        $ast->add(new CollectionItem('otherLists', 'string'));

        $generator = new ClassGenerator('  ');
        $generator->enableTypeChecks();
        $ast->accept($generator);

        $classes = $generator->getClasses();

        $expectedClassCode  = "class Type {\n"
            . "  private \$oneList;\n"
            . "  private \$otherLists;\n"
            . "\n"
            . "  public function setOneLists(array \$oneList) {\n"
            . "    foreach (\$oneList as &\$o) {\n"
            . "      if (!is_string(\$o)) {\n"
            . "        throw new InvalidArgumentException('Invalid list. All containing elements have to be of type \"string\".');\n"
            . "      }\n"
            . "    }\n"
            . "    \$this->oneList = \$oneList;\n"
            . "    return \$this;\n"
            . "  }\n"
            . "  public function getOneLists() {\n"
            . "    return \$this->oneList;\n"
            . "  }\n"
            . "  public function setOtherLists(array \$otherLists) {\n"
            . "    foreach (\$otherLists as &\$o) {\n"
            . "      if (!is_string(\$o)) {\n"
            . "        throw new InvalidArgumentException('Invalid list. All containing elements have to be of type \"string\".');\n"
            . "      }\n"
            . "    }\n"
            . "    \$this->otherLists = \$otherLists;\n"
            . "    return \$this;\n"
            . "  }\n"
            . "  public function getOtherLists() {\n"
            . "    return \$this->otherLists;\n"
            . "  }\n"
            . "}";

        $this->assertEquals($expectedClassCode, $classes['Type']);
    }
}