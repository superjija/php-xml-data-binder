<?php
namespace PXDB\Tests\CodeGen;

use PHPUnit\Framework\TestCase;
use PXDB\CodeGen\SchemaParser;


class SchemaParserTest
    extends TestCase
{
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidSchemaFile() {
        $parser = new SchemaParser('invalidfile.xsd');
    }

    public function testEmptySchemaFileParameter() {
        $parser = new SchemaParser('');
        $this->assertTrue($parser instanceof SchemaParser);
    }

}