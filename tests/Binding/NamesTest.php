<?php
namespace PXDB\Tests\Binding;

use PHPUnit\Framework\TestCase;
use PXDB\Binding\Names;


class NamesTest
    extends TestCase
{
    public function testAttributeNames() {
        $this->assertEquals('lowercase', Names::getAttributeName('lowercase'));
        $this->assertEquals('camelCased', Names::getAttributeName('camelCased'));
        $this->assertEquals('camelCased', Names::getAttributeName('camel_Cased'));
        $this->assertEquals('camelCased', Names::getAttributeName('camel_cased'));
        $this->assertEquals('camelCased', Names::getAttributeName('camel-Cased'));
        $this->assertEquals('camelCased', Names::getAttributeName('camel-cased'));
        $this->assertEquals('UPPERCASED', Names::getAttributeName('UPPERCASED'));
        $this->assertEquals('name', Names::getAttributeName('Name'));
        $this->assertEquals('NAme', Names::getAttributeName('NAme'));

        $this->assertEquals('nameWithUPperANdLowercase', Names::getAttributeName('Name-With-UPper-aNd-Lowercase'));
        $this->assertEquals('nameWithUPPERCase', Names::getAttributeName('Name-With-UPPER-case'));
    }

    public function testListAttributeNames() {
        $this->assertEquals('lowercaseList', Names::getListAttributeName('lowercase'));
        $this->assertEquals('lowercaseList', Names::getListAttributeName('lowercaseList'));
        $this->assertEquals('list', Names::getListAttributeName('list'));
        $this->assertEquals('abcList', Names::getListAttributeName('abc'));
    }
}