<?php
namespace PXDB\Tests\Runtime;

use PHPUnit\Framework\TestCase;
use PXDB\AST\Collection;
use PXDB\AST\CollectionItem;
use PXDB\AST\Structure;
use PXDB\AST\StructureElement;
use PXDB\AST\StructureType;
use PXDB\AST\Type;
use PXDB\AST\TypeAttribute;
use PXDB\Binding\Names;
use PXDB\Runtime\Binding;
use PXDB\Tests\ExtendedTestCase;


class BindingTest
    extends ExtendedTestCase
{
    public function testBooksBinding() {
        $filepath = dirname(__FILE__) . '/../_files/Books';
        $bindingFile = $filepath . '/binding.xml';
        $expectedXml = file_get_contents($bindingFile);

        $binding = new Binding($bindingFile);

        $asts = $binding->parse();
        $this->assertEquals(2, count($asts));

        // first tree/type
        $expectedAst1 = new Type('Collection');
        $expectedAst1->setType('Collection');
        $expectedAst1->setAsRoot();
        $collection = new Collection('books');
        $collection->setGetMethod('getBooks');
        $collection->setSetMethod('setBooks');
        $structure = new Structure('book');
        $structure->setType('BookType');

        $expectedAst1->add(
            $collection->add(
                $structure
            )
        );

        $this->assertEquals($expectedAst1, $asts[0]);

        // second tree/type
        $expectedAst2 = new Type('bookType');
        $expectedAst2->setType('BookType');

        $value1 = new TypeAttribute('name');
        $value1->setStyle('element');
        $value1->setGetMethod('getName');
        $value1->setSetMethod('setName');

        $expectedAst2->add($value1);

        $value2 = new TypeAttribute('ISBN');
        $value2->setStyle('element');
        $value2->setGetMethod('getISBN');
        $value2->setSetMethod('setISBN');

        $expectedAst2->add($value2);

        $value3 = new TypeAttribute('price');
        $value3->setStyle('element');
        $value3->setGetMethod('getPrice');
        $value3->setSetMethod('setPrice');

        $expectedAst2->add($value3);

        $value4 = new Collection('authors');
        $value4->setGetMethod('getAuthorNames');
        $value4->setSetMethod('setAuthorNames');
        $value4item = new CollectionItem('authorName');
        $value4->add($value4item);

        $expectedAst2->add($value4);

        $value5 = new TypeAttribute('description');
        $value5->setStyle('element');
        $value5->setGetMethod('getDescription');
        $value5->setSetMethod('setDescription');

        $expectedAst2->add($value5);

        $value6 = new Structure('promotion');
        $value6->setStructureType(StructureType::CHOICE());
        $value6Item1 = new StructureElement('Discount');
        $value6Item1->setTestMethod('isPromotionDiscount');
        $value6Item1->setGetMethod('getPromotionDiscount');
        $value6Item1->setSetMethod('setPromotionDiscount');
        $value6Item1->setStyle('element');

        $value6Item2 = new StructureElement('None');
        $value6Item2->setTestMethod('isPromotionNone');
        $value6Item2->setGetMethod('getPromotionNone');
        $value6Item2->setSetMethod('setPromotionNone');
        $value6Item2->setStyle('element');

        $value6->add($value6Item1);
        $value6->add($value6Item2);

        $expectedAst2->add($value6);

        $value7 = new TypeAttribute('publicationDate');
        $value7->setStyle('element');
        $value7->setGetMethod('getPublicationDate');
        $value7->setSetMethod('setPublicationDate');

        $expectedAst2->add($value7);

        $value8 = new TypeAttribute('bookCategory');
        $value8->setStyle('element');
        $value8->setGetMethod('getBookCategory');
        $value8->setSetMethod('setBookCategory');

        $expectedAst2->add($value8);

        $value9 = new TypeAttribute('itemId');
        $value9->setStyle('attribute');
        $value9->setGetMethod('getItemId');
        $value9->setSetMethod('setItemId');

        $expectedAst2->add($value9);

        $value10 = new TypeAttribute('itemCode');
        $value10->setStyle('attribute');
        $value10->setGetMethod('getItemCode');
        $value10->setSetMethod('setItemCode');

        $expectedAst2->add($value10);

        $this->assertEquals($expectedAst2, $asts[1]);
    }
}