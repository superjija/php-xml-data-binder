<?php
class Collection {
	private $bookList;

	public function setBooks($bookList) {
		$this->bookList = $bookList;
		return $this;
	}
	public function getBooks() {
		return $this->bookList;
	}
}