<?php
class BookType {
	private $name;
	private $ISBN;
	private $price;
	private $authorNameList;
	private $description;
	private $promotionSelect = -1;
	private $PROMOTION_DISCOUNT_CHOICE = 0;
	private $PROMOTION_NONE_CHOICE = 1;
	private $promotionDiscount;
	private $promotionNone;
	private $publicationDate;
	private $bookCategory;
	private $itemId;
	private $itemCode;

	public function setName(string $name) {
		$this->name = $name;
		return $this;
	}
	public function getName() {
		return $this->name;
	}
	public function setISBN(int $ISBN) {
		$this->ISBN = $ISBN;
		return $this;
	}
	public function getISBN() {
		return $this->ISBN;
	}
	public function setPrice(string $price) {
		$this->price = $price;
		return $this;
	}
	public function getPrice() {
		return $this->price;
	}
	public function setAuthorNames(array $authorNameList) {
		foreach ($authorNameList as &$a) {
			if (!is_string($a)) {
				throw new InvalidArgumentException('Invalid list. All containing elements have to be of type "string".');
			}
		}
		$this->authorNameList = $authorNameList;
		return $this;
	}
	public function getAuthorNames() {
		return $this->authorNameList;
	}
	public function setDescription(string $description = NULL) {
		$this->description = $description;
		return $this;
	}
	public function getDescription() {
		return $this->description;
	}
	public function clearPromotionSelect() {
		$this->promotionSelect = -1;
        return $this;
	}
	public function isPromotionDiscount() {
		return $this->promotionSelect == $this->PROMOTION_DISCOUNT_CHOICE;
	}
	public function setPromotionDiscount(string $promotionDiscount) {
		$this->setPromotionSelect($this->PROMOTION_DISCOUNT_CHOICE);
		$this->promotionDiscount = $promotionDiscount;
		return $this;
	}
	public function getPromotionDiscount() {
		return $this->promotionDiscount;
	}
	public function isPromotionNone() {
		return $this->promotionSelect == $this->PROMOTION_NONE_CHOICE;
	}
	public function setPromotionNone(string $promotionNone) {
		$this->setPromotionSelect($this->PROMOTION_NONE_CHOICE);
		$this->promotionNone = $promotionNone;
		return $this;
	}
	public function getPromotionNone() {
		return $this->promotionNone;
	}
	public function setPublicationDate(DateTime $publicationDate) {
		$this->publicationDate = $publicationDate;
		return $this;
	}
	public function getPublicationDate() {
		return $this->publicationDate;
	}
	public function setBookCategory($bookCategory) {
		if (($bookCategory != 'magazine') && ($bookCategory != 'novel') && ($bookCategory != 'fiction') && ($bookCategory != 'other')) {
			throw new InvalidArgumentException('Unexpected value "' . $bookCategory . '". Expected is one of the following: "magazine", "novel", "fiction", "other".');
		}
		$this->bookCategory = $bookCategory;
		return $this;
	}
	public function getBookCategory() {
		return $this->bookCategory;
	}
	public function setItemId(string $itemId) {
		$this->itemId = $itemId;
		return $this;
	}
	public function getItemId() {
		return $this->itemId;
	}
	public function setItemCode(string $itemCode = NULL) {
		$this->itemCode = $itemCode;
		return $this;
	}
	public function getItemCode() {
		return $this->itemCode;
	}
	private function setPromotionSelect($choice) {
		if ($this->promotionSelect == -1) {
			$this->promotionSelect = $choice;
		} elseif ($this->promotionSelect != $choice) {
			throw new RuntimeException('Need to call clearPromotionSelect() before changing existing choice');
		}
		return $this;
	}
}