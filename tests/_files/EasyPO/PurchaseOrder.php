<?php
class PurchaseOrder {
	private $customer;
	private $date;
	private $lineItemList;
	private $shipper;

	public function setCustomer($customer) {
		$this->customer = $customer;
        return $this;
	}
	public function getCustomer() {
		return $this->customer;
	}
	public function setDate($date) {
		$this->date = $date;
        return $this;
	}
	public function getDate() {
		return $this->date;
	}
	public function setLineItems($lineItemList) {
		$this->lineItemList = $lineItemList;
        return $this;
	}
	public function getLineItems() {
		return $this->lineItemList;
	}
	public function setShipper($shipper = NULL) {
		$this->shipper = $shipper;
        return $this;
	}
	public function getShipper() {
		return $this->shipper;
	}
}