<?php
class LineItem {
	private $description;
	private $perUnitOunces;
	private $price;
	private $quantity;

	public function setDescription(string $description) {
		$this->description = $description;
        return $this;
	}
	public function getDescription() {
		return $this->description;
	}
	public function setPerUnitOunces($perUnitOunces) {
		$this->perUnitOunces = $perUnitOunces;
        return $this;
	}
	public function getPerUnitOunces() {
		return $this->perUnitOunces;
	}
	public function setPrice($price) {
		$this->price = $price;
        return $this;
	}
	public function getPrice() {
		return $this->price;
	}
	public function setQuantity(int $quantity) {
		$this->quantity = $quantity;
        return $this;
	}
	public function getQuantity() {
		return $this->quantity;
	}
}