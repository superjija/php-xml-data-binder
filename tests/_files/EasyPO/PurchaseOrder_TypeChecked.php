<?php
class PurchaseOrder {
	private $customer;
	private $date;
	private $lineItemList;
	private $shipper;

	public function setCustomer(Customer $customer) {
		$this->customer = $customer;
        return $this;
	}
	public function getCustomer() {
		return $this->customer;
	}
	public function setDate($date) {
		$this->date = $date;
        return $this;
	}
	public function getDate() {
		return $this->date;
	}
	public function setLineItems(array $lineItemList) {
		foreach ($lineItemList as &$l) {
			if (get_class($l) !== 'LineItem') {
				throw new InvalidArgumentException('Invalid list. All containing elements have to be of type "LineItem".');
			}
		}
		$this->lineItemList = $lineItemList;
        return $this;
	}
	public function getLineItems() {
		return $this->lineItemList;
	}
	public function setShipper(Shipper $shipper) {
		$this->shipper = $shipper;
        return $this;
	}
	public function getShipper() {
		return $this->shipper;
	}
}