<?php
class Customer {
	private $name;
	private $address;
	private $age;
	private $moo;
	private $poo;

	public function setName($name) {
		$this->name = $name;
		return $this;
	}
	public function getName() {
		return $this->name;
	}
	public function setAddress($address) {
		$this->address = $address;
        return $this;
	}
	public function getAddress() {
		return $this->address;
	}
	public function setAge($age) {
		$this->age = $age;
        return $this;
	}
	public function getAge() {
		return $this->age;
	}
	public function setMoo($moo = NULL) {
		$this->moo = $moo;
        return $this;
	}
	public function getMoo() {
		return $this->moo;
	}
	public function setPoo($poo = NULL) {
		$this->poo = $poo;
        return $this;
	}
	public function getPoo() {
		return $this->poo;
	}
}