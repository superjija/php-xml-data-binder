<?php
class Shipper {
	private $name;
	private $perOunceRate;

	public function setName(string $name) {
		$this->name = $name;
        return $this;
	}
	public function getName() {
		return $this->name;
	}
	public function setPerOunceRate($perOunceRate) {
		$this->perOunceRate = $perOunceRate;
        return $this;
	}
	public function getPerOunceRate() {
		return $this->perOunceRate;
	}
}