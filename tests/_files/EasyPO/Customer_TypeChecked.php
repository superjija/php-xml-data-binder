<?php
class Customer {
	private $name;
	private $address;
	private $age;
	private $moo;
	private $poo;

	public function setName(string $name) {
		$this->name = $name;
        return $this;
	}
	public function getName() {
		return $this->name;
	}
	public function setAddress(string $address) {
		$this->address = $address;
        return $this;
	}
	public function getAddress() {
		return $this->address;
	}
	public function setAge(int $age) {
		$this->age = $age;
        return $this;
	}
	public function getAge() {
		return $this->age;
	}
	public function setMoo(int $moo = null) {
		$this->moo = $moo;
        return $this;
	}
	public function getMoo() {
		return $this->moo;
	}
	public function setPoo(int $poo =null) {
		$this->poo = $poo;
        return $this;
	}
	public function getPoo() {
		return $this->poo;
	}
}