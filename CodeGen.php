<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */
/**
 * CodeGen is a command-line interface for CodeGen.
*/
require_once 'autoload.php';
print "src - CodeGen\n";

$options = [];

for ($i = 2; $i < $argc; $i++) {
    $value = $argv[$i];

    if ($value == '--typechecks') {
        $options['typechecks'] = TRUE;
    }
}

$c = new PXDB\CodeGen('./output', $options['typechecks']);
$result = $c->generate($argv[1]);
var_dump($result);
