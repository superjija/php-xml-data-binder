<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */
use PXDB\Runtime\Binding;
use PXDB\Runtime\Unmarshaller;


require_once 'autoload.php';
require_once './output/BookType.php';
require_once './output/Collection.php';

$xml = <<<XML
<?xml version="1.0"?>
<Collection>
  <books>
    <book itemId="0001">
      <name>The Hitchhiker's Guide to the Galaxy</name>
      <ISBN>1400052936</ISBN>
      <price>$ 23.10</price>
      <authors>
        <authorName>Douglas Adams</authorName>
        <authorName>Arthur Dent</authorName>
        <authorName>Zaphod Beeblebrox</authorName>
      </authors>
      <description>Join Douglas Adams' hapless hero Arthur Dent as he travels the galaxy with his intrepid pal Ford Prefect [...]</description>
      <promotion>
        <Discount>7%</Discount>
      </promotion>
      <publicationDate>2004-10-19</publicationDate>
      <bookCategory>fiction</bookCategory>
    </book>
    <book itemId="0002">
      <name>The Restaurant at the End of the Universe</name>
      <ISBN>345418920</ISBN>
      <price>$ 11.20</price>
      <authors>
        <authorName>Douglas Adams</authorName>
        <authorName>Ford Prefect</authorName>
      </authors>
      <description>Warning! This second volume in the "Hitchhiker's Guide to the Galaxy" series is definitely not a standalone book. [...]</description>
      <promotion>
        <None>Regular price</None>
      </promotion>
      <publicationDate>2005-04-26</publicationDate>
      <bookCategory>fiction</bookCategory>
    </book>
  </books>
</Collection>
XML;

$binding = new Binding('./output/binding.xml');
$unmarshaller = new Unmarshaller($binding);

try {
    $collectionObject = $unmarshaller->unmarshal($xml);
    /** @var BookType[] $books */
    $books = $collectionObject->getBooks();
    $bookCount = count($books);

    echo "Type: " . get_class($collectionObject) . "\n";
    echo "Has " . $bookCount . " books.\n";

    for ($i = 0; $i < $bookCount; $i++) {
        $book = $books[$i];
        echo "Book " . $i . ": " . $book->getName() . "\n";
    }
}
catch (Exception $e) {
    print "Unmarshalling exception: " . $e->getMessage();
}