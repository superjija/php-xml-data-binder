<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */
use PXDB\Runtime\Binding;
use PXDB\Runtime\Marshaller;


require_once 'autoload.php';
require_once './output/BookType.php';
require_once './output/Collection.php';

$c = new \Collection();

$book1 = new \BookType();
$book1->setName('The Hitchhiker\'s Guide to the Galaxy');
$book1->setIsbn(1400052936);
$book1->setPrice('$ 23.10');
$book1->setAuthorNames(['Douglas Adams', 'Arthur Dent', 'Zaphod Beeblebrox']);
$book1->setDescription('Join Douglas Adams\' hapless hero Arthur Dent as he travels the galaxy with his intrepid pal Ford Prefect [...]');
$book1->setPromotionDiscount('7%');
$book1->setPublicationdate('2004-10-19');
$book1->setBookcategory('fiction');
$book1->setItemId("0001");

$book2 = new BookType();
$book2->setName('The Restaurant at the End of the Universe');
$book2->setIsbn(345418920);
$book2->setPrice('$ 11.20');
$book2->setAuthorNames(['Douglas Adams', 'Ford Prefect']);
$book2->setDescription(
    'Warning! This second volume in the "Hitchhiker\'s Guide to the Galaxy" series is definitely not a standalone book. [...]'
);
$book2->setPromotionNone('Regular price');
$book2->setPublicationdate('2005-04-26');
$book2->setBookcategory('fiction');
$book2->setItemId("0002");

$list = [$book1, $book2];

$c->setBooks($list);

$binding = new Binding('./output/binding.xml');
$marshaller = new Marshaller($binding);

try {
    $xml = $marshaller->marshal($c);
}
catch (Exception $e) {
    print "Marshalling exception: " . $e->getMessage();
}

print "-----------------\n";
print $xml;
print "\n-----------------\n";
print "Schema validation: ";

$dom = new \DOMDocument();
$dom->loadXML($xml);
if ($dom->schemaValidate('./tests/_files/Books/books.xsd')) {
    print "Successful.\n";
}

print "\n";
exit();