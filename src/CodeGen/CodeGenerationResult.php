<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

namespace PXDB\CodeGen;


class CodeGenerationResult
{
    private $schemaFile;
    private $outpuDir;
    private $types;
    private $typesAfterOptimalization;
    private $generatedClasses;
    private $bingingFile;
    private $success;

    /**
     * CodeGenerationResult constructor.
     * @param $schemaFile
     * @param $outpuDir
     */
    public function __construct(string $schemaFile = NULL, string $outpuDir = NULL)
    {
        $this->schemaFile = $schemaFile;
        $this->outpuDir = $outpuDir;
        $this->success = FALSE;
    }


    /**
     * @return mixed
     */
    public function getSchemaFile()
    {
        return $this->schemaFile;
    }

    /**
     * @param mixed $schemaFile
     * @return CodeGenerationResult
     */
    public function setSchemaFile($schemaFile)
    {
        $this->schemaFile = $schemaFile;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOutpuDir()
    {
        return $this->outpuDir;
    }

    /**
     * @param mixed $outpuDir
     * @return CodeGenerationResult
     */
    public function setOutpuDir($outpuDir)
    {
        $this->outpuDir = $outpuDir;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTypes()
    {
        return $this->types;
    }

    /**
     * @param mixed $types
     * @return CodeGenerationResult
     */
    public function setTypes($types)
    {
        $this->types = $types;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTypesAfterOptimalization()
    {
        return $this->typesAfterOptimalization;
    }

    /**
     * @param mixed $typesAfterOptimalization
     * @return CodeGenerationResult
     */
    public function setTypesAfterOptimalization($typesAfterOptimalization)
    {
        $this->typesAfterOptimalization = $typesAfterOptimalization;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGeneratedClasses()
    {
        return $this->generatedClasses;
    }

    /**
     * @param mixed $generatedClasses
     * @return CodeGenerationResult
     */
    public function setGeneratedClasses($generatedClasses)
    {
        $this->generatedClasses = $generatedClasses;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBingingFile()
    {
        return $this->bingingFile;
    }

    /**
     * @param mixed $bingingFile
     * @return CodeGenerationResult
     */
    public function setBingingFile($bingingFile)
    {
        $this->bingingFile = $bingingFile;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @return CodeGenerationResult
     */
    public function setSuccessful(): CodeGenerationResult
    {
        $this->success = true;
        return $this;
    }

}