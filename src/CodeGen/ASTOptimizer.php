<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

/**
 * After the construction of the AST, the ASTOptimizer is responsible for
 * tree optimizations.
 *
 * These can include node-reductions, or simplifications of sub-trees.
*/

namespace PXDB\CodeGen;


class ASTOptimizer
{
    private $typeList;
    private $usage;

    /**
     *
     * @param \PXDB\AST\Tree[] $typeList
     * @param TypeUsage $usage
     */
    public function __construct($typeList, TypeUsage $usage)
    {
        $this->typeList = $typeList;
        $this->usage = $usage;
    }

    /**
     * Start the optimization.
     *
     * @return \PXDB\AST\Tree[] The optimized list of types.
     */
    public function optimize()
    {
        $this->removeUnusedTypes();

        return $this->typeList;
    }

    private function removeUnusedTypes()
    {
        $usages = $this->usage->getTypeUsages();

        foreach ($usages as $class => $count) {
            if ($count === 0) {
                $typeIndex = 0;
                // a single used reference can be removed
                foreach ($this->typeList as &$type) {
                    if ($type->getName() == $class) {
                        array_splice($this->typeList, $typeIndex, 1);
                        break;
                    }
                    $typeIndex++;
                }
            }
        }
    }
}