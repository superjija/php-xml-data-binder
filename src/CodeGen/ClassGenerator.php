<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

/**
 * Generating the PHP-code of the classes is done here, with a Hierarchical Visitor
 * of the AST.
 *
 * After visiting, the code can be retrieved with <code>getClasses()</code>.
 * The actual file writing has to be done separately.
*/

namespace PXDB\CodeGen;

use PXDB\AST\Collection;
use PXDB\AST\CollectionItem;
use PXDB\AST\Enumeration;
use PXDB\AST\EnumerationValue;
use PXDB\AST\Structure;
use PXDB\AST\StructureElement;
use PXDB\AST\StructureType;
use PXDB\AST\Tree;
use PXDB\AST\Type;
use PXDB\AST\TypeAttribute;
use PXDB\AST\Visitor\VisitorAbstract;
use PXDB\Binding\Names;
use PXDB\CodeGen\Code\Method;
use PXDB\CodeGen\Code\MethodParameter;
use PXDB\Util\XsdType;


class ClassGenerator implements VisitorAbstract
{
    /**
     * @var TypeCheckGenerator
     */
    private $typeChecks;
    /**
     * @var RestrictionsCheckGenerator
     */
    private $restrictionsChecks;
    /**
     * @var boolean Flag if type-check code should be included in the generated methods.
     */
    private $doTypeChecks;

    /**
     * List of class attributes. Separated by class-name => public/protected/private.
     * @var array
     */
    private $attributes;
    private $methods;
    private $TYPEHINT_ARRAY = '#array#';
    private $localClasses;
    private $indentationString;

    public function __construct($indentationString = "\t")
    {
        $this->typeChecks = new TypeCheckGenerator();
        $this->restrictionsChecks = new RestrictionsCheckGenerator();
        $this->attributes = [];
        $this->methods = [];
        $this->localClasses = [];
        $this->indentationString = $indentationString;
    }

    /**
     * @return string[] hash with class-name => class-code
     * @throws \RuntimeException
     * @throws \ReflectionException
     */
    public function getClasses()
    {
        return $this->buildClasses();
    }

    /**
     * @return array
     * @throws \RuntimeException
     * @throws \ReflectionException
     */
    private function buildClasses()
    {
        $classNames = array_keys($this->attributes);
        $classes = [];

        foreach ($classNames as $className) {
            $code = 'class ' . $className . ' {';
            $code .= $this->buildPrivateAttributesForClass($className);
            $code .= $this->buildPublicMethodsForClass($className);
            $code .= '}';

            $code = $this->prettyPrint($code);

            $classes[$className] = $code;
        }

        return $classes;
    }

    private function buildPrivateAttributesForClass($className)
    {
        $code = '';

        foreach ($this->attributes[$className]['private'] as $attribute) {
            list($attributeName, $attributeValue) = $attribute;
            $code .= 'private $' . $attributeName;
            if ($attributeValue !== '') {
                $code .= ' = ' . $attributeValue;
            }
            $code .= ';';
        }

        return $code;
    }

    /**
     * @param $className
     * @return string
     * @throws \RuntimeException
     * @throws \ReflectionException
     */
    private function buildPublicMethodsForClass($className)
    {
        $code = '';

        foreach ($this->methods[$className] as $scope => $methods) {
            /** @var Method $method */
            foreach ($methods as $method) {
                $code .= $scope . ' function ';
                $code .= $method->getName();
                $code .= '(';
                $code .= $this->getParameterListString($method);
                $code .= ')';
                $code .= ' {';
                $code .= $method->getBody();
                $code .= '}';
            }
        }

        return $code;
    }

    /**
     * @param Method $method
     * @return string
     * @throws \ReflectionException
     * @throws \RuntimeException
     */
    private function getParameterListString(Method $method)
    {
        $parameterString = '';

        /** @var MethodParameter $parameter */
        foreach ($method->getParameters() as $parameter) {
            $parameterName = $parameter->getName();
            if($this->doTypeChecks) {
                $type = MethodParameter::getParameterType($parameter);
                $parameterString = $type ? ($type . ' ') : '';
            }
            $parameterString .= '$' . $parameterName;
            if($parameter->isOptional()){
                $parameterString .= ' = NULL';
            }
        }
        return $parameterString;
    }

    private function prettyPrint($code)
    {
        $indentation = 0;
        $prettyCode = '';
        $length = strlen($code);
        $currentChar = $previousChar = '';
        $quoted = FALSE;

        for ($i = 0; $i < $length; $i++) {
            $currentChar = mb_substr($code, $i, 1);

            if ($currentChar == "'" || $currentChar == '"') {
                $quoted = !$quoted;
            }
            elseif ($currentChar == '}') {
                $indentation--;
            }
            elseif ($previousChar == '{') {
                $indentation++;
            }

            if ($indentation < 0) {
                $indentation = 0;
            }

            if (!$quoted && ($previousChar == '{' || $previousChar == ';' || ($previousChar == '}' && $currentChar != ' '))) {
                $prettyCode .= "\n";
                $prettyCode .= str_repeat($this->indentationString, $indentation);
            }

            $prettyCode .= $currentChar;
            $previousChar = $currentChar;
        }

        // for cosmetic reasons, add a separating newline between class-attributes and its methods
        $prettyCode = preg_replace('/^(\s*(?:private|protected|public) function)/im', "\n$1", $prettyCode, 1);

        return $prettyCode;
    }

    /**
     * Enables type-checks in generated set-methods.
     * This enforces valid formats/values as parameters.
     */
    public function enableTypeChecks()
    {
        $this->doTypeChecks = TRUE;
    }

    /**
     * disables type-checks in generated set-methods.
     * The generated set-methods are plain setters.
     */
    public function disableTypeChecks()
    {
        $this->doTypeChecks = FALSE;
    }

    public function visitCollectionEnter(Collection $collection)
    {
        // Collection informations are used when CollectionItem-nodes are traversed
        return TRUE;
    }

    public function visitCollectionLeave(Collection $collection)
    {
        return TRUE;
    }

    /**
     * @param CollectionItem $collectionItem
     * @throws \RuntimeException
     */
    public function visitCollectionItem(CollectionItem $collectionItem)
    {
        $name = Names::getListAttributeName($collectionItem->getName());

        $this->addPrivateMember($name);
        $this->addSetterFor($collectionItem);//TODO: add parameter $name, to pass pre-defined name?
        $this->addGetterFor($collectionItem);

        return;
    }

    /**
     * @param Enumeration $enumeration
     * @return bool
     * @throws \RuntimeException
     */
    public function visitEnumerationEnter(Enumeration $enumeration)
    {
        $name = $enumeration->getName();
        $parent = $enumeration->getParent();

        if ($name == '' && $this->enumerationIsAnOwnType($enumeration)) {
            $name = $parent->getName();
            $attributeName = Names::getAttributeName($name);
            $methodName = Names::getCamelCasedName($name);

            $this->addPrivateMember($attributeName);
            $this->addSetterFor($enumeration);
            $this->addGetterFor($enumeration);

            return FALSE;
        }

        $this->addPrivateMember($name);

        $this->addSetterFor($enumeration);
        $this->addGetterFor($enumeration);

        return TRUE;
    }

    public function visitEnumerationLeave(Enumeration $enumeration)
    {
        if ($enumeration->getParent() == NULL) {
            return FALSE;
        }

        return TRUE;
    }

    public function visitEnumerationValue(EnumerationValue $enumerationValue)
    {
    }

    /**
     * @param Structure $structure
     * @return bool
     * @throws \RuntimeException
     */
    public function visitStructureEnter(Structure $structure)
    {
        $name = $structure->getName();
        $structureType = $structure->getStructureType();

        if ($structureType === StructureType::CHOICE()) {
            if ($name != '') {
                $selectionAttribute = $name . 'Select';
            }
            else {
                $selectionAttribute = 'choiceSelect';
            }
            $this->addPrivateMember($selectionAttribute, '-1');
            $childrenCount = $structure->countChildren();
            for ($i = 0; $i < $childrenCount; $i++) {
                $child = $structure->get($i);

                $childName = $child->getName();
                $attributeName = '';
                if ($name != '') {
                    $attributeName = $name . '_';
                }
                $attributeName = $attributeName . $childName . '_CHOICE';
                $attributeName = strtoupper($attributeName);

                $this->addPrivateMember($attributeName, $i);
            }

            $methodName = ucfirst($selectionAttribute);
            $parameter = new MethodParameter('choice', '');
            $parameters = [$parameter];
            $body = 'if ($this->' . $selectionAttribute . ' == -1) {';
            $body .= '$this->' . $selectionAttribute . ' = $choice;';
            $body .= '} elseif ($this->' . $selectionAttribute . ' != $choice) {';
            $body .= 'throw new RuntimeException(\'Need to call clear' . $methodName . '() before changing existing choice\');';
            $body .= '}';
            $body .= 'return $this;';
            $this->addPrivateMethod('set' . $methodName, $parameters, $body);
            $body = '$this->' . $selectionAttribute . ' = -1;';
            $body .= 'return $this;';
            $this->addPublicMethod('clear' . $methodName, [], $body);
        }

        return TRUE;
    }

    public function visitStructureLeave(Structure $structure)
    {
        return TRUE;
    }

    /**
     * @param StructureElement $structureElement
     * @return bool
     */
    public function visitStructureElementEnter(StructureElement $structureElement)
    {
        $name = $structureElement->getName();
        $parent = $structureElement->getParent();
        $structureType = $parent->getStructureType();

        if ($structureType === StructureType::CHOICE()) {
            $parentName = $parent->getName();

            if ($parentName == '') {
                $attributeName = $name;
                $choiceConstant = $name . '_CHOICE';
                $choiceMember = 'choiceSelect';
            }
            else {
                $attributeName = $parentName . ucfirst($name);
                $choiceConstant = $parentName . '_' . $name . '_CHOICE';
                $choiceMember = $parentName . 'Select';
            }

            $this->addPrivateMember($attributeName);

            $choiceConstant = strtoupper($choiceConstant);
            $methodName = 'is' . ucfirst($attributeName);
            $parameters = [];
            $body = 'return $this->' . $choiceMember . ' == $this->' . $choiceConstant . ';';
            $this->addPublicMethod($methodName, $parameters, $body);

            $methodName = 'set' . ucfirst($attributeName);
            $parameter = new MethodParameter($attributeName, $structureElement->getType());
            $parameters = [$parameter];
            $body = '$this->set' . ucfirst($choiceMember) . '($this->' . $choiceConstant . ');';
            $body .= '$this->' . $attributeName . ' = $' . $attributeName . ';';
            $body .= 'return $this;';
            $this->addPublicMethod($methodName, $parameters, $body);

            $methodName = 'get' . ucfirst($attributeName);
            $parameters = [];
            $body = 'return $this->' . $attributeName . ';';
            $this->addPublicMethod($methodName, $parameters, $body);
        }

        return TRUE;
    }

    public function visitStructureElementLeave(StructureElement $structureElement)
    {
        return TRUE;
    }

    /**
     * @param Type $type
     * @return bool
     * @throws \RuntimeException
     * @throws \ReflectionException
     */
    public function visitTypeEnter(Type $type)
    {
        $name = $type->getName();
        $typesType = $type->getType();

        $this->currentClassName = Names::createClassnameFor($type);
        $this->addClass($this->currentClassName);

        if (!$type->hasChildren()) {
            $methodName = Names::getCamelCasedName($name);

            if (!XsdType::isBaseType($typesType)) {
                // complexTypes (i.e. classes) have to be type-hinted
                // in the method signature.
                $expectedType = Names::createClassnameFor($type);
            }

            $this->addPrivateMember($name);
            $this->addSetterFor($type);
            $this->addGetterFor($type);
        }

        return TRUE;
    }

    private function addClass($name)
    {
        $initialScopeElements = [
            'public' => [],
            'protected' => [],
            'private' => [],
        ];

        $this->attributes[$name] = $initialScopeElements;
        $this->methods[$name] = $initialScopeElements;
        $this->localClasses[$name] = $initialScopeElements;
    }

    private function addPrivateMember($name, $initialValue = '')
    {
        $this->attributes[$this->currentClassName]['private'][] = [$name, $initialValue];
    }

    private function addPublicMethod($name, array $parameter, $body)
    {
        $method = new Method('public', $name);
        $method->setParameters($parameter)
            ->setBody($body)
        ;
        $this->methods[$this->currentClassName]['public'][] = $method;
    }

    private function addPrivateMethod($name, array $parameter, $body)
    {
        $method = new Method('public', $name);
        $method->setParameters($parameter)
            ->setBody($body)
        ;
        $this->methods[$this->currentClassName]['private'][] = $method;
    }

    public function visitTypeLeave(Type $type)
    {
        return TRUE;
    }

    public function visitTypeAttributeEnter(TypeAttribute $typeAttribute)
    {
        if ($typeAttribute->countChildren() == 0) {
            $name = $typeAttribute->getName();
            $type = $typeAttribute->getType();
            // base type attribute
            $attributeName = Names::getAttributeName($name);

            $this->addPrivateMember($attributeName);
            $this->addSetterFor($typeAttribute);
            $this->addGetterFor($typeAttribute);

            return FALSE;
        }
        else {
            return TRUE;
        }
    }

    /**
     * @param Tree $tree
     * @throws \ReflectionException
     * @throws \RuntimeException
     */
    protected function addSetterFor(Tree $tree)
    {
        $name = $tree->getName();
        $type = $tree->getType();
        $attributeName = Names::getAttributeName($name);
        $methodName = Names::getCamelCasedName($name);
        $typeCheckCode = '';

        if ($tree instanceof CollectionItem) {
            $methodName = $this->buildPlural($methodName);
            $attributeName = Names::getListAttributeName($name);
            $type = $this->TYPEHINT_ARRAY;
            if ($this->doTypeChecks) {
                $typeCheckCode = $this->typeChecks->getListTypeCheckFor($tree, $attributeName);
            }
        }
        elseif ($tree instanceof Enumeration) {
            if ($this->enumerationIsAnOwnType($tree)) {
                $name = $tree->getParent()->getName();
                $attributeName = Names::getAttributeName($name);
                $methodName = Names::getCamelCasedName($name);
            }

            $firstEnumerationValue = $tree->get(0);
            // the type is stored in the actual EnumerationValue nodes, not in the Enumeration itself
            $type = $firstEnumerationValue->getType();
            if ($this->doTypeChecks) {
                $typeCheckCode = $this->typeChecks->getEnumerationTypeCheckFor($tree, $attributeName);
            }
        }

        if ($tree instanceof TypeAttribute) {
            $restrictionTests = $this->restrictionsChecks->getSimpleTypeAttributeChecks($tree);
        }
        else {
            $restrictionTests = '';
        }
        $parameter = new MethodParameter($attributeName, $type, $tree instanceof TypeAttribute && $tree->isOptional());
        $parameters = [$parameter];
        $body = $typeCheckCode . $restrictionTests. '$this->' . $attributeName . ' = $' . $attributeName . ';return $this;';

        $this->addPublicMethod('set' . $methodName, $parameters, $body);
    }


    protected function addGetterFor(Tree $tree)
    {
        $name = $tree->getName();
        $type = $tree->getType();
        $attributeName = Names::getAttributeName($name);
        $methodName = Names::getCamelCasedName($name);

        if ($tree instanceof CollectionItem) {
            $methodName = $this->buildPlural($methodName);
            $attributeName = Names::getListAttributeName($name);
        }
        elseif ($tree instanceof Enumeration && $this->enumerationIsAnOwnType($tree)) {
            $name = $tree->getParent()->getName();
            $attributeName = Names::getAttributeName($name);
            $methodName = Names::getCamelCasedName($name);
        }
        switch ($type) {
            case('date'):
                $body = 'return new \PXDB\Util\Date($this->' . $attributeName . ');';
                break;
            case('dateTime'):
                $body = 'return new \PXDB\Util\DateTime($this->' . $attributeName . ');';
                break;
            default:
                $body = 'return $this->' . $attributeName . ';';
        }

        $this->addPublicMethod('get' . $methodName, [], $body);
    }

    private function enumerationIsAnOwnType(Enumeration $enumeration)
    {
        $parent = $enumeration->getParent();

        return $parent instanceof Type && $parent->isRoot();
    }

    public function visitTypeAttributeLeave(TypeAttribute $typeAttribute)
    {
        return TRUE;
    }

    private function buildPlural($name)
    {
        $lastCharacter = substr($name, -1);
        if (strtolower($lastCharacter) == 's') {
            return $name;
        }

        return $name . 's';
    }
}
