<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

namespace PXDB\CodeGen;

use PXDB\AST\Tree;
use PXDB\ParseTree\AttributeNode;
use PXDB\ParseTree\ChoiceNode;
use PXDB\ParseTree\ComplexContentNode;
use PXDB\ParseTree\ComplexTypeNode;
use PXDB\ParseTree\ElementNode;
use PXDB\ParseTree\EnumerationNode;
use PXDB\ParseTree\ExtensionNode;
use PXDB\ParseTree\RestrictionConditionNode;
use PXDB\ParseTree\RestrictionNode;
use PXDB\ParseTree\SequenceNode;
use PXDB\ParseTree\SimpleTypeNode;
use PXDB\ParseTree\Visitor\VisitorAbstract;


class ASTCreator implements VisitorAbstract
{
    /** @var ASTFactory $factory */
    protected $factory;

    /** @var Tree[] $asts*/
    protected $asts;

    /** @var int  $currentLevel*/
    protected $currentLevel;

    /** @var int $lastLevel */
    protected $lastLevel;

    /** @var Tree[] $subtrees*/
    protected $subtrees;

    public function __construct()
    {
        $this->factory = new ASTFactory();
        $this->currentLevel = $this->lastLevel = 0;
        $this->subtrees = [];
        $this->asts = [];
    }

    /**
     * @return Tree[]
     */
    public function getTypeList()
    {
        // be sure that all remaining level 0 (i.e. root level) types are built
        $this->handleTypeCreationForLevel(-1);

        return $this->asts;
    }

    public function visitAttributeNode(AttributeNode $attribute)
    {
        $this->currentLevel = $attribute->getLevel();
        $this->handleTypeCreationForLevel($this->currentLevel);
        $ast = $this->factory->createFromAttributeNode($attribute);

        $this->addSubtreeToAST($ast);
        $this->lastLevel = $this->currentLevel;
    }

    /**
     * @param ElementNode $element
     * @throws \RuntimeException
     */
    public function visitElementNode(ElementNode $element)
    {
        $this->currentLevel = $element->getLevel();
        $this->handleTypeCreationForLevel($this->currentLevel);

        $ast = $this->factory->createFromElementNode($element);
        if($ast) {
            $this->addSubtreeToAST($ast);
        }
        $this->lastLevel = $this->currentLevel;
    }

    public function visitSimpleTypeNode(SimpleTypeNode $simpleType)
    {
        $this->currentLevel = $simpleType->getLevel();
        $this->handleTypeCreationForLevel($this->currentLevel);
        $ast = $this->factory->createFromSimpleTypeNode($simpleType);
        if ($ast != NULL) {
            $this->currentLevel--;
            $this->addSubtreeToAST($ast);
        }
        $this->lastLevel = $this->currentLevel;
    }

    /**
     * @param ComplexTypeNode $complexType
     * @throws \RuntimeException
     */
    public function visitComplexTypeNode(ComplexTypeNode $complexType)
    {
        $this->currentLevel = $complexType->getLevel();
        $this->handleTypeCreationForLevel($this->currentLevel);

        $ast = $this->factory->createFromComplexTypeNode($complexType);
        if ($ast != NULL) {
            $this->addSubtreeToAST($ast);
        }
        $this->lastLevel = $this->currentLevel;
    }

    public function visitSequenceNode(SequenceNode $sequence)
    {
        $this->currentLevel = $sequence->getLevel();
        $this->handleTypeCreationForLevel($this->currentLevel);
        $this->lastLevel = $this->currentLevel;
    }

    public function visitGroupNode(\PXDB\ParseTree\Tree $group)
    {
        $this->currentLevel = $group->getLevel();
        $this->handleTypeCreationForLevel($this->currentLevel);
        $this->lastLevel = $this->currentLevel;
    }

    public function visitAllNode(\PXDB\ParseTree\Tree $all)
    {
        $this->currentLevel = $all->getLevel();
        $this->handleTypeCreationForLevel($this->currentLevel);
        $this->lastLevel = $this->currentLevel;
    }

    public function visitChoiceNode(ChoiceNode $choice)
    {
        $this->currentLevel = $choice->getLevel();
        $this->handleTypeCreationForLevel($this->currentLevel);
        $ast = $this->factory->createFromChoiceNode($choice);
        if ($ast !== NULL) {
            $this->addSubtreeToAST($ast);
        }
        $this->lastLevel = $this->currentLevel;
    }

    public function visitRestrictionNode(RestrictionNode $restriction)
    {
        $this->currentLevel = $restriction->getLevel();
        $this->handleTypeCreationForLevel($this->currentLevel);
        $ast = $this->factory->createFromRestrictionNode($restriction);
        if ($ast !== NULL) {
            $this->addSubtreeToAST($ast);
        }
        $this->lastLevel = $this->currentLevel;
    }

    public function visitRestrictionConditionNode(RestrictionConditionNode $restriction)
    {
        $this->currentLevel = $restriction->getLevel();
        $this->handleTypeCreationForLevel($this->currentLevel);
        $ast = $this->factory->createFromRestrictionConditionNode($restriction);
        if ($ast !== NULL) {
            $this->addSubtreeToAST($ast);
        }
        $this->lastLevel = $this->currentLevel;
    }

    public function visitEnumerationNode(EnumerationNode $enumeration)
    {
        $this->currentLevel = $enumeration->getLevel();
        $this->handleTypeCreationForLevel($this->currentLevel);

        $ast = $this->factory->createFromEnumerationNode($enumeration);
        if ($ast !== NULL) {
            $this->addSubtreeToAST($ast);
        }

        $this->lastLevel = $this->currentLevel;
    }

    public function visitComplexContentNode(ComplexContentNode $complexType)
    {
        $this->currentLevel = $complexType->getLevel();
        $this->handleTypeCreationForLevel($this->currentLevel);
        $this->lastLevel = $this->currentLevel;
    }

    public function visitExtensionNode(ExtensionNode $extension)
    {
        $this->currentLevel = $extension->getLevel();
        $this->handleTypeCreationForLevel($this->currentLevel);
        $this->lastLevel = $this->currentLevel;
    }

    /**
     * @param $parseTreeLevel
     */
    private function handleTypeCreationForLevel($parseTreeLevel)
    {
        if ($this->parsedElementsAreCreatableInLevel($parseTreeLevel)) {
            $previousSubtrees = NULL;
            $previousSubtrees = end($this->subtrees);
            $subtrees = prev($this->subtrees);

            do {
                if ($subtrees === FALSE) {
                    break;
                }
                /** @var Tree $lastElementInCurrentSubtree */
                $lastElementInCurrentSubtree = $subtrees[count($subtrees) - 1];
                /** @var Tree $previousSubtree */
                foreach ($previousSubtrees as &$previousSubtree) {
                    $lastElementInCurrentSubtree->add($previousSubtree);
                }

                $previousSubtrees = $subtrees;
            } while ($subtrees = prev($this->subtrees));

            if ($parseTreeLevel <= 0) {
                $this->asts = array_merge($this->asts, $previousSubtrees);
            }

            $this->unsetCurrentSubtrees($parseTreeLevel);
        }
    }

    private function unsetCurrentSubtrees($parseTreeLevel)
    {
        foreach ($this->subtrees as $level => &$subteesInLevel) {
            if ($level < $parseTreeLevel) {
                continue;
            }

            unset($this->subtrees[$level]);
        }
    }

    private function parsedElementsAreCreatableInLevel($parseTreeLevel)
    {
        return $this->parsedElementIsParentOfLastElement($parseTreeLevel) ||
            $this->subtreeIsCreatable($parseTreeLevel);
    }

    private function subtreeIsCreatable($parseTreeLevel)
    {
        return $this->lastLevel < $parseTreeLevel
            && isset($this->subtrees[$parseTreeLevel])
            && count($this->subtrees[$parseTreeLevel]) > 0;
    }

    private function parsedElementIsParentOfLastElement($parseTreeLevel)
    {
        return $parseTreeLevel < $this->lastLevel;
    }

    private function addSubtreeToAST(Tree $ast)
    {
        $this->subtrees[$this->currentLevel][] = $ast;
    }
}
