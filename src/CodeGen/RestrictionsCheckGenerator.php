<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

/**
 * When generating class-code, it is possible to add extended type checks
 * into the methods.
 * This ensures only valid values can be passed to the methods.
 *
 * TypeCheckGenerator can be used for XSD base-types as well as generated classes.
*/

namespace PXDB\CodeGen;

use PXDB\AST\Collection;
use PXDB\AST\CollectionItem;
use PXDB\AST\Enumeration;
use PXDB\AST\Tree;
use PXDB\AST\TypeAttribute;
use PXDB\Binding\Names;
use PXDB\Util\XsdType;


class RestrictionsCheckGenerator
{
    public function getSimpleTypeAttributeChecks(TypeAttribute $attribute)
    {
        $name = $attribute->getName();
        $attributeName = Names::getAttributeName($name);
        $code = '';
        foreach ($attribute->getRestrictions() as $restriction => $value) {
            if ($restriction == 'length') {
                $code .= 'if( strlen($' . $attributeName . ') != ' . $value . ' )
                    {throw new \InvalidArgumentException("Argument has not correct length.");}';
            }
            if ($restriction == 'maxLength') {
                $code .= 'if( strlen($' . $attributeName . ') > ' . $value . ' )
                    {throw new \InvalidArgumentException("Argument is too long.");}';
            }
            if ($restriction == 'minLength') {
                $code .= 'if( strlen($' . $attributeName . ') < ' . $value . ' )
                    {throw new \InvalidArgumentException("Argument is too short.");}';
            }
            if ($restriction == 'maxExclusive') {
                $code .= 'if( $' . $attributeName . ' >= ' . $value . ' )
                    {throw new \InvalidArgumentException("Argument is too big.");}';
            }
            if ($restriction == 'minExclusive') {
                $code .= 'if( $' . $attributeName . ' <= ' . $value . ' )
                    {throw new \InvalidArgumentException("Argument is too small.");}';
            }
            if ($restriction == 'maxInclusive') {
                $code .= 'if( $' . $attributeName . ' > ' . $value . ' )
                    {throw new \InvalidArgumentException("Argument is too big.");}';
            }
            if ($restriction == 'minInclusive') {
                $code .= 'if( $' . $attributeName . ' < ' . $value . ' )
                    {throw new \InvalidArgumentException("Argument is too small.");}';
            }
            if ($restriction == 'fractionDigits') {
                $code .= 'if((strlen($' . $attributeName . ') - strrpos($' . $attributeName . ', '.') - 1)  > ' . $value . ' )
                    {throw new \InvalidArgumentException("Argument too much decimal places.");}';
            }
            if ($restriction == 'totalDigits') {
                $code .= 'if((strlen($' . $attributeName . ') - strrpos($' . $attributeName . ', '.') - 1)  != ' . $value . ' )
                    {throw new \InvalidArgumentException("Argument has not correct number decimal places.");}';
            }
            if ($restriction == 'pattern') {
                $pattern = str_replace('/', '\/', $value);
                $code .= 'if( !preg_match("/^' . $pattern . '$/ix",$' . $attributeName . ')  )
                    {throw new \InvalidArgumentException("Argument has invalid format.");}';
            }
            if ($restriction == 'whiteSpace') {
                if($value == 'replace'){
                    $code .= '$' . $attributeName . ' = preg_replace("/\s/"," ",$' . $attributeName . ');';
                }
                if($value == 'collapse'){
                    $code .= '$' . $attributeName . ' = preg_replace("/\s+/"," ",trim($' . $attributeName . '));';
                }
            }
        }

        return $code;
    }

    /**
     * @param Enumeration $enumeration
     * @param $attributeName
     * @return string
     * @throws \RuntimeException
     */
    public function getEnumerationTypeCheckFor(Enumeration $enumeration, $attributeName)
    {
        $code = '';
        $valueCount = $enumeration->countChildren();
        $conditionValues = [];

        for ($i = 0; $i < $valueCount; $i++) {
            $valueAst = $enumeration->get($i);
            $conditionValues[] = $valueAst->getName();
        }

        $ifConditions = "(\$" . $attributeName . " != '" . implode("') || (\$" . $attributeName . " != '", $conditionValues) . "')";
        $listOfValues = '"' . implode('", "', $conditionValues) . '"';

        $code = 'if (' . $ifConditions . ') {'
            . 'throw new \InvalidArgumentException(\'Unexpected value "\' . $' . $attributeName . ' . \'". Expected is one of the following: ' . $listOfValues . '.\');'
            . '}';

        return $code;
    }
}
