<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

/**
 * A TypeUsage-object is used as a reference counter for defined types
 * (complex and simple) in a schema.
 *
 * For example: a defined type, which is never referenced or used, can be
 * removed completely without informaion loss.
*/

namespace PXDB\CodeGen;

use PXDB\Util\XsdType;


class TypeUsage
{
    /**
     * @var array A map with usage informations: type => usage count
     */
    private $typeUsage;

    public function __construct()
    {
        $this->typeUsage = [];
    }

    public function addType($type)
    {
        if (XsdType::isBaseType($type)) {
            // there is no interest in base-type usages
            return;
        }

        if (trim($type) === '') {
            // and no interest in "empty" types as well
            return;
        }

        if (!isset($this->typeUsage[$type])) {
            $this->typeUsage[$type] = 0;
        }

        $this->typeUsage[$type]++;
    }

    /**
     *
     * @return array Hash with type => count
     */
    public function getTypeUsages()
    {
        return $this->typeUsage;
    }
}
