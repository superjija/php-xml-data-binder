<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

/**
 * Encapsulated code generating exception.
*/

namespace PXDB\CodeGen;


class CodeGenException extends \Exception
{
    public function __construct($message, $code, $previous)
    {
        parent::__construct($message, $code, $previous);
    }
}
