<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

namespace PXDB\CodeGen\Code;


class Method
{
    /** @var string $scope */
    private $scope;
    /** @var string $name */
    private $name;
    /** @var array $parameters */
    private $parameters;
    /** @var string $body */
    private $body;

    /**
     * Method constructor.
     * @param string $scope
     * @param string $name
     */
    public function __construct(string $scope, string $name)
    {
        $this->scope = $scope;
        $this->name = $name;
    }


    /**
     * @return string
     */
    public function getScope(): string
    {
        return $this->scope;
    }

    /**
     * @param string $scope
     * @return Method
     */
    public function setScope(string $scope): Method
    {
        $this->scope = $scope;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Method
     */
    public function setName(string $name): Method
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * @param array $parameters
     * @return Method
     */
    public function setParameters(array $parameters): Method
    {
        $this->parameters = $parameters;
        return $this;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @param string $body
     * @return Method
     */
    public function setBody(string $body): Method
    {
        $this->body = $body;
        return $this;
    }
}