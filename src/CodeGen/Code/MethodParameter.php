<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

namespace PXDB\CodeGen\Code;


use PXDB\Binding\Names;
use PXDB\CodeGen;
use PXDB\Util\XsdType;


class MethodParameter
{
    /** @var string $name */
    private $name;
    /** @var string $type */
    private $type;
    /** @var boolean $optinal */
    private $optional;

    /**
     * MethodParameter constructor.
     * @param string $name
     * @param string $type
     * @param bool $optional
     */
    public function __construct(string $name, string $type, bool $optional = FALSE)
    {
        $this->name = $name;
        $this->type = $type;
        $this->optional = $optional;
    }


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return MethodParameter
     */
    public function setName(string $name): MethodParameter
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return MethodParameter
     */
    public function setType(string $type): MethodParameter
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return bool
     */
    public function isOptional(): bool
    {
        return $this->optional;
    }

    /**
     * @param bool $optional
     * @return MethodParameter
     */
    public function setOptional(bool $optional): MethodParameter
    {
        $this->optional = $optional;
        return $this;
    }

    /**
     * @param MethodParameter $parameter
     * @return string
     * @throws \ReflectionException
     * @throws \RuntimeException
     */
    public static function getParameterType(MethodParameter $parameter)
    {
        $parameterType = $parameter->getType();
        if ($parameterType == 'array' || $parameterType == '#array#') {
            return 'array';
        }
        elseif (!XsdType::isBaseType($parameterType)) {
            return Names::createClassnameFor($parameterType);
        }
        elseif (in_array($parameterType, ['string', 'integer', 'int', 'float', 'boolean'])) {
            return $parameterType;
        }
        switch ($parameterType) {
            case 'date':
            case 'dateTime':
                return '\DateTime';
                break;
            case 'decimal':
            case 'double':
                return 'float';
                break;
            case 'long':
                return 'int';
                break;
            default:
                return null;
                break;
        }
    }
}