<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

/**
 * When generating class-code, it is possible to add extended type checks
 * into the methods.
 * This ensures only valid values can be passed to the methods.
 *
 * TypeCheckGenerator can be used for XSD base-types as well as generated classes.
*/

namespace PXDB\CodeGen;

use PXDB\AST\Collection;
use PXDB\AST\CollectionItem;
use PXDB\AST\Enumeration;
use PXDB\AST\Tree;
use PXDB\Binding\Names;
use PXDB\Util\XsdType;


class TypeCheckGenerator
{
    /**
     * Returns the PHP code for a type check.
     *
     * E.g. a XSD-date type has to be formatted like "yyyy-mm-dd". If a value
     * in a different format will be passed to a method, an InvalidArgumentException
     * will be thrown.
     *
     * When no applicable check could be found, an empty string is returned.
     *
     * @param string $type The type, the check should be generated for.
     * @param string $attributeName Name of the attribute-variable
     * @return string The PHP code used for the check.
     */
    public function getTypeCheckFor($type, $attributeName)
    {
        if ($type == 'date') {
            return $this->getDateCheck($attributeName);
        }
        elseif ($type == 'string') {
            return $this->getStringCheck($attributeName);
        }
        elseif ($type == 'int' || $type == 'integer') {
            return $this->getIntCheck($attributeName);
        }
        elseif ($type == 'long') {
            return $this->getLongCheck($attributeName);
        }

        return '';
    }

    /**
     * Returns the PHP code for a collection of arbitrary types.
     *
     * @param Tree $ast
     * @param $attributeName
     * @return string
     * @throws \ReflectionException
     * @throws \RuntimeException
     */
    public function getListTypeCheckFor(Tree $ast, $attributeName)
    {
        if (!($ast instanceof Collection) && !($ast instanceof CollectionItem)) {
            throw new \RuntimeException('Not valid list AST given.');
        }

        $iterationVar = strtolower(substr($attributeName, 0, 1));
        $expectedType = $ast->getType();

        $code = "foreach (\$" . $attributeName . " as &\$" . $iterationVar . ") {";

        if (XsdType::isBaseType($expectedType)) {
            $code .= "if (!is_" . $expectedType . "(\$" . $iterationVar . ")) {";
        }
        else {
            $expectedType = Names::createClassnameFor($expectedType);
            $code .= "if (!\$" . $iterationVar . " instanceof " . $expectedType . ") {";
        }

        $code .= "throw new \InvalidArgumentException('Invalid list. "
            . "All containing elements have to be of type \"" . $expectedType . "\".');"
            . "}"
            . "}";

        return $code;
    }

    /**
     * Returns the PHP code for a "date" check.
     *
     * @param string $attributeName
     * @return string
     */
    private function getDateCheck($attributeName)
    {
        $code = 'if (!preg_match(\'/\d{4}-\d{2}-\d{2}/ism\')) {'
            . 'throw new \InvalidArgumentException(\'Unexpected date '
            . 'format:\' . $%1$s . \'. Expected is: yyyy-mm-dd.\');'
            . '}';

        return sprintf($code, $attributeName);
    }

    /**
     * Returns the PHP code for a "string" check.
     *
     * @param string $attributeName
     * @return string
     */
    private function getStringCheck($attributeName)
    {
        return $this->getSimpleTypeCheck('string', $attributeName);
    }

    /**
     * Returns the PHP code for a base type check.
     * Base types are the standard PHP ones like "string", "long", "int" and so on.
     *
     * @param string $type Type description (e.g. "string", "long", ...)
     * @param <type> $attributeName
     * @return string
     */
    private function getSimpleTypeCheck($type, $attributeName)
    {
        $code = 'if (!is_%1$s($%2$s)) {'
            . 'throw new \InvalidArgumentException(\'"\' . $%2$s . \'" is not a valid %1$s.\');'
            . '}';

        return sprintf($code, $type, $attributeName);
    }

    /**
     * Returns the PHP code for a "int" check.
     *
     * @param string $attributeName
     * @return string
     */
    private function getIntCheck($attributeName)
    {
        return $this->getSimpleTypeCheck('int', $attributeName);
    }

    /**
     * Returns the PHP code for a "long" check.
     *
     * @param string $attributeName
     * @return string
     */
    private function getLongCheck($attributeName)
    {
        return $this->getSimpleTypeCheck('long', $attributeName);
    }

    /**
     * @param Enumeration $enumeration
     * @param $attributeName
     * @return string
     * @throws \RuntimeException
     */
    public function getEnumerationTypeCheckFor(Enumeration $enumeration, $attributeName)
    {
        $code = '';
        $valueCount = $enumeration->countChildren();
        $conditionValues = [];

        for ($i = 0; $i < $valueCount; $i++) {
            $valueAst = $enumeration->get($i);
            $conditionValues[] = $valueAst->getName();
        }

        $ifConditions = "(\$" . $attributeName . " != '" . implode("') && (\$" . $attributeName . " != '", $conditionValues) . "')";
        $listOfValues = '"' . implode('", "', $conditionValues) . '"';

        $code = 'if (' . $ifConditions . ') {'
            . 'throw new \InvalidArgumentException(\'Unexpected value "\' . $' . $attributeName . ' . \'". Expected is one of the following: ' . $listOfValues . '.\');'
            . '}';

        return $code;
    }
}
