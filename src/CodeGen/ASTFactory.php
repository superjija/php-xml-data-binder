<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

namespace PXDB\CodeGen;

use PXDB\AST\Collection;
use PXDB\AST\CollectionItem;
use PXDB\AST\Enumeration;
use PXDB\AST\EnumerationValue;
use PXDB\AST\Structure;
use PXDB\AST\StructureElement;
use PXDB\AST\StructureType;
use PXDB\AST\Type;
use PXDB\AST\TypeAttribute;
use PXDB\ParseTree\AttributeNode;
use PXDB\ParseTree\ChoiceNode;
use PXDB\ParseTree\ComplexContentNode;
use PXDB\ParseTree\ComplexTypeNode;
use PXDB\ParseTree\ElementNode;
use PXDB\ParseTree\EnumerationNode;
use PXDB\ParseTree\ExtensionNode;
use PXDB\ParseTree\RestrictionConditionNode;
use PXDB\ParseTree\RestrictionNode;
use PXDB\ParseTree\RootNode;
use PXDB\ParseTree\SequenceNode;
use PXDB\ParseTree\SimpleTypeNode;
use PXDB\ParseTree\Tree;


/**
 * Factory to create an AST subnode for a given ParseTree-node.
*/
class ASTFactory
{
    /**
     * @param ComplexTypeNode $complexType
     * @return Type
     * @throws \RuntimeException
     */
    public function createFromComplexTypeNode(ComplexTypeNode $complexType)
    {
        if ($complexType->getLevel() == 0) {
            $name = $complexType->getName();
            /** @var RootNode $rootNode */
            $rootNode = $complexType->getParent();
            $baseType = $this->lookupExtensionBase($complexType);

            $newType = new Type($complexType->getName(), '', $baseType);
            $newType->setNamespaces($complexType->getNamespaces());
            $newType->setTargetNamespace($rootNode->getTargetNamespace());

            return $newType;
        }
    }

    /**
     * The extension base is used as a base type for an AST Type-node.
     *
     * ComplexTypes can be extended via complexContent/extension elements.
     * When there is an extension defined, the "base" attribute is used a the
     * Type's base-type.
     *
     * @param ComplexTypeNode $complexType
     * @return string The base-type or empty string
     * @throws \RuntimeException
     */
    private function lookupExtensionBase(ComplexTypeNode $complexType)
    {
        if (!$complexType->hasChildren()) {
            return '';
        }

        $firstChild = $complexType->get(0);
        if ($firstChild instanceof ComplexContentNode) {
            if (!$firstChild->hasChildren()) {
                return '';
            }

            $firstChildOfFirstChild = $firstChild->get(0);

            if ($firstChildOfFirstChild instanceof ExtensionNode) {
                return $firstChildOfFirstChild->getBase();
            }
        }

        return '';
    }

    /**
     * @param Tree $tree
     * @return CollectionItem|Enumeration|EnumerationValue|Structure|StructureElement|Type|TypeAttribute
     * @throws \RuntimeException
     */
    private function inspectNode(Tree $tree)
    {

        if ($tree instanceof ComplexTypeNode) {
            return $this->inspectComplexTypeNode($tree);
        }
        elseif ($tree instanceof ElementNode) {
            return $this->inspectElementNode($tree);
        }
        elseif ($tree instanceof SequenceNode) {
            $firstChild = $tree->get(0);

            return $this->inspectNode($firstChild);
        }
        elseif ($tree instanceof ChoiceNode) {
            $firstChild = $tree->get(0);

            return $this->inspectNode($firstChild);
        }
        elseif ($tree instanceof SimpleTypeNode) {
            $firstChild = $tree->get(0);

            return $this->inspectNode($firstChild);
        }
        elseif ($tree instanceof RestrictionNode) {
            $firstChild = $tree->get(0);

            return $this->inspectNode($firstChild);
        }
        elseif ($tree instanceof RestrictionConditionNode) {
            return $this->inspectRestrictionConditionNode($tree);
        }
        elseif ($tree instanceof EnumerationNode) {
            return $this->inspectEnumerationNode($tree);
        }
        elseif ($tree instanceof ComplexContentNode) {
            $firstChild = $tree->get(0);

            return $this->inspectNode($firstChild);
        }
        elseif ($tree instanceof ExtensionNode) {
            return $this->inspectExtensionNode($tree);
        }

        die('TODO: implement inspectNode() for ' . get_class($tree));
    }

    /**
     * @param SimpleTypeNode $simpleType
     * @return Type
     */
    public function createFromSimpleTypeNode(SimpleTypeNode $simpleType)
    {
        if ($simpleType->getLevel() == 0) {
            $newType = new Type($simpleType->getName());
        }else {
            $newType = new TypeAttribute($simpleType->getParent()->getName());
        }
        foreach ($simpleType->getChildren() as $restriction) {
            if($restriction instanceof RestrictionNode) {
                $newType->setType($restriction->getBase());
                $preserve = FALSE;
                foreach ($restriction->getChildren() as $child) {
                    if ($child instanceof RestrictionConditionNode) {
                        $newType->addRestriction($child->getName(), $child->getValue());
                    }else{
                        $preserve = true;
                    }
                }
                if (!$preserve) {
                    $simpleType->remove($restriction);
                }
            }
        }
        if ($simpleType->getLevel() == 0) {
            $newType->setAsRoot();
            $newType->setNamespaces($simpleType->getNamespaces());
            /** @var RootNode $rootNode */
            $rootNode = $simpleType->getParent();
            $newType->setTargetNamespace($rootNode->getTargetNamespace());

        }
        if ($newType->hasRestrictions() || $simpleType->getLevel() == 0) {
            return $newType;
        }
       /* if ($newType->hasRestrictions() && $simpleType->getParent() instanceof ElementNode) {
            foreach ($newType->getRestrictions() as $name => $value){
                $simpleType->getParent()->add
            }
        }*/
    }

    /**
     * @param ElementNode $element
     * @return Collection|CollectionItem|Enumeration|Structure|StructureElement|Type|TypeAttribute
     * @throws \RuntimeException
     */
    public function createFromElementNode(ElementNode $element)
    {
        $ast = $this->inspectElementNode($element);

        return $ast;
    }

    /**
     * @param EnumerationNode $enumeration
     * @return EnumerationValue
     */
    public function createFromEnumerationNode(EnumerationNode $enumeration)
    {
        $name = $enumeration->getValue();
        $type = $enumeration->getParent()->getBase();

        return new EnumerationValue($name, $type);
    }

    public function createFromRestrictionNode(RestrictionNode $restriction)
    {
        $restrictionParent = $restriction->getParent();

        if ($restrictionParent instanceof SimpleTypeNode && $restriction->hasChildren() && $restriction->get(0) instanceof EnumerationNode) {
            if ($restrictionParent->getLevel() == 0) {
                // when the current restriction's simpleType is a root-level
                // simpleType, an enumeration container is needed for the
                // next-to-be-added EnumerationValues.
                return new Enumeration();
            }

        }else {

            $newType = new Type($restriction->getName());
            $newType->setAsRoot();
            $newType->setNamespaces($restriction->getNamespaces());
            /** @var RootNode $rootNode */
            $rootNode = $restriction->getParent();
            $newType->setTargetNamespace($rootNode->getTargetNamespace());
            return $newType;
        }
    }

    /**
     * @param RestrictionConditionNode $restriction
     * @return Collection|CollectionItem|Enumeration|Structure|StructureElement|Type|TypeAttribute
     */
    public function createFromRestrictionConditionNode(RestrictionConditionNode $restriction)
    {
        if ($restriction->getLevel() == 0) {
            /** @var RootNode $rootNode */
            $rootNode = $restriction->getParent();

            $newType = new Type($restriction->getName());
            $newType->setAsRoot();
            $newType->setNamespaces($restriction->getNamespaces());
            $newType->setTargetNamespace($rootNode->getTargetNamespace());
            return $newType;
        }
    }

    public function createFromChoiceNode(ChoiceNode $choice)
    {
        $choiceParent = $choice->getParent();

        if (($choiceParent instanceof ComplexTypeNode)) {
            if ($choiceParent->getLevel() == 0) {
                // when the current choice's complexType is a root-level
                // complexType, a structure container is needed for the
                // next-to-be-added StructureElement.
                $structure = new Structure();
                $structure->setStructureType(StructureType::CHOICE());

                return $structure;
            }
        }
    }

    public function createFromAttributeNode(AttributeNode $attribute)
    {
        $name = $attribute->getName();
        $type = $attribute->getType();

        if ($attribute->getLevel() == 0) {
            /** @var RootNode $rootNode */
            $rootNode = $attribute->getParent();

            $newType = new Type($name, $type);
            $newType->setNamespaces($attribute->getNamespaces());
            $newType->setTargetNamespace($rootNode->getTargetNamespace());
            $newType->setValueStyle('attribute');

            return $newType;
        }

        $newAttribute = new TypeAttribute($name, $type, !$attribute->isRequired());
        $newAttribute->setStyle('attribute');

        foreach ($attribute->getChildren() as $simpleType) {
            if ($simpleType instanceof SimpleTypeNode) {
                $delete = FALSE;
                foreach ($simpleType->getChildren() as $restriction) {
                    if ($restriction instanceof RestrictionNode) {
                        $newAttribute->setType($restriction->getBase());
                        foreach ($restriction->getChildren() as $child) {
                            if ($child instanceof RestrictionConditionNode) {
                                $newAttribute->addRestriction($child->getName(), $child->getValue());
                            }
                        }
                        $simpleType->remove($restriction);
                        $delete = TRUE;
                    }
                }
                if ($delete) {
                    $attribute->remove($simpleType);
                }
            }
        }
        return $newAttribute;
    }

    private function inspectExtensionNode(ExtensionNode $extension)
    {
        return new Structure('', $extension->getBase());
    }

    /**
     * @param ElementNode $element
     * @return Collection|CollectionItem|Enumeration|Structure|StructureElement|Type|TypeAttribute
     * @throws \RuntimeException
     */
    private function inspectElementNode(ElementNode $element)
    {
        if ($element->hasChildren()) {
            return $this->inspectElementNodeWithChildren($element);
        }
        else {
            return $this->inspectElementNodeWithoutChildren($element);
        }
    }

    /**
     * @param ElementNode $element
     * @return Collection|Enumeration|Structure|Type|TypeAttribute
     * @throws \RuntimeException
     */
    private function inspectElementNodeWithChildren(ElementNode $element)
    {
        $name = $element->getName();
        $type = $element->getType();
        $isOptional = $element->isOptional();

        if ($element->getLevel() == 0) {
            $newType = new Type($name, $type);
            $newType->setAsRoot();
            $newType->setNamespaces($element->getNamespaces());
            /** @var RootNode $rootNode */
            $rootNode = $element->getParent();
            $newType->setTargetNamespace($rootNode->getTargetNamespace());

            return $newType;
        }

        // to know what kind of AST the current element-node will be,
        // further information about its children is needed.
        $firstChild = $element->get(0);

        $deepestNode = $this->inspectNode($firstChild);

        if ($deepestNode instanceof CollectionItem) {
            return new Collection($name, $type);
        }
        elseif ($deepestNode instanceof StructureElement) {
            $newStructure = new Structure($name, $type);
            $structureType = $this->lookupStructureType($element);

            if ($structureType === StructureType::CHOICE()) {
                $newStructure->setStructureType(StructureType::CHOICE());
            }

            return $newStructure;
        }
        elseif ($deepestNode instanceof EnumerationValue) {
            $newEnumeration = new Enumeration($name, $type);

            return $newEnumeration;
        }
        elseif ($deepestNode instanceof Structure) {
            // when the deepest node returns a structure, an adaption of the
            // current type needs to be done (e.g. extension node in an
            // element node).
            $structureType = $deepestNode->getType();

            if ($structureType !== '') {
                return new Structure($name, $deepestNode->getType());
            }
        }elseif($firstChild instanceof SimpleTypeNode){
            return null;
        }

        return new TypeAttribute($name, $type, $isOptional);
    }

    /**
     * @param ElementNode $element
     * @return mixed
     * @throws \RuntimeException
     */
    private function lookupStructureType(ElementNode $element)
    {
        if (!$element->hasChildren()) {
            return StructureType::STANDARD();
        }

        $firstChild = $element->get(0);

        if ($firstChild instanceof ComplexTypeNode) {
            if (!$firstChild->hasChildren()) {
                return StructureType::STANDARD();
            }

            $firstChildOfFirstChild = $firstChild->get(0);

            if ($firstChildOfFirstChild instanceof ChoiceNode) {
                return StructureType::CHOICE();
            }
        }

        return StructureType::STANDARD();
    }

    private function inspectElementNodeWithoutChildren(ElementNode $element)
    {
        $parent = $element->getParent();
        $name = $element->getName();
        $type = $element->getType();
        $isOptional = $element->isOptional();

        if ($element->getLevel() == 0) {
            $newType = new Type($name, $type);
            $newType->setAsRoot();
            $newType->setNamespaces($element->getNamespaces());
            /** @var RootNode $rootNode */
            $rootNode = $element->getParent();
            $newType->setTargetNamespace($rootNode->getTargetNamespace());

            return $newType;
        }

        if ($element->getMaxOccurs() == 'unbounded') {
            return new CollectionItem($name, $type);
        }

        if ($this->elementIsPartOfGlobalType($element)) {
            return new TypeAttribute($name, $type, $isOptional);
        }
        else {
            return new StructureElement($name, $type);
        }
    }

    private function inspectRestrictionConditionNode(RestrictionConditionNode $element)
    {
        return null;
    }

    /**
     * Checks if the give ElementNode is part of a global element/type or if it's
     * used in a sub-structure (e.g. local element complexType, or something similar).
     *
     * @param ElementNode $element
     * @return boolean Whether the element is part of a global type or not
     */
    private function elementIsPartOfGlobalType(ElementNode $element)
    {
        $parent = $element->getParent();
        $isPartOfGlobalType = FALSE;

        while ($parent !== NULL) {
            if ($parent instanceof ComplexTypeNode
                || $parent instanceof SimpleTypeNode
                || $parent instanceof ElementNode
            ) {
                if ($parent->getLevel() == 0) {
                    $isPartOfGlobalType = TRUE;
                    break;
                }
                else {
                    $grandparent = $parent->getParent();

                    if ($grandparent->getLevel() == 0) {
                        $isPartOfGlobalType = TRUE;
                        break;
                    }
                    else {
                        $isPartOfGlobalType = FALSE;
                        break;
                    }
                }
            }
            elseif ($parent instanceof ChoiceNode) {
                $isPartOfGlobalType = FALSE;
                break;
            }

            $parent = $parent->getParent();
        }

        return $isPartOfGlobalType;
    }

    /**
     * @param ComplexTypeNode $complexType
     * @return CollectionItem|Enumeration|EnumerationValue|Structure|StructureElement|Type|TypeAttribute
     * @throws \RuntimeException
     */
    private function inspectComplexTypeNode(ComplexTypeNode $complexType)
    {
        if (!$complexType->hasChildren()) {
            throw new \RuntimeException('Invalid complexType definition');
        }

        if ($complexType->getLevel() == 0) {
            $newType = new Type($complexType->getName());
            $newType->setAsRoot();
            $newType->setNamespaces($complexType->getNamespaces());

            return $newType;
        }

        $firstChild = $complexType->get(0);

        return $this->inspectNode($firstChild);
    }

    private function inspectEnumerationNode(EnumerationNode $enumeration)
    {
        return new EnumerationValue($enumeration->getValue(), $enumeration->getValue());
    }
}
