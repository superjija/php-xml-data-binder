<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

namespace PXDB\Util;


use DateTimeZone;


/**
 * An Enum of all XSD types.
*/
class DateTime extends \DateTime
{

    /**
     * Date constructor.
     * @param string $date
     * @param DateTimeZone|null $timezone
     */
    public function __construct($date = "now", DateTimeZone $timezone = NULL)
    {
        if ($date instanceof \DateTime) {
            parent::__construct($date->format('Y-m-d H:i:s'), $timezone);
        }
        else {
            parent::__construct($date, $timezone);
        }
    }

    public function __toString()
    {
        return $this->format('Y-m-d\TH:i:s');
    }
}
