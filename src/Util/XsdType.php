<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

namespace PXDB\Util;


/**
 * An Enum of all XSD types.
*/
class XsdType
{

    // primitive datatypes
    // according to http://www.w3.org/TR/xmlschema-2/
    private static $string;
    private static $boolean;
    private static $decimal;
    private static $float;
    private static $double;
    private static $duration;
    private static $dateTime;
    private static $time;
    private static $date;
    private static $gYearMonth;
    private static $gYear;
    private static $gMonthDay;
    private static $gDay;
    private static $gMonth;
    private static $hexBinary;
    private static $base64Binary;
    private static $anyURI;
    private static $QName;
    private static $NOTATION;

    // derived datatypes
    private static $normalizedString;
    private static $token;
    private static $language;
    private static $NMTOKEN;
    private static $NMTOKENS;
    private static $Name;
    private static $NCName;
    private static $ID;
    private static $IDREF;
    private static $IDREFS;
    private static $ENTITY;
    private static $ENTITIES;
    private static $integer;
    private static $nonPositiveInteger;
    private static $negativeInteger;
    private static $long;
    private static $int;
    private static $short;
    private static $byte;
    private static $nonNegativeInteger;
    private static $unsignedLong;
    private static $unsignedInt;
    private static $unsignedShort;
    private static $unsignedByte;
    private static $positiveInteger;

    private $value;
    private static $alreadyInitialized = FALSE;

    private function __construct($value)
    {

        $this->value = $value;
    }

    public static function init()
    {

        if (!self::$alreadyInitialized) {
            self::$string = new XsdType('string');
            self::$boolean = new XsdType('boolean');
            self::$decimal = new XsdType('decimal');
            self::$float = new XsdType('float');
            self::$double = new XsdType('double');
            self::$duration = new XsdType('duration');
            self::$dateTime = new XsdType('dateTime');
            self::$time = new XsdType('time');
            self::$date = new XsdType('date');
            self::$gYearMonth = new XsdType('gYearMonth');
            self::$gYear = new XsdType('gYear');
            self::$gMonthDay = new XsdType('gMonthDay');
            self::$gDay = new XsdType('gDay');
            self::$gMonth = new XsdType('gMonth');
            self::$hexBinary = new XsdType('hexBinary');
            self::$base64Binary = new XsdType('base64Binary');
            self::$anyURI = new XsdType('anyURI');
            self::$QName = new XsdType('QName');
            self::$NOTATION = new XsdType('NOTATION');

            self::$normalizedString = new XsdType('normalizedString');
            self::$token = new XsdType('token');
            self::$language = new XsdType('language');
            self::$NMTOKEN = new XsdType('NMTOKEN');
            self::$NMTOKENS = new XsdType('NMTOKENS');
            self::$Name = new XsdType('Name');
            self::$NCName = new XsdType('NCName');
            self::$ID = new XsdType('ID');
            self::$IDREF = new XsdType('IDREF');
            self::$IDREFS = new XsdType('IDREFS');
            self::$ENTITY = new XsdType('ENTITY');
            self::$ENTITIES = new XsdType('ENTITIES');
            self::$integer = new XsdType('integer');
            self::$nonPositiveInteger = new XsdType('nonPositiveInteger');
            self::$negativeInteger = new XsdType('negativeInteger');
            self::$long = new XsdType('long');
            self::$int = new XsdType('int');
            self::$short = new XsdType('short');
            self::$byte = new XsdType('byte');
            self::$nonNegativeInteger = new XsdType('nonNegativeInteger');
            self::$unsignedLong = new XsdType('unsignedLong');
            self::$unsignedInt = new XsdType('unsignedInt');
            self::$unsignedShort = new XsdType('unsignedShort');
            self::$unsignedByte = new XsdType('unsignedByte');
            self::$positiveInteger = new XsdType('positiveInteger');

            self::$alreadyInitialized = TRUE;
        }
    }

    /**
     * @param $type
     * @return bool
     * @throws \ReflectionException
     */
    public static function isBaseType($type)
    {
        $r = new \ReflectionClass(XsdType::class);
        $props = $r->getProperties(\ReflectionProperty::IS_STATIC);

        foreach ($props as &$prop) {
            $name = $prop->getName();

            if ($name == 'alreadyInitialized') {
                continue;
            }

            if (self::$$name == $type) {
                return TRUE;
            }
        }

        return FALSE;
    }

    public function __toString()
    {
        return $this->value;
    }

    public static function STRING()
    {
        return (string)self::$string;
    }

    public static function BOOLEAN()
    {
        return (string)self::$boolean;
    }

    public static function DECIMAL()
    {
        return (string)self::$decimal;
    }

    public static function FLOAT()
    {
        return (string)self::$float;
    }

    public static function DOUBLE()
    {
        return (string)self::$double;
    }

    public static function DURATION()
    {
        return (string)self::$duration;
    }

    public static function DATETIME()
    {
        return (string)self::$dateTime;
    }

    public static function TIME()
    {
        return (string)self::$time;
    }

    public static function DATE()
    {
        return (string)self::$date;
    }

    public static function GYEARMONTH()
    {
        return (string)self::$gYearMonth;
    }

    public static function GYEAR()
    {
        return (string)self::$gYear;
    }

    public static function GMONTHDAY()
    {
        return (string)self::$gMonthDay;
    }

    public static function GDAY()
    {
        return (string)self::$gDay;
    }

    public static function GMONTH()
    {
        return (string)self::$gMonth;
    }

    public static function HEXBINARY()
    {
        return (string)self::$hexBinary;
    }

    public static function BASE64BINARY()
    {
        return (string)self::$base64Binary;
    }

    public static function ANYURI()
    {
        return (string)self::$anyURI;
    }

    public static function QNAME()
    {
        return (string)self::$QName;
    }

    public static function NOTATION()
    {
        return (string)self::$NOTATION;
    }

    public static function NORMALIZEDSTRING()
    {
        return (string)self::$normalizedString;
    }

    public static function TOKEN()
    {
        return (string)self::$token;
    }

    public static function LANGUAGE()
    {
        return (string)self::$language;
    }

    public static function NMTOKEN()
    {
        return (string)self::$NMTOKEN;
    }

    public static function NMTOKENS()
    {
        return (string)self::$NMTOKENS;
    }

    public static function NAME()
    {
        return (string)self::$Name;
    }

    public static function NCNAME()
    {
        return (string)self::$NCName;
    }

    public static function ID()
    {
        return (string)self::$ID;
    }

    public static function IDREF()
    {
        return (string)self::$IDREF;
    }

    public static function IDREFS()
    {
        return (string)self::$IDREFS;
    }

    public static function ENTITY()
    {
        return (string)self::$ENTITY;
    }

    public static function ENTITIES()
    {
        return (string)self::$ENTITIES;
    }

    public static function INTEGER()
    {
        return (string)self::$integer;
    }

    public static function NONPOSITIVEINTEGER()
    {
        return (string)self::$nonPositiveInteger;
    }

    public static function NEGATIVEINTEGER()
    {
        return (string)self::$negativeInteger;
    }

    public static function LONG()
    {
        return (string)self::$long;
    }

    public static function INT()
    {
        return (string)self::$int;
    }

    public static function SHORT()
    {
        return (string)self::$short;
    }

    public static function BYTE()
    {
        return (string)self::$byte;
    }

    public static function NONNEGATIVEINTEGER()
    {
        return (string)self::$nonNegativeInteger;
    }

    public static function UNSIGNEDLONG()
    {
        return (string)self::$unsignedLong;
    }

    public static function UNSIGNEDINT()
    {
        return (string)self::$unsignedInt;
    }

    public static function UNSIGNEDSHORT()
    {
        return (string)self::$unsignedShort;
    }

    public static function UNSIGNEDBYTE()
    {
        return (string)self::$unsignedByte;
    }

    public static function POSITIVEINTEGER()
    {
        return (string)self::$positiveInteger;
    }

}


// simulate static initializer
XsdType::init();