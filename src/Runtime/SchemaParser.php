<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */


/**
 * The SchemaParser parses a given XML-Schema file.
 * While parsing, it creates the parse-tree.
*/

namespace PXDB\Runtime;

use PXDB\CodeGen\TypeUsage;
use PXDB\ParseTree\ChoiceNode;
use PXDB\ParseTree\ComplexTypeNode;
use PXDB\ParseTree\ElementNode;
use PXDB\ParseTree\EnumerationNode;
use PXDB\ParseTree\RestrictionNode;
use PXDB\ParseTree\RootNode;
use PXDB\ParseTree\SequenceNode;
use PXDB\ParseTree\SimpleTypeNode;
use PXDB\ParseTree\Tree;


class SchemaParser
{
    private static $XSD_NS = "http://www.w3.org/2001/XMLSchema";
    private $schemaNamespace;
    /**
     * @var Tree Internal ParseTree that is being built.
     */
    private $parseTree;

    private $typeUsage;

    /**
     * Creates a new SchemaParser.
     *
     * @param string $schemaFile
     * @throws \Exception
     */
    public function __construct($schemaFile = '', TypeUsage $typeUsage = NULL)
    {
        if ($schemaFile !== '') {
            $this->setSchemaFile($schemaFile);
        }

        if ($typeUsage === NULL) {
            $typeUsage = new TypeUsage();
        }
        $this->typeUsage = $typeUsage;
    }

    /**
     *
     * @param string $schemaFile
     * @throws \Exception
     */
    private function init($schemaFile)
    {
        $this->xml = simplexml_load_file($schemaFile);

        if (!is_object($this->xml)) {
            // @TODO: add simple-xml-error message
            throw new \Exception("Invalid XSD");
        }

        $this->initializeXml();
    }

    private function initializeXml()
    {
        $nsPrefix = array_search(self::$XSD_NS, $this->xml->getDocNamespaces());
        $this->xml->registerXPathNamespace($nsPrefix, self::$XSD_NS);
        $this->schemaNamespace = $nsPrefix;
    }

    /**
     * Starts the parsing process.
     * @return RootNode|
     * Tree
     */
    public function parse()
    {
        $this->parseTree = new RootNode();

        // starts a straight-forward schema parsing
        $this->parseSchemaNodes($this->xml, $this->parseTree);

        return $this->parseTree;
    }

    /**
     * Recursive top-down parsing (depth-first) of an XSD-Schema.
     *
     * @param \SimpleXMLElement $xml The current xml-node to parse
     * @param Tree $part Current ParseTree-node
     * @param int $level Parse-tree level (current depth)
     */
    private function parseSchemaNodes(\SimpleXMLElement $xml, Tree $part, $level = 0)
    {
        $ns = $this->schemaNamespace;

        foreach ($xml->children($ns, TRUE) as $child) {
            $name = (string)$child->getName();

            if ($name == 'element') {
                $newPart = new ElementNode($child, $level);
                $attributes = $child->attributes();

                $type = (string)$attributes['type'];
                $type = $this->getStringWithoutNamespace($type);

                $this->typeUsage->addType($type);
            }
            elseif ($name == 'simpleType') {
                $newPart = new SimpleTypeNode($child, $level);
            }
            elseif ($name == 'complexType') {
                $newPart = new ComplexTypeNode($child, $level);
            }
            elseif ($name == 'sequence') {
                $newPart = new SequenceNode($child, $level);
            }
            elseif ($name == 'choice') {
                $newPart = new ChoiceNode($child, $level);
            }
            elseif ($name == 'restriction') {
                $newPart = new RestrictionNode($child, $level);
            }
            elseif ($name == 'enumeration') {
                $newPart = new EnumerationNode($child, $level);
            }

            $this->parseSchemaNodes($child, $newPart, $level + 1);
            $part->add($newPart);
        }
    }

    /**
     *
     * @param \SimpleXMLElement $xml The Schema-XML (or a fragment of a schema)
     */
    public function setSchema(\SimpleXMLElement $xml)
    {
        $this->xml = $xml;
        $this->initializeXml();
    }

    /**
     * @param $schemaFile
     * @throws \Exception
     */
    public function setSchemaFile($schemaFile)
    {
        if (!file_exists($schemaFile) || !is_readable($schemaFile)) {
            throw new \InvalidArgumentException("Unreadable XSD-Schema file: $schemaFile");
        }

        $this->init($schemaFile);
    }

    /**
     * Helper method to strip off namespaces from a string.
     *
     * @param string $string
     * @return string The namespace-free string
     */
    private function getStringWithoutNamespace($string)
    {
        if (strpos($string, ':')) {
            $parts = explode(':', $string);
            return $parts[1];
        }

        return $string;
    }
}
