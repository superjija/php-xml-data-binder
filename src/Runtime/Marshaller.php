<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */


/**
 * The Marshaller is responsible to serialize a given object-structure into
 * a string representation.
 * This string can also be written directly into an output-file, if it's a
 * root level element/object.
*/

namespace PXDB\Runtime;

use PXDB\AST\TypeAttribute;
use PXDB\AST\Type;
use PXDB\AST\Collection;
use PXDB\AST\CollectionItem;
use PXDB\AST\Tree;
use PXDB\AST\StructureType;
use PXDB\AST\StructureElement;
use PXDB\AST\Structure;


class Marshaller
{
    /**
     * @var Binding
     */
    private $binding;

    /**
     * @var \DomDocument The XML document/fragment that is being generated.
     */
    private $dom;

    /**
     * @var \DomElement The current node that has been, or is to be added, to the DomDocument.
     */
    private $currentDomNode;

    /**
     * @var \DomElement The currents node parent.
     */
    private $parentDomNode;

    public function __construct(Binding $binding)
    {
        $this->binding = $binding;
    }

    /**
     * Converts the given $object into its XML representation.
     *
     * @param object $object
     * @return string
     * @throws \InvalidArgumentException When the given parameter is not an object.
     * @throws MarshalException
     */
    public function marshal($object)
    {
        if (!is_object($object)) {
            throw new \InvalidArgumentException('Cannot marshal a non-object');
        }

        $this->dom = new \DomDocument('1.0');
        $this->dom->preserveWhiteSpace = FALSE;
        $this->dom->formatOutput = TRUE;

        $this->currentDomNode = $this->parentDomNode = $this->dom;
        $this->currentDomNode = $this->dom->documentElement;

        $classToMarshal = get_class($object);
        try {
            $ast = $this->binding->getASTForClass($classToMarshal);
            $xml = $this->marshalObject($object, $ast);
        }
        catch (\RuntimeException $e) {
            throw new MarshalException($e->getMessage(), $e->getCode(), $e);
        }

        return trim($this->dom->saveXML());
    }

    /**
     * Converts the given parameter $object into its XML representation corresponding
     * to its AST.
     *
     * @param object $object The object to marshal
     * @param Tree $ast object's AST
     * @return void
     * @throws \RuntimeException
     */
    private function marshalObject($object, Tree $ast)
    {
        $astName = $ast->getName();

        $lastNode = $this->currentDomNode;

        if ($astName != '') {
            // for every named element, a new node in the resulting xml-tree is created
            $this->currentDomNode = $this->dom->createElement($astName);
            $this->parentDomNode->appendChild($this->currentDomNode);
        }
        else {
            // "anonymous" elements are just chained down. No new node has to be created
            $this->currentDomNode = $this->parentDomNode;
        }

        if ($ast instanceof Type) {
            $this->marshalType($object, $ast);
        }
        elseif ($ast instanceof Collection) {
            $this->marshalCollection($object, $ast);
        }
        elseif ($ast instanceof Structure) {
            $this->marshalStructure($object, $ast);
        }
        elseif ($ast instanceof TypeAttribute) {
            $this->marshalTypeAttribute($object, $ast);
        }
        elseif ($ast instanceof StructureElement) {
            $this->marshalStructureElement($object, $ast);
        }
        elseif (is_string($object) || ($ast instanceof CollectionItem)) {
            $newNode = $this->dom->createTextNode($object);
            $this->currentDomNode->appendChild($newNode);
        }
        else {
            throw new \RuntimeException('Currently not supported: ' . get_class($ast));
        }
    }

    /**
     * @param $object
     * @param Type $ast
     * @throws \RuntimeException
     */
    private function marshalType($object, Type $ast)
    {
        $lastNode = $this->parentDomNode;
        $this->parentDomNode = $this->currentDomNode;
        if ($ast->isRoot()) {
            $targetNamespace = $ast->getTargetNamespace();

            if ($targetNamespace != '') {
                $this->currentDomNode->setAttribute('xmlns', $targetNamespace);
            }
        }
        if ($ast->hasChildren()) {
            $childrenCount = $ast->countChildren();
            for ($i = 0; $i < $childrenCount; $i++) {
                $child = $ast->get($i);
                $this->marshalObject($object, $child);
            }
        }
        $this->parentDomNode = $lastNode;
    }

    /**
     * @param $object
     * @param Collection $ast
     * @throws \RuntimeException
     */
    private function marshalCollection($object, Collection $ast)
    {
        $getter = $ast->getGetMethod();
        $collectionItems = $object->$getter();
        $lastNode = $this->parentDomNode;
        $this->parentDomNode = $this->currentDomNode;

        foreach ($collectionItems as &$item) {
            if ($ast->hasChildren()) {
                $childrenCount = $ast->countChildren();

                for ($i = 0; $i < $childrenCount; $i++) {
                    $child = $ast->get($i);
                    if (is_object($item)) {
                        // TODO is abstract mapping collection?
                        $classToMarshal = get_class($item);
                        $classAst = $this->binding->getASTForClass($classToMarshal);
                        $structureName = $child->getName();
                        $classAst->setName($structureName);

                        $this->marshalObject($item, $classAst);
                    }
                    else {
                        // this collection is just a list of (scalar) values
                        $this->marshalObject($item, $child);
                    }
                }
            }
        }

        $this->parentDomNode = $lastNode;
    }

    /**
     * @param $object
     * @param Structure $ast
     * @throws \RuntimeException
     */
    private function marshalStructure($object, Structure $ast)
    {
        $lastNode = $this->parentDomNode;

        if ($ast->getStructureType() === StructureType::CHOICE()) {
            $this->parentDomNode = $this->currentDomNode;

            if ($ast->hasChildren()) {
                $childrenCount = $ast->countChildren();
                $currentChoice = NULL;

                for ($i = 0; $i < $childrenCount; $i++) {
                    $child = $ast->get($i);

                    $testMethod = $child->getTestMethod();

                    if ($object->$testMethod()) {
                        $currentChoice = $child;
                        break;
                    }
                }

                $this->marshalObject($object, $currentChoice);
            }
        }
        elseif ($ast->getStructureType() === StructureType::ORDERED()) {
            throw new \RuntimeException('Currently not supported.');
        }
        else {
            $this->parentDomNode->removeChild($this->currentDomNode);
            // when a structure has no type, it is a referenced complex-type
            // used as a type-attribute
            $getter = $ast->getGetMethod();
            $obj = $object->$getter();
            $classname = $this->binding->getClassnameForName($ast->getName());
            $structureAst = $this->binding->getASTForClass($classname);

            $this->marshalObject($obj, $structureAst);
        }

        $this->parentDomNode = $lastNode;
    }

    /**
     * @param $object
     * @param TypeAttribute $ast
     * @throws \RuntimeException
     */
    private function marshalTypeAttribute($object, TypeAttribute $ast)
    {
        if ($ast->getStyle() == 'element') {
            $getter = $ast->getGetMethod();
            $value = $object->$getter();

            $newNode = $this->dom->createTextNode($value);
            $this->currentDomNode->appendChild($newNode);
        }
        elseif ($ast->getStyle() == 'attribute') {
            $getter = $ast->getGetMethod();
            $name = $ast->getName();
            $value = $object->$getter();
            if ($value != '') {// TODO check for "optional" attribute in binding.
                $this->parentDomNode->setAttribute($name, $value);
            }
            // in marshalObject() a child is added everytime.
            // no matter what type it is ("attribute" or "element"),
            // so we can just remove it here
            $this->parentDomNode->removeChild($this->currentDomNode);
        }
        else {
            throw new \RuntimeException('Invalid TypeAttribute style "' . $ast->getStyle() . '"');
        }
    }

    private function marshalStructureElement($object, StructureElement $ast)
    {
        $getter = $ast->getGetMethod();
        $value = $object->$getter();

        $newNode = $this->dom->createTextNode($value);
        $this->currentDomNode->appendChild($newNode);

        return $value;
    }
}
