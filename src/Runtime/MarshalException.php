<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

/**
 * Encapsulated marshalling exception.
*/

namespace PXDB\Runtime;


class MarshalException extends \Exception
{
    public function __construct($message, $code, $previous)
    {
        parent::__construct($message, $code, $previous);
    }
}
