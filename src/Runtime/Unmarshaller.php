<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

/**
 * The Marshaller is responsible to serialize a given object-structure into
 * a string representation.
 * This string can also be written directly into an output-file, if it's a
 * root level element/object.
*/

namespace PXDB\Runtime;

use PXDB\AST\Tree;
use PXDB\AST\TypeAttribute;
use PXDB\AST\Collection;
use PXDB\AST\CollectionItem;
use PXDB\AST\StructureType;
use PXDB\AST\Structure;
use PXDB\Util\XsdType;


class Unmarshaller
{
    /**
     * @var Binding
     */
    private $binding;

    public function __construct(Binding $binding)
    {
        $this->binding = $binding;
    }

    /**
     * @param $string
     * @return mixed
     * @throws UnMarshalException
     */
    public function unmarshal($string)
    {
        if (!is_string($string)) {
            throw new \InvalidArgumentException('Cannot unmarshal a non-string');
        }

        $xml = simplexml_load_string($string);

        list($rootNode) = $xml->xpath('/*');

        $attributes = $rootNode->attributes();
        $name = $rootNode->getName();
        try {
            $ast = $this->binding->getASTForName($name);
            $class = $ast->getType();

            $object = new $class();

            try {
                $this->parseXml($rootNode, $ast, $object);
            }
            catch (\ReflectionException $e) {
                throw new UnMarshalException($e->getMessage(), $e->getCode(), $e);
            }
        }
        catch (\RuntimeException $e) {
            throw new UnMarshalException($e->getMessage(), $e->getCode(), $e);
        }

        return $object;
    }

    /**
     * Parses the XML data and returns the object-structure represented by the XML.
     *
     * @param \SimpleXMLElement $xml The current XML node
     * @param Tree $ast The XML corresponding AST subtree
     * @param object $parentObject The object that contains the element described by $xml and $ast
     * @return mixed string or object, dependent on the current $xml and $ast.
     *               If the current $xml is a leaf node <code>parseXml()</code> returns the literal
     *               value of $xml. When it's composite node, it returns the object-structure
     *               reflecting the $xml data.
     * @throws \RuntimeException
     * @throws \ReflectionException
     */
    private function parseXml(\SimpleXMLElement $xml, Tree $ast, $parentObject)
    {
        $count = $ast->countChildren();

        if (!$xml->children()) {
            // a leaf node in the XML documents doesn't need to be parsed any further
            return (string)$xml;
        }

        if ($count == 0) {
            if ($ast instanceof Structure) {
                $newObject = $this->parseStructure($xml, $ast, $parentObject);
            }
            else {
                throw new \RuntimeException('Not supported yet');
            }
        }
        else {

            $newObject = $parentObject;

            for ($i = 0; $i < $count; $i++) {
                $child = $ast->get($i);

                $name = $child->getName();
                $childXml = $xml->$name;

                if ($child instanceof Structure) {
                    $newObject = $this->parseStructure($xml->{$name}, $child, $parentObject);
                }
                elseif ($child instanceof Collection) {
                    $name = $ast->getName();
                    $list = $this->parseCollection($xml, $child, $parentObject);
                    $setter = $child->getSetMethod();
                    $parentObject->$setter($list);
                }
                elseif ($child instanceof TypeAttribute) {
                    $newObject = $this->parseTypeAttribute($xml, $child, $parentObject);
                }
                else {
                    throw new \RuntimeException('Not supported yet');
                }
            }
        }
        return $newObject;
    }

    /**
     *
     * @param \SimpleXMLElement $xml
     * @param Structure $ast
     * @param object $parentObject
     * @return mixed|object
     * @throws \ReflectionException
     * @throws \RuntimeException
     */
    private function parseStructure(\SimpleXMLElement $xml, Structure $ast, $parentObject)
    {
        $name = $ast->getName();
        $type = $ast->getType();

        if ($type == '') {
            if ($ast->getStructureType() === StructureType::CHOICE()) {
                return $this->parseChoiceStructure($xml, $ast, $parentObject);
            }
            elseif ($ast->getStructureType() === StructureType::ORDERED()) {
                throw new \RuntimeException('Not supported yet.');
            }
            else {
                throw new \RuntimeException('Invalid <structure>.');
            }
        }
        else { // a structure with a type is a reference to the type

            if (!XsdType::isBaseType($type)) {

                $structAst = $this->binding->getASTForClass($type);
                $newObject = new $type();

                $parsedObject = $this->parseXml($xml, $structAst, $newObject);
                $setter = $ast->getSetMethod();

                $parentObject->$setter($parsedObject);
                return $parentObject;
            }
            else {
                return $parentObject;
            }
        }

    }

    /**
     * @param \SimpleXMLElement $xml
     * @param Structure $ast
     * @param $parentObject
     * @return mixed
     * @throws \ReflectionException
     * @throws \RuntimeException
     */
    private function parseChoiceStructure(\SimpleXMLElement $xml, Structure $ast, $parentObject)
    {
        $choiceCount = $ast->countChildren();

        for ($i = 0; $i < $choiceCount; $i++) {
            $child = $ast->get($i);
            $name = $child->getName();

            if ($xml->{$name}) {// choice in XML found
                $newObject = $this->parseXml($xml->{$name}, $child, $parentObject);
                $setter = $child->getSetMethod();

                $parentObject->$setter($newObject);

                break;
            }

        }

        return $parentObject;
    }

    private function parseTypeAttribute(\SimpleXMLElement $xml, TypeAttribute $ast, $parentObject)
    {
        $name = $ast->getName();

        if ($ast->getStyle() == 'element') {
            $value = (string)$xml->$name;

        }
        elseif ($ast->getStyle() == 'attribute') {
            $attributes = $xml->attributes();
            $value = (string)$attributes[$name];
        }
        $setter = $ast->getSetMethod();

        $parentObject->$setter($value);

        return $parentObject;
    }

    /**
     *
     * @param \SimpleXMLElement $xml
     * @param Collection $ast
     * @param <type> $parentObject
     * @return mixed[] The collection entries
     * @throws \ReflectionException
     * @throws \RuntimeException
     */
    private function parseCollection(\SimpleXMLElement $xml, Collection $ast, $parentObject)
    {
        if ($ast->getName() == '') {
            return $this->parseAnonymCollection($xml, $ast, $parentObject);
        }
        else {
            return $this->parseNamedCollection($xml, $ast, $parentObject);
        }
    }

    /**
     *
     * @param \SimpleXMLElement $xml
     * @param Collection $ast
     * @param <type> $parentObject
     * @return mixed[] The collection entries
     * @throws \ReflectionException
     * @throws \RuntimeException
     */
    private function parseNamedCollection(\SimpleXMLElement $xml, Collection $ast, $parentObject)
    {
        $count = $ast->countChildren();
        $list = [];
        $collectionName = $ast->getName();

        for ($i = 0; $i < $count; $i++) {
            $child = $ast->get($i);

            if ($child instanceof Structure) {
                $name = $child->getName();
                $structAst = $this->binding->getASTForName($name);
                $class = $structAst->getType();

                // somehow xpath didn't work well with node names containing a dash
                // so fetch the nodes with "{}", e.g. "{'node-containing-a-dash'}"
                $itemCount = count($xml->{$collectionName}->{$name});

                if ($itemCount > 0) {
                    // collection items/nodes are directly in the current node
                    for ($j = 0; $j < $itemCount; $j++) {
                        $newObject = new $class();
                        $listNode = $xml->{$collectionName}->{$name}[$j];

                        $list[] = $this->parseXml($listNode, $structAst, $newObject);
                    }
                }
            }
            elseif ($child instanceof CollectionItem) {
                $name = $child->getName();
                $itemCount = count($xml->{$collectionName}->{$name});

                if ($itemCount > 0) {
                    for ($j = 0; $j < $itemCount; $j++) {
                        $listNode = $xml->{$collectionName}->{$name}[$j];

                        $list[] = $this->parseXml($listNode, $ast, $parentObject);
                    }
                }
            }
            else {
                throw new \RuntimeException('Invalid <collection>');
            }
        }

        return $list;
    }

    /**
     *
     * @param \SimpleXMLElement $xml
     * @param Collection $ast
     * @param <type> $parentObject
     * @return mixed[] The collection entries
     * @throws \ReflectionException
     * @throws \RuntimeException
     */
    private function parseAnonymCollection(\SimpleXMLElement $xml, Collection $ast, $parentObject)
    {
        $count = $ast->countChildren();
        $list = [];

        for ($i = 0; $i < $count; $i++) {
            $child = $ast->get($i);

            if ($child instanceof Structure) {
                $name = $child->getName();
                $structAst = $this->binding->getASTForName($name);
                $class = $structAst->getType();

                // somehow xpath didn't work well with node names containing a dash
                // so fetch the nodes with "{}", e.g. "{'node-containing-a-dash'}"
                $itemCount = count($xml->{$name});

                if ($itemCount > 0) {
                    // collection items/nodes are directly in the current node
                    for ($j = 0; $j < $itemCount; $j++) {
                        $newObject = new $class();
                        $listNode = $xml->{$name}[$j];

                        $list[] = $this->parseXml($listNode, $structAst, $newObject);
                    }
                }
            }
            else {
                throw new \RuntimeException('Invalid <collection>');
            }
        }

        return $list;
    }
}
