<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

/**
 * An enumeration of available structure types.
 *
 * There are three possible structure types: ordered, choice and a default or "none".
 *
 * It's a PHP adaption of the "Typesafe enum pattern".
 * (@see <link>http://java.sun.com/developer/Books/shiftintojava/page1.html#replaceenums</link>)
 * Since PHP doesn't have static initializers, this construct has to be simulated
 * via a static method call.
*/

namespace PXDB\AST;

class StructureType
{
    private static $ordered;
    private static $choice;
    private static $standard;

    private $value;
    private static $alreadyInitialized = FALSE;

    private function __construct($value)
    {
        $this->value = $value;
    }

    public static function init()
    {

        if (!self::$alreadyInitialized) {
            self::$ordered = new StructureType('ordered');
            self::$choice = new StructureType('choice');
            self::$standard = new StructureType('');

            self::$alreadyInitialized = TRUE;
        }
    }

    public static function ORDERED()
    {
        return self::$ordered;
    }

    public static function CHOICE()
    {
        return self::$choice;
    }

    public static function STANDARD()
    {
        return self::$standard;
    }
}


// simulate static initializers
StructureType::init();