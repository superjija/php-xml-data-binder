<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

/**
 * Abstract syntax tree visitor.
 *
 * It is a Hierarchical Visitor, hence the <code>visitXXXEnter()</code> and
 * <code>visitXXXLeave()</code> methods.
 * Since not every AST-Node is a composite (but a leaf), not every node-type
 * needs to have a corresponding <code>...Enter()</code> and
 * <code>...Leave()</code> method.
 *
 */

namespace PXDB\AST\Visitor;


use PXDB\AST\Collection;
use PXDB\AST\CollectionItem;
use PXDB\AST\Enumeration;
use PXDB\AST\EnumerationValue;
use PXDB\AST\Structure;
use PXDB\AST\StructureElement;
use PXDB\AST\Type;
use PXDB\AST\TypeAttribute;


interface VisitorAbstract
{
    public function visitCollectionEnter(Collection $collection);

    public function visitCollectionLeave(Collection $collection);

    public function visitCollectionItem(CollectionItem $collectionItem);

    public function visitEnumerationEnter(Enumeration $enumeration);

    public function visitEnumerationLeave(Enumeration $enumeration);

    public function visitEnumerationValue(EnumerationValue $enumerationValue);

    public function visitStructureEnter(Structure $structure);

    public function visitStructureLeave(Structure $structure);

    public function visitStructureElementEnter(StructureElement $structureElement);

    public function visitStructureElementLeave(StructureElement $structureElement);

    public function visitTypeEnter(Type $type);

    public function visitTypeLeave(Type $type);

    public function visitTypeAttributeEnter(TypeAttribute $typeAttribute);

    public function visitTypeAttributeLeave(TypeAttribute $typeAttribute);
}
