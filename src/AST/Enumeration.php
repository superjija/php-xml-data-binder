<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

/**
 * An enumeration of values.
 * Every value will be an object of class EnumerationValue.
 *
 */

namespace PXDB\AST;

use PXDB\AST\Visitor\VisitorAbstract;


class Enumeration extends Tree
{
    public function accept(VisitorAbstract $v)
    {

        if ($v->visitEnumerationEnter($this)) {
            foreach ($this->children as $child) {
                if ($child->accept($v) === FALSE) {
                    break;
                }
            }
        }

        return $v->visitEnumerationLeave($this);
    }
}
