<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

/**
 * A scalar class attribute/member. It's not an XML-Attribute, it's a class-member.
*/

namespace PXDB\AST;

use PXDB\AST\Visitor\VisitorAbstract;


class TypeAttribute extends Tree
{
    private $style;
    private $isOptional;
    private $getMethod;
    private $setMethod;
    private $restrictions =[];

    public function __construct($name = '', $type = '', $isOptional = FALSE)
    {
        parent::__construct($name, $type);
        $this->style = 'element';
        $this->isOptional = $isOptional;
    }

    public function setStyle($style)
    {
        if ($style == 'attribute') {
            //$this->isOptional = TRUE;
        }

        $this->style = $style;
    }

    public function getStyle()
    {
        return $this->style;
    }

    public function setGetMethod($methodName)
    {
        $this->getMethod = $methodName;
    }

    public function getGetMethod()
    {
        return $this->getMethod;
    }

    public function setSetMethod($methodName)
    {
        $this->setMethod = $methodName;
    }

    public function getSetMethod()
    {
        return $this->setMethod;
    }

    public function isOptional()
    {
        return $this->isOptional;
    }

    public function addRestriction(string $type,string $value)
    {
        $this->restrictions[$type] = $value;
    }

    /**
     * @return array
     */
    public function getRestrictions(): array
    {
        return $this->restrictions;
    }

    public function hasRestrictions()
    {
        return count($this->restrictions) > 0;
    }

    public function accept(VisitorAbstract $v)
    {
        if ($v->visitTypeAttributeEnter($this)) {
            foreach ($this->children as $child) {
                if ($child->accept($v) === FALSE) {
                    break;
                }
            }
        }

        return $v->visitTypeAttributeLeave($this);

    }

    public function jsonSerialize()
    {
        return parent::jsonSerialize()+[
                'restrictions' => $this->restrictions
            ];
    }
}
