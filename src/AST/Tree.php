<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

/**
 * Base, or root, type of the abstract-syntax-tree.
*/

namespace PXDB\AST;

use PXDB\AST\Visitor\VisitorAbstract;


abstract class Tree implements \JsonSerializable
{
    /**
     * @var string Name of the current node
     */
    protected $name;
    /**
     * @var Tree[] Child-nodes (composite)
     */
    protected $children;
    /**
     * @var Tree The nodes parent-node
     */
    protected $parent;
    /**
     * @var string The type of the current node (e.g. "string", "long" for XSD base types or names of complexTypes in a schema).
     */
    protected $type;

    public function __construct($name = '', $type = '')
    {
        $this->name = $name;
        $this->children = [];
        $this->type = $type;
    }

    public function setParent(Tree $tree)
    {
        $this->parent = $tree;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function add(Tree $tree)
    {
        $this->children[] = $tree;
        $tree->setParent($this);

        return $this;
    }

    /**
     * @param $index
     * @return mixed|Tree
     * @throws \RuntimeException
     */
    public function get($index)
    {
        if (!isset($this->children[$index])) {
            throw new \RuntimeException('Invalid child index "' . $index . '"');
        }
        return $this->children[$index];
    }

    public function remove(Tree $tree)
    {
        $index = array_search($tree, $this->children, TRUE);

        if ($index === FALSE) {
            return FALSE;
        }

        array_splice($this->children, $index, 1);

        return TRUE;
    }

    public function hasChildren()
    {
        return count($this->children) > 0;
    }

    public function countChildren()
    {
        return count($this->children);
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getType()
    {
        return $this->type;
    }

    abstract function accept(VisitorAbstract $v);

    public function jsonSerialize()
    {
        return [
            'class' => get_class($this),
            'name' => $this->name,
            'type' => $this->type,
            'children' => $this->children,
        ];
    }


}
