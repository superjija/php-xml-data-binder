<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

/**
 * A concrete or abstract type.
 *
 * Concrete type means, it can occur as a root element in the corresponding
 * XML file. Abstract is used for complexTypes that are used for structure
 * references.
*/

namespace PXDB\AST;

use PXDB\AST\Visitor\VisitorAbstract;


class Type extends Tree
{
    /**
     * @var int How many attributes/members the current Type has
     */
    private $attributeCount;
    /**
     * @var boolean Wether the current Type is the root type of the XSD or not.
     */
    private $isRootType;

    /**
     * @var string
     */
    private $targetNamespace;
    /**
     * @var array Associative array of used namespaces: prefix => URI
     */
    private $namespaces;

    private $valueStyle;

    private $baseType;
    private $restrictions =[];

    public function __construct($name = '', $type = '', $baseType = '')
    {
        parent::__construct($name, $type);

        $this->isRootType = FALSE;
        $this->valueStyle = 'element';
        $this->baseType = $baseType;
    }

    public function setAsRoot()
    {
        $this->isRootType = TRUE;
    }

    public function isRoot()
    {
        return $this->isRootType;
    }

    public function setTargetNamespace($targetNamespace)
    {
        $this->targetNamespace = $targetNamespace;
    }

    public function getTargetNamespace()
    {
        return $this->targetNamespace;
    }

    public function setNamespaces(array $namespaces = NULL)
    {
        $this->namespaces = $namespaces;
    }

    public function getNamespaces()
    {
        return $this->namespaces;
    }

    public function setValueStyle($valueStyle)
    {
        $this->valueStyle = $valueStyle;
    }

    public function getValueStyle()
    {
        return $this->valueStyle;
    }

    /**
     * @return bool
     * @throws \RuntimeException
     */
    public function isEnumerationType()
    {
        if (!$this->hasChildren()) {
            return FALSE;
        }

        $firstEnumeration = $this->get(0);
        if ($firstEnumeration instanceof Enumeration) {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * @return bool
     * @throws \RuntimeException
     */
    public function isStandardType()
    {
        if (!$this->hasChildren()) {
            return TRUE;
        }

        $firstAttribute = $this->get(0);
        if ($firstAttribute instanceof TypeAttribute || $firstAttribute instanceof Structure || $firstAttribute instanceof Collection || $firstAttribute instanceof CollectionItem) {
            return TRUE;
        }

        return FALSE;
    }

    public function hasBaseType()
    {
        return $this->baseType !== '';
    }

    public function getBaseType()
    {
        return $this->baseType;
    }

    public function setBaseType($baseType)
    {
        $this->baseType = $baseType;
    }

    public function addRestriction(string $type,string $value)
    {
        if(!isset($this->restrictions[$type])) {
            $this->restrictions[$type] = [];
        }
        $this->restrictions[$type][] = $value;
    }

    public function accept(VisitorAbstract $v)
    {
        if ($v->visitTypeEnter($this)) {
            foreach ($this->children as $child) {
                if ($child->accept($v) === FALSE) {
                    break;
                }
            }
        }

        return $v->visitTypeLeave($this);
    }


    public function jsonSerialize()
    {
        return parent::jsonSerialize()+[
            'restrictions' => $this->restrictions
        ];
    }

    public function hasRestrictions()
    {
        return count($this->restrictions) > 0;
    }

    public function getRestrictions()
    {
        return $this->restrictions;
    }
}
