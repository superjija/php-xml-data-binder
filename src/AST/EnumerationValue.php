<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

/**
 * A value in an enumeration.
*/

namespace PXDB\AST;

use PXDB\AST\Visitor\VisitorAbstract;


class EnumerationValue extends Tree
{
    public function accept(VisitorAbstract $v)
    {
        return $v->visitEnumerationValue($this);
    }
}
