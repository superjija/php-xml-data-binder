<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */


/**
 * A Structure represents an (un)ordered set of types/elements.
 * Think of an abstraction of ordered and choice complex-types in a schema.
*/

namespace PXDB\AST;

use PXDB\AST\Visitor\VisitorAbstract;


class Structure extends Tree
{
    /**
     * @var StructureType An Enum, what type this structure is
     */
    private $structureType;
    private $getMethod;
    private $setMethod;

    public function __construct($name = '', $type = '')
    {
        parent::__construct($name, $type);
        $this->setStructureType(StructureType::STANDARD());
    }

    public function setStructureType(StructureType $type)
    {
        $this->structureType = $type;
    }

    public function getStructureType()
    {
        return $this->structureType;
    }

    public function setGetMethod($methodName)
    {
        $this->getMethod = $methodName;
    }

    public function getGetMethod()
    {
        return $this->getMethod;
    }

    public function setSetMethod($methodName)
    {
        $this->setMethod = $methodName;
    }

    public function getSetMethod()
    {
        return $this->setMethod;
    }

    public function getName()
    {
        return $this->name;
    }

    public function accept(VisitorAbstract $v)
    {
        if ($v->visitStructureEnter($this)) {
            foreach ($this->children as $child) {
                if ($child->accept($v) === FALSE) {
                    break;
                }
            }
        }

        return $v->visitStructureLeave($this);
        //poopoo
    }
}
