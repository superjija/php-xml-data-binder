<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

/**
 * A StructureElement contains one or more AST-nodes. It's kind of a wrapper
 * for the containing nodes in a Structure-node (@see Structure).
*/

namespace PXDB\AST;

use PXDB\AST\Visitor\VisitorAbstract;


class StructureElement extends Tree
{
    protected $testMethod;
    protected $getMethod;
    protected $setMethod;
    protected $style;

    public function __construct($name = '', $type = '')
    {
        parent::__construct($name, $type);
    }

    public function setTestMethod($methodName)
    {
        $this->testMethod = $methodName;
    }

    public function getTestMethod()
    {
        return $this->testMethod;
    }

    public function setGetMethod($methodName)
    {
        $this->getMethod = $methodName;
    }

    public function getGetMethod()
    {
        return $this->getMethod;
    }

    public function setSetMethod($methodName)
    {
        $this->setMethod = $methodName;
    }

    public function getSetMethod()
    {
        return $this->setMethod;
    }

    public function setStyle($style)
    {
        $this->style = $style;
    }

    public function getStyle()
    {
        return $this->style;
    }


    public function accept(VisitorAbstract $v)
    {
        if ($v->visitStructureElementEnter($this)) {
            foreach ($this->children as $child) {
                if ($child->accept($v) === FALSE) {
                    break;
                }
            }
        }

        return $v->visitStructureElementLeave($this);
    }
}
