<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

/**
 * An item/element in a collection.
 *
 */

namespace PXDB\AST;

use PXDB\AST\Visitor\VisitorAbstract;


class CollectionItem extends Tree
{

    public function accept(VisitorAbstract $v)
    {
        return $v->visitCollectionItem($this);
    }
}
