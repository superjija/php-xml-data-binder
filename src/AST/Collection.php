<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

/**
 * A collection of elements.
 *
 */

namespace PXDB\AST;

use PXDB\AST\Visitor\VisitorAbstract;


class Collection extends Tree
{
    private $getMethod;
    private $setMethod;

    // okay, these names are a bit clunky
    public function setGetMethod($getMethod)
    {
        $this->getMethod = $getMethod;
    }

    public function getGetMethod()
    {
        return $this->getMethod;
    }

    public function setSetMethod($setMethod)
    {
        $this->setMethod = $setMethod;
    }

    public function getSetMethod()
    {
        return $this->setMethod;
    }

    public function accept(VisitorAbstract $v)
    {
        if ($v->visitCollectionEnter($this)) {
            foreach ($this->children as $child) {
                if ($child->accept($v) === FALSE) {
                    break;
                }
            }
        }
        return $v->visitCollectionLeave($this);
    }
}
