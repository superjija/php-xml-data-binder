<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

/**
 * Represents a <code>&lt;sequence></code>-node of an XML-Schema.
*/

namespace PXDB\ParseTree;

use PXDB\ParseTree\Visitor\VisitorAbstract;


class SequenceNode extends Tree
{
    private $elementCount;

    public function __construct($xmlOrOptions, $level = 0)
    {
        parent::__construct($xmlOrOptions, $level);
        $this->options = AttributeHelper::getSequenceOptions($xmlOrOptions);
    }

    public function getMinOccurs()
    {
        return $this->options['minOccurs'];
    }

    public function getId()
    {
        return $this->options['id'];
    }

    /**
     * @return int
     * @throws \RuntimeException
     */
    public function getElementCount()
    {
        if ($this->elementCount == 1) {
            $child = $this->children[0];

            if ($child instanceof ElementNode) {
                $max = $child->getMaxOccurs();
                if ($max === 'unbounded') {
                    return -1;
                }
            }
            else {
                throw new \RuntimeException('Currently not supported');
            }
        }

        return $this->elementCount;
    }

    public function accept(VisitorAbstract $v)
    {
        $v->visitSequenceNode($this);

        foreach ($this->children as $child) {
            $child->accept($v);
        }
    }
}
