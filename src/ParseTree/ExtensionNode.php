<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

namespace PXDB\ParseTree;


use PXDB\ParseTree\Visitor\VisitorAbstract;


/**
 * Represents a <code>&lt;extension></code>-node of an XML-Schema.
*/
class ExtensionNode extends Tree
{

    public function __construct($xmlOrOptions, $level = 0)
    {
        parent::__construct($xmlOrOptions, $level);
        $this->options = AttributeHelper::getExtensionOptions($xmlOrOptions);
    }

    public function getId()
    {
        return $this->options['id'];
    }

    public function getBase()
    {
        return $this->options['base'];
    }

    public function accept(VisitorAbstract $v)
    {
        $v->visitExtensionNode($this);

        foreach ($this->children as $child) {
            $child->accept($v);
        }
    }
}
