<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

namespace PXDB\ParseTree;

use SimpleXMLElement;


/**
 * A factory for creating option-arrays for element attributes of the Parse Tree.
*/
class AttributeHelper
{

    public static function getElementOptions($objectOrArray)
    {
        $defaultAttributes = [
            'id' => '',
            'name' => '',
            'ref' => '',
            'type' => '',
            'substitutionGroup' => '',
            'default' => '',
            'fixed' => '',
            'form' => '',
            'maxOccurs' => 1,
            'minOccurs' => 1,
            'nillable' => FALSE,
            'abstract' => FALSE,
            'block' => '',
            'final' => '',
        ];

        return self::createProperAttributes($defaultAttributes, $objectOrArray);
    }

    public static function getSimpleTypeOptions($objectOrArray)
    {
        $defaultAttributes = [
            'id' => '',
            'name' => '',
        ];

        return self::createProperAttributes($defaultAttributes, $objectOrArray);
    }

    public static function getComplexTypeOptions($objectOrArray)
    {
        $defaultAttributes = [
            'id' => '',
            'name' => '',
            'abstract' => FALSE,
            'mixed' => FALSE,
            'block' => '',
            'final' => '',
        ];

        return self::createProperAttributes($defaultAttributes, $objectOrArray);
    }

    public static function getSequenceOptions($objectOrArray)
    {
        $defaultAttributes = [
            'id' => '',
            'maxOccurs' => 1,
            'minOccurs' => 1,
        ];

        return self::createProperAttributes($defaultAttributes, $objectOrArray);
    }

    public static function getAttributeOptions($objectOrArray)
    {
        $defaultAttributes = [
            'default' => '',
            'fixed' => '',
            'form' => '',
            'name' => '',
            'ref' => '',
            'type' => '',
            'use' => '',
        ];

        return self::createProperAttributes($defaultAttributes, $objectOrArray);
    }

    public static function getRestrictionOptions($objectOrArray)
    {
        $defaultAttributes = [
            'id' => '',
            'base' => '',
        ];

        return self::createProperAttributes($defaultAttributes, $objectOrArray);
    }

    public static function getEnumerationOptions($objectOrArray)
    {
        $defaultAttributes = [
            'value' => '',
        ];

        return self::createProperAttributes($defaultAttributes, $objectOrArray);
    }

    public static function getComplexContentOptions($objectOrArray)
    {
        $defaultAttributes = [
            'id' => '',
            'mixed' => FALSE,
        ];

        return self::createProperAttributes($defaultAttributes, $objectOrArray);
    }

    public static function getExtensionOptions($objectOrArray)
    {
        $defaultAttributes = [
            'id' => '',
            'base' => '',
        ];

        return self::createProperAttributes($defaultAttributes, $objectOrArray);
    }

    public static function getChoiceOptions($objectOrArray)
    {
        $defaultAttributes = [
            'id' => '',
            'maxOccurs' => 1,
            'minOccurs' => 1,
        ];

        return self::createProperAttributes($defaultAttributes, $objectOrArray);
    }

    private static function createProperAttributes(array $defaultAttributes, $actualAttributes)
    {
        $options = [];

        if ($actualAttributes instanceof SimpleXMLElement) {
            $options = self::convertSimpleXmlAttributesToStringArray($actualAttributes);
        }
        else {
            $options = $actualAttributes;
        }

        $cleanedOptions = self::castAttributesToProperTypes($options);
        $cleanedOptions = self::removeNamespaces($cleanedOptions);

        return array_merge($defaultAttributes, $cleanedOptions);
    }

    private static function convertSimpleXmlAttributesToStringArray(SimpleXMLElement $simpleXml)
    {
        $attributes = $simpleXml->attributes();
        $array = [];

        foreach ($attributes as $key => $val) {
            $array[$key] = (string)$val;
        }

        return $array;
    }

    private static function castAttributesToProperTypes(array $attributes)
    {
        $array = [];

        foreach ($attributes as $key => $val) {
            if ($key == 'minOccurs' || $key == 'maxOccurs') {
                if (is_numeric((string)$val)) {
                    $array[$key] = (int)$val;
                }
                else {
                    $array[$key] = (string)$val;
                }
            }
            else {
                if (self::attributeHasBooleanValue($key)) {
                    if ((string)$val == 'true') {
                        $array[$key] = TRUE;
                    }
                    else {
                        $array[$key] = FALSE;
                    }
                }
                else {
                    $array[$key] = (string)$val;
                }
            }
        }

        return $array;
    }

    private static function attributeHasBooleanValue($attribute)
    {
        return $attribute == 'abstract' || $attribute == 'nillable' || $attribute == 'mixed';
    }

    private static function removeNamespaces(array $attributes)
    {
        $newAttributes = $attributes;

        // all parts of PiBX which handle ParseTree nodes, look at the type-attribute
        // so referenced types, have no other semantically value within PiBX.
        if (array_key_exists('ref', $newAttributes)) {
            $newAttributes['type'] = $newAttributes['ref'];
            unset($newAttributes['ref']);
        }

        if (array_key_exists('type', $newAttributes)) {
            $newAttributes['type'] = self::removeNamespace($newAttributes['type']);
        }

        if (array_key_exists('base', $newAttributes)) {
            $newAttributes['base'] = self::removeNamespace($newAttributes['base']);
        }

        return $newAttributes;
    }

    private static function removeNamespace($stringWithNamespace)
    {
        $cleanedString = $stringWithNamespace;

        if (strpos($cleanedString, ':') !== FALSE) {
            $parts = explode(':', $cleanedString);
            $cleanedString = $parts[1];
        }

        return $cleanedString;
    }
}
