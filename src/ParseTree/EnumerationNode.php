<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

/**
 * Represents a <code>&lt;enumeration></code>-node of an XML-Schema.
*/

namespace PXDB\ParseTree;

use PXDB\ParseTree\Visitor\VisitorAbstract;


class EnumerationNode extends Tree
{

    public function __construct($xmlOrOptions, $level = 0)
    {
        parent::__construct($xmlOrOptions, $level);
        $this->options = AttributeHelper::getEnumerationOptions($xmlOrOptions);
    }

    public function getValue()
    {
        return $this->options['value'];
    }

    public function accept(VisitorAbstract $v)
    {
        $v->visitEnumerationNode($this);
    }
}
