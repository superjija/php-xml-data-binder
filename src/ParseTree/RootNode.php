<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */


namespace PXDB\ParseTree;

use PXDB\ParseTree\Visitor\VisitorAbstract;


class RootNode extends Tree
{
    /**
     * @var string Contains the targetNamespace defined in a schema (schema-level)
     */
    private $targetNamespace;

    public function __construct()
    {
        // Intentionally blank
        // This constructor does not need the parameters
        // defined in Tree
    }

    public function setTargetNamespace($targetNamespace)
    {
        $this->targetNamespace = $targetNamespace;
    }

    public function getTargetNamespace()
    {
        return $this->targetNamespace;
    }

    public function accept(VisitorAbstract $v)
    {
        /**
         * Traverses the ParseTree with a depth-first strategy.
         */
        foreach ($this->children as $child) {
            $child->accept($v);
        }
    }
}
