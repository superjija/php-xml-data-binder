<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

namespace PXDB\ParseTree;

use PXDB\ParseTree\Visitor\VisitorAbstract;


/**
 * Represents a <code>&lt;complexContent></code>-node of an XML-Schema.
*/
class ComplexContentNode extends Tree
{
    private $elementCount;

    public function __construct($xmlOrOptions, $level = 0)
    {
        parent::__construct($xmlOrOptions, $level);
        $this->options = AttributeHelper::getComplexContentOptions($xmlOrOptions);
    }

    public function isMixed()
    {
        return $this->options['mixed'] == 'true';
    }

    public function getId()
    {
        return $this->options['id'];
    }

    public function accept(VisitorAbstract $v)
    {
        $v->visitComplexContentNode($this);

        foreach ($this->children as $child) {
            $child->accept($v);
        }
    }
}
