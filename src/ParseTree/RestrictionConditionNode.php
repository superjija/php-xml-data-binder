<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

/**
 * Represents a <code>&lt;restriction></code>-node of an XML-Schema.
*/

namespace PXDB\ParseTree;

use PXDB\ParseTree\Visitor\VisitorAbstract;


class RestrictionConditionNode extends Tree
{
    private $name;
    private $value;
    public function __construct($name, $xmlOrOptions, $level = 0)
    {
        parent::__construct($xmlOrOptions, $level);
        $this->name = $name;
        $this->options = AttributeHelper::getElementOptions($xmlOrOptions);
        $this->value = $this->options['value']?:null;
    }

    public function accept(VisitorAbstract $v)
    {
        $v->visitRestrictionConditionNode($this);

        foreach ($this->children as $child) {
            $child->accept($v);
        }
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    public function jsonSerialize()
    {
        $parent = parent::jsonSerialize();
        $parent['restriction_name'] = $this->name;
        $parent['restriction_value'] = $this->value;
        return $parent;
    }
}
