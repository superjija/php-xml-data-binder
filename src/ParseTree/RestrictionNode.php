<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

/**
 * Represents a <code>&lt;restriction></code>-node of an XML-Schema.
*/

namespace PXDB\ParseTree;

use PXDB\ParseTree\Visitor\VisitorAbstract;


class RestrictionNode extends Tree
{
    public function __construct($xmlOrOptions, $level = 0)
    {
        parent::__construct($xmlOrOptions, $level);
        $this->options = AttributeHelper::getRestrictionOptions($xmlOrOptions);
    }

    public function getName()
    {
        return $this->options['name'];
    }

    /**
     * Returns the restriction-base (xml-attribute value)
     * @return string
     */
    public function getBase()
    {
        return $this->options['base'];
    }

    public function accept(VisitorAbstract $v)
    {
        $v->visitRestrictionNode($this);

        foreach ($this->children as $child) {
            $child->accept($v);
        }
    }
}
