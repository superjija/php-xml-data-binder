<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

/**
 * Represents a <code>&lt;attribute></code>-node of an XML-Schema.
*/

namespace PXDB\ParseTree;

use PXDB\ParseTree\Visitor\VisitorAbstract;


class AttributeNode extends Tree
{

    public function __construct($xmlOrOptions, $level = 0)
    {
        parent::__construct($xmlOrOptions, $level);
        $this->options = AttributeHelper::getAttributeOptions($xmlOrOptions);
    }

    public function getName()
    {
        return $this->options['name'];
    }

    public function getType()
    {
        return $this->options['type'];
    }

    public function getUse()
    {
        return $this->options['use'];
    }

    public function isRequired()
    {
        return strtolower($this->getUse()) == 'required';
    }

    public function getForm()
    {
        return $this->options['form'];
    }

    public function accept(VisitorAbstract $v)
    {
        $v->visitAttributeNode($this);

        foreach ($this->children as $child) {
            $child->accept($v);
        }
    }
}
