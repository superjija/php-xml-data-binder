<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

/**
 * Represents a <code>&lt;element></code>-node of an XML-Schema.
*/

namespace PXDB\ParseTree;

use PXDB\ParseTree\Visitor\VisitorAbstract;


class ElementNode extends Tree
{

    public function __construct($xmlOrOptions, $level = 0)
    {
        parent::__construct($xmlOrOptions, $level);
        $this->options = AttributeHelper::getElementOptions($xmlOrOptions);
        //        if ($xmlOrOptions instanceof SimpleXMLElement) {
        //            $attributes = $xmlOrOptions->attributes();
        //
        //            $this->options['name'] = (string)$attributes['name'];
        //            $this->options['type'] = (string)$attributes['type'];
        //            if (strpos($this->options['type'], ':') !== false) {
        //                // remove the namespace prefix
        //                $parts = explode(':', $this->options['type']);
        //                $this->options['type'] = $parts[1];
        //            }
        //            $this->options['minOccurs'] = (string)$attributes['minOccurs'];
        //            $this->options['maxOccurs'] = (string)$attributes['maxOccurs'];
        //        } else {
        //            $this->options = $xmlOrOptions;
        //        }
    }

    public function getName()
    {
        return $this->options['name'];
    }

    public function getId()
    {
        return $this->options['id'];
    }

    public function isAnonym()
    {
        return $this->getName() === '';
    }

    public function getType()
    {
        return $this->options['type'];
    }

    public function getMinOccurs()
    {
        return $this->options['minOccurs'];
    }

    public function getMaxOccurs()
    {
        return $this->options['maxOccurs'];
    }

    public function isNillable()
    {
        return $this->options['nillable'];
    }

    public function getForm()
    {
        return $this->options['form'];
    }

    public function hasReferencedElement()
    {
        return $this->options['ref'] != '';
    }

    public function getRef()
    {
        return $this->options['ref'];
    }

    public function isOptional()
    {
        return $this->getMinOccurs() == 0 || $this->isNillable();
    }

    public function accept(VisitorAbstract $v)
    {
        $v->visitElementNode($this);

        foreach ($this->children as $child) {
            $child->accept($v);
        }
    }
}
