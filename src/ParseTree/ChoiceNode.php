<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

/**
 * Represents a <code>&lt;choice></code>-node of an XML-Schema.
*/

namespace PXDB\ParseTree;

use PXDB\ParseTree\Visitor\VisitorAbstract;


class ChoiceNode extends Tree
{
    public function __construct($xmlOrOptions, $level = 0)
    {
        parent::__construct($xmlOrOptions, $level);
        $this->options = AttributeHelper::getChoiceOptions($xmlOrOptions);
    }

    public function accept(VisitorAbstract $v)
    {
        $v->visitChoiceNode($this);

        foreach ($this->children as $child) {
            $child->accept($v);
        }
    }
}
