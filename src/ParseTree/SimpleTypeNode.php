<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

/**
 * Represents a <code>&lt;simpleType></code>-node of an XML-Schema.
*/

namespace PXDB\ParseTree;

use PXDB\ParseTree\Visitor\VisitorAbstract;


class SimpleTypeNode extends Tree
{
    public function __construct($xmlOrOptions, $level = 0)
    {
        parent::__construct($xmlOrOptions, $level);
        $this->options = AttributeHelper::getSimpleTypeOptions($xmlOrOptions);
    }

    public function getName()
    {
        return $this->options['name'];
    }

    public function accept(VisitorAbstract $v)
    {
        $v->visitSimpleTypeNode($this);

        foreach ($this->children as $child) {
            $child->accept($v);
        }
    }
}
