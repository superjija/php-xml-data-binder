<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

/**
 * Defines the structure of the Visitor-Pattern used for the ParseTree.
*/

namespace PXDB\ParseTree\Visitor;

use PXDB\ParseTree\AttributeNode;
use PXDB\ParseTree\ChoiceNode;
use PXDB\ParseTree\ComplexContentNode;
use PXDB\ParseTree\ComplexTypeNode;
use PXDB\ParseTree\ElementNode;
use PXDB\ParseTree\EnumerationNode;
use PXDB\ParseTree\ExtensionNode;
use PXDB\ParseTree\RestrictionConditionNode;
use PXDB\ParseTree\RestrictionNode;
use PXDB\ParseTree\SequenceNode;
use PXDB\ParseTree\SimpleTypeNode;
use PXDB\ParseTree\Tree;


interface VisitorAbstract
{
    public function visitAttributeNode(AttributeNode $attribute);

    public function visitElementNode(ElementNode $element);

    public function visitSimpleTypeNode(SimpleTypeNode $simpleType);

    public function visitComplexTypeNode(ComplexTypeNode $complexType);

    public function visitSequenceNode(SequenceNode $sequence);

    public function visitGroupNode(Tree $group);

    public function visitAllNode(Tree $all);

    public function visitChoiceNode(ChoiceNode $choice);

    public function visitRestrictionNode(RestrictionNode $restriction);

    public function visitRestrictionConditionNode(RestrictionConditionNode $restriction);

    public function visitEnumerationNode(EnumerationNode $enumeration);

    public function visitComplexContentNode(ComplexContentNode $complexContent);

    public function visitExtensionNode(ExtensionNode $extension);
}
