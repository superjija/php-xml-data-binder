<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

/**
 * Represents a <code>&lt;complexType></code>-node of an XML-Schema.
*/

namespace PXDB\ParseTree;

use PXDB\ParseTree\Visitor\VisitorAbstract;


class ComplexTypeNode extends Tree
{
    public function __construct($xmlOrOptions, $level = 0)
    {
        parent::__construct($xmlOrOptions, $level);
        $this->options = AttributeHelper::getComplexTypeOptions($xmlOrOptions);
    }

    public function getName()
    {
        return $this->options['name'];
    }

    public function getId()
    {
        return $this->options['id'];
    }

    public function isAbstract()
    {
        return $this->options['abstract'] === TRUE;
    }

    public function isMixed()
    {
        return $this->options['mixed'] === TRUE;
    }

    /**
     * @param VisitorAbstract $v
     */
    public function accept(VisitorAbstract $v)
    {
        $v->visitComplexTypeNode($this);

        foreach ($this->children as $child) {
            $child->accept($v);
            //            $v->plowTypesForLevel($child->getLevel());
        }
    }

}
