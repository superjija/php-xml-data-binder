<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

/**
 * A Tree represents the whole schema definition in a composite
 * object structure.
*/

namespace PXDB\ParseTree;

use PXDB\ParseTree\Visitor\VisitorAbstract;


abstract class Tree implements \JsonSerializable
{
    /**
     * @var int The current level in the tree.
     */
    private $level;

    /**
     * @var Tree[]
     */
    protected $children = [];

    /**
     * @var Tree The current's node parent node.
     */
    protected $parent;

    /**
     * @var array List of registered namespaces of the current XML fragment
     */
    protected $namespaces;

    /**
     * @var array Attributes of the node (key => value)
     */
    protected $options;

    public function __construct($xmlOrOptions, $level = 0)
    {
        $this->level = $level;
        if ($xmlOrOptions instanceof \SimpleXMLElement) {
            $this->namespaces = $xmlOrOptions->getDocNamespaces(TRUE);
        }
        elseif (is_array($xmlOrOptions)) {
            $this->options = $xmlOrOptions;
            $this->namespaces = isset($xmlOrOptions['namespaces']) ? $xmlOrOptions['namespaces'] : [];
        }
        else {
            throw new \InvalidArgumentException('Parameter must be instance of SimpleXMLElement or array');
        }
    }

    public function setParent(Tree $tree)
    {
        $this->parent = $tree;
    }

    /**
     * @return Tree
     */
    public function getParent()
    {
        return $this->parent;
    }

    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Returns used/defined namespaces in the current tree element as an
     * associative array: prefix => URI
     *
     * @return array
     */
    public function getNamespaces()
    {
        return $this->namespaces;
    }

    public function setNamespaces(array $namespaces)
    {
        $this->namespaces = $namespaces;
    }

    public function addNamespace(array $namespace)
    {
        $this->namespaces[] = $namespace;
    }

    public function add(Tree $tree)
    {
        $this->children[] = $tree;
        $tree->setParent($this);
        return $this;
    }

    public function remove(Tree $tree)
    {
        $index = array_search($tree, $this->children, TRUE);

        if ($index === FALSE) {
            return FALSE;
        }

        array_splice($this->children, $index, 1);

        return TRUE;
    }

    /**
     * @param $index
     * @return Tree
     * @throws \RuntimeException
     */
    public function get($index)
    {
        if (!isset($this->children[$index])) {
            throw new \RuntimeException('Invalid child index "' . $index . '"');
        }
        return $this->children[$index];
    }

    public function hasChildren()
    {
        return count($this->children) > 0;
    }

    public function countChildren()
    {
        return count($this->children);
    }

    /**
     * @return Tree[]
     */
    public function getChildren(): array
    {
        return $this->children;
    }

    abstract function accept(VisitorAbstract $v);

    public function jsonSerialize()
    {
        return [
            'class' => get_class($this),
            'level' => $this->level,
            'namespace' => $this->namespaces,
            'options' => $this->options,
            'element_name' => $this->options['name']?:null,
            'children' => $this->children,
        ];
    }


}
