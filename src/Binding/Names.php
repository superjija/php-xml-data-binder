<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

/**
 * Names is a facility to create names for getter/setter methods or variable-names
 * for class attributes needed when generating code.
 * There are two instances (at least), that needs to get names out of the AST.
 * The Binding_Creator and the ClassGenerator. Both of them needs to retrieve
 * names for methods for specific AST-nodes.
 * Binding_Names encapsulates the logic for defining those names.
*/

namespace PXDB\Binding;

use PXDB\AST\Collection;
use PXDB\AST\CollectionItem;
use PXDB\AST\Enumeration;
use PXDB\AST\Structure;
use PXDB\AST\StructureElement;
use PXDB\AST\Tree;
use PXDB\AST\Type;
use PXDB\AST\TypeAttribute;


class Names
{
    /**
     * @param Tree $tree
     * @return string
     * @throws \RuntimeException
     */
    public static function createGetterNameFor(Tree $tree)
    {
        if ($tree instanceof Type) {
            if ($tree->getType() == 'boolean') {
                return 'is' . self::getCamelCasedName($tree->getName());
            }

            return 'get' . self::getCamelCasedName($tree->getName());
        }
        elseif ($tree instanceof Collection) {
            if ($tree->countChildren() == 1) {
                $child = $tree->get(0);
                $name = self::getCamelCasedName($child->getName());

                return 'get' . self::getCollectionName($name);
            }
        }
        elseif ($tree instanceof CollectionItem) {
            // a CollectionItem in a TypeAttribute is a list of the CollectionItems
            // without a parenting Collection-node

            if ($tree->getParent() instanceof Type) {
                $name = self::getCamelCasedName($tree->getName());

                return 'get' . self::getCollectionName($name);
            }
        }
        elseif ($tree instanceof TypeAttribute || $tree instanceof Structure) {
            if ($tree->getName() != '') {
                $getterName = $tree->getName();
            }
            else {
                $getterName = $tree->getType();
            }

            $name = self::getCamelCasedName($getterName);

            return 'get' . $name;
        }
        elseif ($tree instanceof StructureElement) {
            $structureAst = $tree->getParent();
            $structureName = self::getCamelCasedName($structureAst->getName());
            $elementName = self::getCamelCasedName($tree->getName());

            return 'get' . $structureName . $elementName;
        }
        elseif ($tree instanceof Enumeration) {
            $name = self::getCamelCasedName($tree->getName());
            return 'get' . $name;
        }
        return '';
    }

    /**
     * @param Tree $tree
     * @return string
     * @throws \RuntimeException
     */
    public static function createSetterNameFor(Tree $tree)
    {
        if ($tree instanceof Type) {
            return 'set' . self::getCamelCasedName($tree->getName());
        }
        elseif ($tree instanceof Collection) {
            if ($tree->countChildren() == 1) {
                $child = $tree->get(0);
                $name = self::getCamelCasedName($child->getName());
                // append a plural "s" for collection items if applicable
                if (strtolower(substr($name, -1)) != 's') {
                    $name .= 's';
                }
                return 'set' . $name;
            }
        }
        elseif ($tree instanceof CollectionItem) {
            // a CollectionItem in a TypeAttribute is a list of the CollectionItems
            // without a parenting Collection-node
            if ($tree->getParent() instanceof Type) {
                $name = self::getCamelCasedName($tree->getName());

                return 'set' . self::getCollectionName($name);
            }
        }
        elseif ($tree instanceof TypeAttribute || $tree instanceof Structure) {
            if ($tree->getName() != '') {
                $getterName = $tree->getName();
            }
            else {
                $getterName = $tree->getType();
            }
            $name = self::getCamelCasedName($getterName);
            return 'set' . $name;
        }
        elseif ($tree instanceof StructureElement) {
            $structureAst = $tree->getParent();
            $structureName = self::getCamelCasedName($structureAst->getName());
            $elementName = self::getCamelCasedName($tree->getName());

            return 'set' . $structureName . $elementName;
        }
        elseif ($tree instanceof Enumeration) {
            $name = self::getCamelCasedName($tree->getName());
            return 'set' . $name;
        }
        return '';
    }

    public static function createTestFunctionFor(Tree $tree)
    {
        if ($tree instanceof StructureElement) {
            $structureAst = $tree->getParent();
            $structureName = self::getCamelCasedName($structureAst->getName());
            $elementName = self::getCamelCasedName($tree->getName());

            return 'is' . $structureName . $elementName;
        }
        return '';
    }

    /**
     * Returns a valid classname for a given string *or* instance of Tree.
     *
     * @param mixed $treeOrString string or Tree
     * @return string
     * @throws \RuntimeException
     */
    public static function createClassnameFor($treeOrString)
    {
        $name = '';

        if (is_string($treeOrString)) {
            $name = $treeOrString;
        }
        elseif (is_object($treeOrString) && ($treeOrString instanceof Tree)) {
            $name = $treeOrString->getName();
        }
        else {
            throw new \RuntimeException("Cannot create classname for " . print_r($treeOrString, TRUE));
        }

        return self::getCamelCasedName($name);
    }

    /**
     * XSD-Choice elements are handled via private variables/constants
     *
     * @param Tree $tree
     * @return array The choice constants as strings
     * @throws \RuntimeException
     */
    public static function createChoiceConstantsFor(Tree $tree)
    {
        $childCount = $tree->countChildren();
        $name = strtoupper($tree->getName());

        $names = [];

        for ($i = 0; $i < $childCount; ++$i) {
            $child = $tree->get($i);
            $childName = strtoupper($child->getName());
            $names[] = $name . '_' . $childName . '_CHOICE';
        }

        return $names;
    }

    /**
     * Returns a camel cased version of the given string.
     *
     * @param string $name
     * @return string
     */
    public static function getCamelCasedName($name)
    {
        $name = str_replace('_', '-', $name);
        $parts = explode('-', $name);

        foreach ($parts as &$part) {
            $part = ucfirst($part);
        }

        return implode('', $parts);
    }

    private static function combineNamePartsCamelCased($name)
    {
        $name = str_replace('_', '-', $name);
        $parts = explode('-', $name);

        foreach ($parts as &$part) {
            $part = ucfirst($part);
        }

        return implode('', $parts);
    }

    /**
     * Creates a valid attribute name.
     *
     * Strips off "-" and "_" characters in a name.
     *
     * @param string $name
     * @return string
     */
    public static function getAttributeName($name)
    {
        $camelCasedName = self::combineNamePartsCamelCased($name);//self::getCamelCasedName($name);

        $firstTwoLettersOfOriginalName = substr($name, 0, 2);
        $firstTwoLettersOfCamelCasedName = substr($camelCasedName, 0, 2);

        if ($firstTwoLettersOfOriginalName == strtoupper($firstTwoLettersOfCamelCasedName)) {
            return $camelCasedName;
        }

        return lcfirst($camelCasedName);
    }

    /**
     * Creates a valid name for a list attribute.
     *
     * @param string $name
     * @return string
     */
    public static function getListAttributeName($name)
    {
        $attributeName = self::getAttributeName($name);

        if (self::nameAlreadyEndsWithWordList($attributeName)) {
            return $attributeName;
        }

        return $attributeName . 'List';
    }

    private static function nameAlreadyEndsWithWordList($name)
    {
        return preg_match('/lists?$/is', $name) > 0;
    }

    /**
     * A very naive implemantation to create a plural version of the given name.
     * It concats a "s" on a name if applicable, i.e. "item" gets "items" and so on.
     * Or replaces a trailing "y" with "ies", i.e. "country" gets "countries".
     *
     * @param string $name
     * @return string Plural of $name
     */
    private static function getCollectionName($name)
    {
        $lastLetterInLowerCase = strtolower(substr($name, -1));

        if ($lastLetterInLowerCase == 'y') {
            $name = substr_replace($name, 'ies', strlen($name) - 1, 1);
        }
        elseif ($lastLetterInLowerCase != 's') {
            $name .= 's';
        }

        return $name;
    }
}
