<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

/**
 * Traverses the AST as a Visitor to produce a binding definition.
*/

namespace PXDB\Binding;

use PXDB\AST\Collection;
use PXDB\AST\CollectionItem;
use PXDB\AST\Enumeration;
use PXDB\AST\EnumerationValue;
use PXDB\AST\Structure;
use PXDB\AST\StructureElement;
use PXDB\AST\StructureType;
use PXDB\AST\Tree;
use PXDB\AST\Type;
use PXDB\AST\TypeAttribute;
use PXDB\AST\Visitor\VisitorAbstract;
use PXDB\Util\XsdType;


class Creator implements VisitorAbstract
{
    private $dom;
    private $bindingRoot;
    private $nodeStack;

    private $typeList;

    private $namespaceHasBeenAdded;
    /**
     * @var string
     */
    private $namespace;

    public function __construct(array $typeList, string $namespace = NULL)
    {
        $this->typeList = $typeList;
        $this->nodeStack = [];
        $this->namespaceHasBeenAdded = FALSE;

        $this->dom = new \DomDocument('1.0');
        $this->dom->preserveWhiteSpace = FALSE;
        $this->dom->formatOutput = TRUE;

        $this->bindingRoot = $this->dom->createElement('binding');

        $this->dom->appendChild($this->bindingRoot);
        array_push($this->nodeStack, $this->bindingRoot);
        $this->namespace = $namespace;
    }

    public function getXml()
    {
        return $this->dom->saveXML();
    }

    private function getClassName($name)
    {
        return $this->namespace.($this->namespace?'\\':'').Names::createClassnameFor($name);
    }

    public function visitCollectionEnter(Collection $collection)
    {
        $name = $collection->getName();
        $getter = Names::createGetterNameFor($collection);
        $setter = Names::createSetterNameFor($collection);
        $node = $this->dom->createElement('collection');

        $node->setAttribute('name', $name);
        $node->setAttribute('get-method', $getter);
        $node->setAttribute('set-method', $setter);

        $lastNode = $this->nodeStack[count($this->nodeStack) - 1];
        $lastNode->appendChild($node);
        array_push($this->nodeStack, $node);

        return TRUE;
    }

    public function visitCollectionLeave(Collection $collection)
    {
        array_pop($this->nodeStack);

        return TRUE;
    }

    public function visitCollectionItem(CollectionItem $collectionItem)
    {
        $name = $collectionItem->getName();
        $type = $collectionItem->getType();

        if (XsdType::isBaseType($type)) {
            $node = $this->dom->createElement('value');
            $node->setAttribute('style', 'element');
            $node->setAttribute('name', $name);
            $node->setAttribute('type', $type);
        }
        else {
            $node = $this->dom->createElement('structure');
            $node->setAttribute('map-as', $type);
            $node->setAttribute('name', $name);
        }

        if (!($collectionItem->getParent() instanceof Collection)) {
            // create an anonymous collection whenever there is no parent Collection
            // which would handle this
            $parentNode = $this->dom->createElement('collection');
            $getter = Names::createGetterNameFor($collectionItem);
            $setter = Names::createSetterNameFor($collectionItem);

            $parentNode->setAttribute('get-method', $getter);
            $parentNode->setAttribute('set-method', $setter);
            $parentNode->appendChild($node);
            // the parent node gets inserted on-the-fly
            $node = $parentNode;
        }

        $lastNode = $this->nodeStack[count($this->nodeStack) - 1];
        $lastNode->appendChild($node);

        return TRUE;
    }

    public function visitEnumerationEnter(Enumeration $enumeration)
    {
        $name = $enumeration->getName();
        $getter = Names::createGetterNameFor($enumeration);
        $setter = Names::createSetterNameFor($enumeration);

        $node = $this->dom->createElement('value');
        $node->setAttribute('style', 'element');
        $node->setAttribute('name', $name);
        $node->setAttribute('get-method', $getter);
        $node->setAttribute('set-method', $setter);

        $lastNode = $this->nodeStack[count($this->nodeStack) - 1];
        $lastNode->appendChild($node);

        array_push($this->nodeStack, $node);


        return TRUE;
    }

    public function visitEnumerationLeave(Enumeration $enumeration)
    {
        array_pop($this->nodeStack);

        return TRUE;
    }

    public function visitEnumerationValue(EnumerationValue $enumerationValue)
    {
        return TRUE;
    }

    public function visitStructureEnter(Structure $structure)
    {
        $name = $structure->getName();
        $type = $structure->getType();

        $node = $this->dom->createElement('structure');

        if ($structure->getStructureType() == StructureType::CHOICE()) {
            $node->setAttribute('ordered', 'false');
            $node->setAttribute('choice', 'true');
        }
        elseif ($structure->getStructureType() == StructureType::STANDARD()) {
            if ($type != '') {
                // // a standard structure with a given type is a reference to that type
                $getter = Names::createGetterNameFor($structure);
                $setter = Names::createSetterNameFor($structure);

                $node->setAttribute('map-as', $type);
                $node->setAttribute('get-method', $getter);
                $node->setAttribute('set-method', $setter);
                $node->setAttribute('name', $name);
            }
            else {
                $node->setAttribute('name', $name);
            }
        }

        $lastNode = $this->nodeStack[count($this->nodeStack) - 1];

        if ($name != '' && $structure->getStructureType() != StructureType::STANDARD()) {
            $parentNode = $this->dom->createElement('structure');
            $parentNode->setAttribute('name', $name);
            $parentNode->appendChild($node);

            $lastNode->appendChild($parentNode);
            $lastNode = $parentNode;
        }

        $lastNode->appendChild($node);


        array_push($this->nodeStack, $node);

        return TRUE;
    }

    public function visitStructureLeave(Structure $structure)
    {
        array_pop($this->nodeStack);

        return TRUE;
    }

    public function visitStructureElementEnter(StructureElement $structureElement)
    {
        $name = $structureElement->getName();
        $type = $structureElement->getType();
        $getter = Names::createGetterNameFor($structureElement);
        $setter = Names::createSetterNameFor($structureElement);
        $tester = Names::createTestFunctionFor($structureElement);

        $node = $this->dom->createElement('value');

        $node->setAttribute('style', 'element');
        $node->setAttribute('name', $name);

        $parent = $structureElement->getParent();
        if ($parent instanceof Structure) {
            if ($parent->getStructureType() == StructureType::CHOICE()) {
                $node->setAttribute('test-method', $tester);
                $node->setAttribute('get-method', $getter);
                $node->setAttribute('set-method', $setter);
                $node->setAttribute('usage', 'optional');
            }
            else {
                $node->setAttribute('get-method', $getter);
                $node->setAttribute('set-method', $setter);
            }
        }

        $lastNode = $this->nodeStack[count($this->nodeStack) - 1];
        $lastNode->appendChild($node);
        array_push($this->nodeStack, $node);

        return TRUE;
    }

    public function visitStructureElementLeave(StructureElement $structureElement)
    {
        array_pop($this->nodeStack);
        return TRUE;
    }

    /**
     * @param Type $type
     * @return bool
     * @throws \RuntimeException
     */
    public function visitTypeEnter(Type $type)
    {
        $name = $type->getName();
        $typesType = $type->getType();
        $className = $this->getClassName($name);

        $this->checkAndAddNamespace($type);

        if ($type->isEnumerationType()) {
            $label = $this->getClassName($name);
            $typeName = $label;
            $nameUsage = $this->getNameUsagesRegardlessOfCase($label);

            if ($nameUsage > 1) {
                $typeName = $typeName . ($nameUsage - 1);
            }

            $node = $this->dom->createElement('format');
            $node->setAttribute('label', $label);
            $node->setAttribute('type', $typeName);
            $node->setAttribute('enum-value-method', 'toString');
            $this->bindingRoot->appendChild($node);
            // children are irrelevant for the binding defintion
            return FALSE;
        }

        $node = $this->dom->createElement('mapping');

        $node->setAttribute('class', $className);

        if ($type->isRoot()) {
            $node->setAttribute('name', $name);
        }
        else {
            $node->setAttribute('abstract', 'true');

            if ($type->getValueStyle() == 'attribute') {
                $node->setAttribute('type-name', $this->getClassName($name));
            }
            else {
                $node->setAttribute('type-name', $name);
            }
        }

        if ($type->hasBaseType()) {
            // when a base-type is available, the "inheritance" hierarchy is
            // expressed via a mapped structure
            $subnode = $this->dom->createElement('structure');
            $subnode->setAttribute('map-as', $type->getBaseType());
            $node->appendChild($subnode);
        }

        if ($typesType != '') {
            $referencedType = $this->getTypeByName($typesType);

            // a base-type will be mapped directly
            if (XsdType::isBaseType($typesType)) {
                $getter = Names::createGetterNameFor($type);
                $setter = Names::createSetterNameFor($type);

                $subnode = $this->dom->createElement('value');

                $style = 'text';
                if ($type->getValueStyle() == 'attribute') {
                    $style = 'attribute';
                }
                $subnode->setAttribute('style', $style);
                $subnode->setAttribute('get-method', $getter);
                $subnode->setAttribute('set-method', $setter);

                $node->appendChild($subnode);
            }
            elseif (!$type->hasChildren() && $referencedType->isEnumerationType()) {
                $getter = Names::createGetterNameFor($type);
                $setter = Names::createSetterNameFor($type);

                $subnode = $this->dom->createElement('value');

                $subnode->setAttribute('style', 'text');
                $subnode->setAttribute('get-method', $getter);
                $subnode->setAttribute('set-method', $setter);

                $subnode->setAttribute('format', $typesType);

                $node->appendChild($subnode);
            }
            else {
                // another complex-type has to be referenced
                $referencedType = Names::createClassnameFor($type);

                $subnode = $this->dom->createElement('structure');

                $subnode->setAttribute('map-as', $referencedType);

                $node->appendChild($subnode);
            }
        }

        $this->bindingRoot->appendChild($node);
        array_push($this->nodeStack, $node);

        return TRUE;
    }

    public function visitTypeLeave(Type $type)
    {
        array_pop($this->nodeStack);
        return TRUE;
    }

    private function checkAndAddNamespace(Type $type)
    {
        if ($this->namespaceHasBeenAdded) {
            return;
        }

        $targetNamespace = $type->getTargetNamespace();

        if ($targetNamespace != '') {
            $node = $this->dom->createElement('namespace');
            $node->setAttribute('uri', $targetNamespace);
            $node->setAttribute('default', 'elements');

            $availableNamespaces = $type->getNamespaces();
            $prefixKey = array_search($targetNamespace, $availableNamespaces);

            if ($prefixKey !== FALSE && $prefixKey != '') {
                $node->setAttribute('prefix', $prefixKey);
            }

            $this->bindingRoot->appendChild($node);

            $this->namespaceHasBeenAdded = TRUE;
        }
    }

    /**
     * @param TypeAttribute $typeAttribute
     * @return bool
     * @throws \RuntimeException
     */
    public function visitTypeAttributeEnter(TypeAttribute $typeAttribute)
    {
        $name = $typeAttribute->getName();
        $type = $typeAttribute->getType();
        $style = $typeAttribute->getStyle();
        $getter = Names::createGetterNameFor($typeAttribute);
        $setter = Names::createSetterNameFor($typeAttribute);

        if ($name == '') {
            $node = $this->dom->createElement('structure');
            $referencedType = $typeAttribute->getParent();
            $typeOfReferencedType = $type;
            $nameOfReferencedType = Names::createClassnameFor($type);
            $referencedType = $this->getTypeByName($type);

            $deep = $this->getDeepestReferencedType($typeAttribute);

            $shouldBeMapped = $this->anonymTypeAttributeShouldBeMapped($type);

            if ($referencedType->getValueStyle() == 'attribute') {
                $node->setAttribute('get-method', $getter);
                $node->setAttribute('set-method', $setter);
                $node->setAttribute('usage', 'optional');
                $subnode = $this->dom->createElement('value');
                $subnode->setAttribute('style', 'attribute');
                $subnode->setAttribute('name', $type);
                $subnode->setAttribute('ns', $referencedType->getTargetNamespace());
                $subnode->setAttribute('get-method', $getter);
                $subnode->setAttribute('set-method', $setter);
                $subnode->setAttribute('usage', 'optional');
                $node->appendChild($subnode);

            }
            else {
                if ($shouldBeMapped == FALSE) {
                    // base types do not need to be mapped via a structure
                    $node->setAttribute('type', $nameOfReferencedType);
                    $node->setAttribute('get-method', $getter);
                    $node->setAttribute('set-method', $setter);
                }
                else {
                    $node->setAttribute('map-as', $nameOfReferencedType);
                    $node->setAttribute('get-method', $getter);
                    $node->setAttribute('set-method', $setter);
                    $node->setAttribute('name', $type);
                }
            }
        }
        else {
            if ($type == '' || XsdType::isBaseType($type) || $typeAttribute->getStyle() == 'attribute') {
                $node = $this->dom->createElement('value');


                $node->setAttribute('style', $style);
                $node->setAttribute('name', $name);
                $node->setAttribute('get-method', $getter);
                $node->setAttribute('set-method', $setter);
            }
            else {
                $getter = Names::createGetterNameFor($typeAttribute);
                $setter = Names::createSetterNameFor($typeAttribute);

                $node = $this->dom->createElement('structure');
                $node->setAttribute('map-as', $type);
                $node->setAttribute('get-method', $getter);
                $node->setAttribute('set-method', $setter);
                $node->setAttribute('name', $name);
            }

            if ($typeAttribute->isOptional()) {
                $node->setAttribute('usage', 'optional');
            }

            $referencedType = $this->getTypeByName($type);
            if ($referencedType && !$referencedType->isStandardType()) {
                $node->setAttribute('format', $type);
            }
        }

        $lastNode = $this->nodeStack[count($this->nodeStack) - 1];
        $lastNode->appendChild($node);
        array_push($this->nodeStack, $lastNode);

        return TRUE;
    }

    public function visitTypeAttributeLeave(TypeAttribute $typeAttribute)
    {
        array_pop($this->nodeStack);

        return TRUE;
    }

    private function anonymTypeAttributeShouldBeMapped($name)
    {
        if (XsdType::isBaseType($name)) {
            $shouldBeMapped = FALSE;
            return FALSE;
        }

        $type = $this->getTypeByName($name);

        do {
            $typeName = $type->getType();
            $name = $type->getName();

            if ($typeName == '' || XsdType::isBaseType($typeName)) {
                return FALSE;
            }

            $test = $this->getTypeByName($typeName);

            if ($test && $test->isEnumerationType()) {
                return FALSE;
            }

            $type = $this->getTypeByTypeName($typeName);
        } while ($type && $typeName != '');

        return TRUE;
    }

    /**
     * Walks down the type-list and returns the last element of chained types.
     * @param Tree $type
     * @return string
     */
    private function getDeepestReferencedType(Tree $type)
    {
        if (XsdType::isBaseType($type->getType())) {
            return $type;
        }

        $nextType = $this->getRootTypeByName($type->getType());
        if ($nextType == NULL) {
            return $type;
        }
        if ($nextType->getType() == '') {
            return $nextType;
        }

        return $this->getDeepestType($nextType);
    }

    private function getDeepestType(Tree $tree)
    {
        if (XsdType::isBaseType($tree->getType())) {
            return $tree;
        }

        $nextType = $this->getNonRootTypeByName($tree->getType());

        if ($nextType == NULL) {
            return $tree;
        }
        if ($nextType == NULL || $nextType->getType() == '') {
            return $nextType;
        }

        return $this->getDeepestType($nextType);
    }

    private function getRootTypeByName($typeName)
    {
        if ($typeName == '') {
            return NULL;
        }
        foreach ($this->typeList as &$type) {
            if (!$type->isRoot()) {
                continue;
            }

            if ($type->getName() == $typeName) {
                return $type;
            }
        }

        return NULL;
    }

    private function getNonRootTypeByName($typeName)
    {
        if ($typeName == '') {
            return NULL;
        }
        foreach ($this->typeList as &$type) {
            if ($type->isRoot()) {
                continue;
            }

            if ($type->getName() == $typeName) {
                return $type;
            }
        }

        return NULL;
    }


    private function getTypeByName($typeName)
    {
        if ($typeName == '') {
            return NULL;
        }
        foreach ($this->typeList as &$type) {
            //            if (!$type->isRoot()) {
            //                continue;
            //            }

            if ($type->getName() == $typeName) {
                return $type;
            }
        }
        //        return $type;
        return NULL;
    }

    private function getTypeByTypeName($typeName)
    {
        if ($typeName == '') {
            return NULL;
        }

        foreach ($this->typeList as &$type) {
            if ($type->isRoot()) {
                continue;
            }

            if ($type->getType() == $typeName) {
                return $type;
            }
        }

        return NULL;
    }

    private function getNameUsagesRegardlessOfCase($typeName)
    {
        $lowerCaseTypeName = strtolower($typeName);
        $usage = 0;

        foreach ($this->typeList as &$type) {
            if (strtolower($type->getName()) == $lowerCaseTypeName) {
                $usage++;
            }
        }

        return $usage;
    }
}
