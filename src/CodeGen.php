<?php
/**
 * Copyright (c) 2018 Jindřich Buk (https://jindrichbuk.cz)
 * Developed for NanoEnergies
 */

/**
 * CodeGen is used to generate PHP classes from a given XSD-Schema file.
 *
 * It is constructed like a classical compiler. It goes through different
 * phases like: parsing the input stream, building an abstract syntax tree
 * and so on.
*/

namespace PXDB;

use PXDB\Binding\Creator;
use PXDB\CodeGen\ASTCreator;
use PXDB\CodeGen\ASTOptimizer;
use PXDB\CodeGen\ClassGenerator;
use PXDB\CodeGen\CodeGenerationResult;
use PXDB\CodeGen\CodeGenException;
use PXDB\CodeGen\SchemaParser;
use PXDB\CodeGen\TypeUsage;


class CodeGen
{
    /**
     * @var string
     */
    private $outputDir;
    /**
     * @var bool
     */
    private $typeChecks;

    /**
     * Creating an object of PiBX_CodeGen starts the code generation.
     *
     * A valid schema-file has to be passed.
     * Options can be passed as an associative array with a value of a boolean "true".
     * This way the code generation can be customized.
     *
     * Possible option-values are (keys of "$options"):
     *     - "typechecks" enables the generation of type-check code into setter methods.
     *
     * @param string $outputDir
     * @param bool $typeChecks
     * @param string|null $namespace
     */
    public function __construct(string $outputDir, $typeChecks = TRUE)
    {

        $this->outputDir = rtrim($outputDir, '/\\');
        $this->typeChecks = $typeChecks;
    }

    /**
     * @param string $schemaFile
     * @param string|null $namespace
     * @return CodeGenerationResult
     * @throws CodeGenException
     */
    public function generate(string $schemaFile, string $namespace = NULL)
    {
        $result = new CodeGenerationResult($schemaFile, $this->outputDir);
        $typeUsage = new TypeUsage();

        // phase 1 - Parsing schema file
        try {
            $parser = new SchemaParser($schemaFile, $typeUsage);

            try {
                $parsedTree = $parser->parse();
            }
            catch (\RuntimeException $e) {
                throw new CodeGenException($e->getMessage(), $e->getCode(), $e);
            }
        }
        catch (\Exception $e) {
            throw new CodeGenException($e->getMessage(), $e->getCode(), $e);
        }

        // phase 2 - Creating abstract syntax tree
        $creator = new ASTCreator();
        $parsedTree->accept($creator);

        $typeList = $creator->getTypeList();

        // phase 3 - Optimizing abstract syntax tree
        $result->setTypes(count($typeList));
        $usages = $typeUsage->getTypeUsages();

        $optimizer = new ASTOptimizer($typeList, $typeUsage);
        //$typeList = $optimizer->optimize();
        $result->setTypesAfterOptimalization(count($typeList));

        // phase 4 - Creating binding.xml
        $b = new Creator($typeList, $namespace);

        foreach ($typeList as &$type) {
            $type->accept($b);
        }

        if (!is_dir($this->outputDir)) {
            mkdir($this->outputDir);
        }

        file_put_contents($this->outputDir . '/binding.xml', $b->getXml());
        $result->setBingingFile($this->outputDir.'/binding.xml');

        // phase 5 - Generating classes
        $generator = new ClassGenerator();

        if ($this->typeChecks) {
            $generator->enableTypeChecks();
        }

        foreach ($typeList as &$type) {
            $type->accept($generator);
        }


        try {
            $classes = 0;
            $autoload = "<?php\n";
            foreach ($generator->getClasses() as $className => $classCode) {
                $code = "<?php\n".($namespace?('namespace '.$namespace."; \n\n "):'') . $classCode;
                $autoload.='require_once(\''.($this->outputDir . '/' . $className . '.php')."'); \n";
                file_put_contents($this->outputDir . '/' . $className . '.php', $code);
                $classes++;
                $result->setGeneratedClasses($classes);
            }
            file_put_contents($this->outputDir . '/autoload.php', $autoload);
        }
        catch (\ReflectionException $e) {
            throw new CodeGenException($e->getMessage(), $e->getCode(), $e);
        }
        catch (\RuntimeException $e) {
            throw new CodeGenException($e->getMessage(), $e->getCode(), $e);
        }
        $result->setSuccessful();
        return $result;
    }
}
